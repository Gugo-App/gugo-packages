Meteor.publish('myNetworks', function() {
  if(!this.userId) {
    return this.ready();
  }

  return Gugo.collections.networks.find({ ownerId : this.userId })
});