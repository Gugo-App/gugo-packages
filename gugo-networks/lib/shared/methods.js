Meteor.methods({
  createNetwork : function(name, members) {
    if(!this.userId) {
      return;
    }

    Gugo.collections.networks.insert({
      ownerId : this.userId,
      name : name,
      members : members
    }, function(err, docs) {
      console.log("Error?", err, "docs", docs);
    })
  }
});