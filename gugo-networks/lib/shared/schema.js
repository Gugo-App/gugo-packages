NetworkScehma = new SimpleSchema({
  ownerId : {
    type : String
  },
  name : {
    type : String
  },
  members : {
    type : [String]
  }
});