Gugo.collections.suggestedLocations.helpers({
  getLocationName : function() {
    if(this.name) {
    	var split = this.name.split(", ");
    	return split[0];
    } else if(this.address) {
      	return this.address.fullAddress;
    } else {
    	return "";
    }

    
  }
});