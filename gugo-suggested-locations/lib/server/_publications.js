Meteor.publish('eventSuggestedLocations', function(eventId) {
  check(eventId, String);

  var suggestedLocations = Gugo.collections.suggestedLocations.find({ eventId : eventId });

  if(suggestedLocations) {
    return suggestedLocations;
  } else {
    return this.ready();
  }
});