Meteor.methods({
  addSuggestedLocation: function(location, eventId) {
    if(!(eventId && this.userId)) {
      throw new Meteor.Error('invalid-params', 'Did not pass loc or userId')
    }

    console.log("Adding suggested location", location);

    var insertObj = {
      eventId : eventId,
      createdBy : this.userId,
      suggestedBy : [],
      address : {},
      geo: {}
    };

    if(location.address) {
      insertObj.address = location.address;
    }

    if(location.name) {
      insertObj.name = location.name;
    }

    if(location.geo && location.geo[0] && location.geo[1]) {
      insertObj.geo.coordinates = [location.geo.longitude, location.geo.latitude];
    }


    var suggestedLocations = Gugo.collections.suggestedLocations.find({ eventId : eventId }).fetch();
    var locationExists = false;

    console.log("[COMPARING LOCATIONS]");

    if(suggestedLocations && suggestedLocations.length > 0) {
      _.each(suggestedLocations, function (sl) {
        console.log("Comparing ", sl);

        const addressSame = sl.address && insertObj.address && sl.address.city === insertObj.address.city &&
          sl.address.fullAddress === insertObj.address.fullAddress &&
          sl.address.state === insertObj.address.state &&
          sl.address.street === insertObj.address.street &&
          sl.address.zip === insertObj.address.zip;

        console.log("Address Same?", addressSame);

        //var geoSame = false;
        //
        //if(sl.geo === insertObj.geo) {
        //
        //} else if( (sl.geo && sl.geo.coordinates && insertObj.geo && insertObj.geo.coordinates) || (!sl.geo && !insertObj.geo) ) {
        //  geoSame = (sl.geo.coordinates[0] === insertObj.geo.coordinates[0]) && (sl.geo.coordinates[1] === insertObj.geo.coordinates[1]);
        //}
        //
        //console.log("Geo same?", geoSame, (sl.geo && insertObj.geo), (!sl.geo && !insertObj.geo));

        const nameSame = sl.name === insertObj.name;

        console.log("Name same?", nameSame);

        if(addressSame && nameSame) {
          locationExists = true;
        }
              
      })
    }

    if(locationExists) {
      console.log("Location already exists return");
      return;
    }

    function cb(err, docs) {
      if(!err) {
        Meteor.call('newActivity', {eventId: eventId, type : ActivityTypes.SUGGEST_LOCATION});
      }
    }

    return Gugo.collections.suggestedLocations.insert(insertObj, Gugo.handlers.mongo.bind(this, {msg : "Suggested Locations", callback : cb }));
  },
  suggestExistingLocation : function(locId) {
    if(!(locId && this.userId)) {
      throw new Meteor.Error('invalid-params', 'Did not pass loc id or userId')
    }

    Gugo.collections.suggestedLocations.update(locId, {
      $push : {
        'suggestedBy' : this.userId
      },
      $inc : {
        suggestedCount : 1
      }
    }, Gugo.handlers.mongo.bind(this, { msg : "Suggested Locations" }));
  },
  unSuggestExistingLocation : function (locId) {
    if(!(locId && this.userId)) {
      throw new Meteor.Error('invalid-params', 'Did not pass loc id or userId')
    }

    Gugo.collections.suggestedLocations.update(locId, {
      $pull : {
        'suggestedBy' : this.userId
      },
      $inc : {
        suggestedCount : -1
      }
    }, Gugo.handlers.mongo.bind(this, { msg : "Suggested Locations" }));

  },
  switchSuggestedLocation : function(fromLocId, toLocId) {
    Meteor.call("unSuggestExistingLocation", fromLocId);
    Meteor.call("suggestExistingLocation", toLocId);
  },
  lockLocation : function(locId) {
    var lc = Gugo.collections.suggestedLocations.findOne(locId);

    if(!lc) {
      throw new Meteor.Error("Could not find Suggested Location");
    }


    console.log("Date", lc);

    function cb(err) {
      if(!err) {
        Meteor.call('newActivity', {eventId: lc.eventId, type : ActivityTypes.OWNER_LOCKED_LOCATION});
      }
    }

    console.log("Location", lc);

    lc.geo.type = "Point";

    var locationUpdate = {
      address : lc.address,
      geo: lc.geo,
      name : lc.name
    };

    Gugo.collections.userEvents.update(lc.eventId, { $set :{
      location : locationUpdate,
      isLocationLocked : true
    }}, Gugo.handlers.mongo.bind(this, { msg : "Suggested Locations", callback : cb }))
  }
});