Gugo.schemas.SuggestedLocationSchema = new SimpleSchema({
  name : {
    type: String,
    optional : true
  },
  geo: {
    type : Gugo.schemas.GeoSchema,
    optional: true
  },
  address : {
    type : Gugo.schemas.AddressSchema,
    optional: true
  },
  eventId : {
    type : String,
    optional: false
  },
  createdBy : {
    type : String,
    optional : false
  },
  suggestedBy : {
    type : [String],
    optional: false
  },
  suggestedCount : {
    type : Number,
    defaultValue : 0,
    optional : false
  }
});