Gugo.collections.suggestedLocations = new Meteor.Collection('suggested-locations');

Gugo.collections.suggestedLocations.attachSchema(Gugo.schemas.SuggestedLocationSchema);

if(Meteor.isServer) {
  Gugo.collections.suggestedLocations._ensureIndex({ eventId : 1 })
}