import React from 'react';
import ReactDOM from 'react-dom';
import { Dialog, FlatButton } from 'material-ui';


ShareDialog = React.createClass({
  getInitialState() {
    return {
      open : true
    }
  },
  onCancel() {
    console.log("Submitted Dialog");

    Meteor.setTimeout( ()=> {
      this.setState({open: false});
      ReactDOM.unmountComponentAtNode(document.getElementById('dialog-root'));
      this.props.cancelCallback && this.props.cancelCallback();
    }, 50);

  },
  onShare() {
    var self = this;
    console.log("Submitted Dialog");
    Meteor.setTimeout( ()=> {
      GugoShare.shareAll(self.props.eventId);
      self.setState({open: false});
      ReactDOM.unmountComponentAtNode(document.getElementById('dialog-root'));
      self.props.submitCallback && self.props.submitCallback();

    }, 50);
  },
  onCopyLink() {
    GugoShare.copyLink(this.props.eventId);
    SnackBarController.show("Copied To Clipboard");
  },
  render() {
    let defaultProps = {
      title : "Share This Event",
      textBody : "Share This Event",
      shareLabel : "Share"
    };

    const link = GugoShare.getEventLink(this.props.eventId);

    this.props = _.extend(defaultProps, this.props);

    return <Dialog
      bodyStyle={{ padding: 0 }}
      title={this.props.title}
      titleStyle={{fontFamily: "Oswald", textTransform: "uppercase", fontSize: ".9rem", textAlign: "center", color: "#006091", lineHeight: "1.2rem"}}
      open={this.state.open}
      onRequestClose={ this.onCancel }>

      <FlexBox flexDirection="row" style={{paddingTop:30}}>
        <input type="text" value={link} style={{ width: "70%" }} />
        <FlatButton
          style={{ width:"30%", backgroundColor:"#4BCDDB", color:"white", height:50, borderRadius: "0px"}}
          labelStyle={{fontFamily: "Oswald"}}
          label={"Copy Url"}
          primary={true}
          onTouchTap={this.onCopyLink} />
      </FlexBox>

      <FlatButton
        style={{ width:"100%", backgroundColor:"#00779c", color:"white", height:50, borderRadius: "0px"}}
        labelStyle={{fontFamily: "Oswald"}}
        label={"Share"}
        primary={true}
        onTouchTap={this.onShare}/>

    </Dialog>
  }
});

DialogController.dialogs.share = ShareDialog;