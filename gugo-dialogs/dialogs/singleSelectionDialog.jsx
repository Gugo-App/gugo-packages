import React from 'react';
import ReactDOM from 'react-dom';
import { Divider, Dialog } from 'material-ui';

SelectionPopupDialog = React.createClass({
  getDefaultProps() {
    return {
      selections : [
        {
          text : "This is a selection",
          handler : function() {
            console.log("Clicked selection");
            this.onCancel();
          }
        }
      ]
    }
  },
  getInitialState() {
    return {
      open : true
    }
  },
  onCancel() {
    console.log("Submitted Dialog");

    Meteor.setTimeout( ()=> {
      this.setState({open: false});
      ReactDOM.unmountComponentAtNode(document.getElementById('dialog-root'));
      this.props.cancelCallback && this.props.cancelCallback();
    }, 50);
  },
  handleSelection(index) {
     const selections = this.props.selections;
     selections[index].handler && selections[index].handler();

     if(!selections[index].suppressAutoClose) {
      this.onCancel();
      }
  },
  renderSelections() {
    var els = [];
    const selections = this.props.selections;

    for(var i = 0; i < selections.length; i++) {
      els.push(
        <div key={uuid.new()}
        style={{ height: "40px", lineHeight: "40px", verticalAlign : "center", fontWeight: "500", paddingLeft : "10px" }} 
        onClick={this.handleSelection.bind(this, i)}> { selections[i].text } 
        </div>
      );
      els.push(<Divider key={uuid.new()} />)
    }

    return els;
  },
  render() {
    return <Dialog
      bodyStyle={{ padding: 0 }}
      open={this.state.open}
      onRequestClose={ this.onCancel }>

      {this.renderSelections()}
    </Dialog>
  }
});

DialogController.dialogs.selection = SelectionPopupDialog;