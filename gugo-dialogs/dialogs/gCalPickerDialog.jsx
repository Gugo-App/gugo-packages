import React from 'react';
import ReactDOM from 'react-dom';
import { Dialog, SelectableList, List, ListItem, FlatButton, Checkbox } from 'material-ui';

//Todo : Clean up this code, its messy and isnt descriptive
GoogleCalendarDialog = React.createClass({
    getInitialState() {
      return {
          open : true,
          checkboxes : []
      }
    },
    componentWillMount() {
        var calData = this.props.calData;
        var checks = [];

        var google = Meteor.user().services.google;

        _.each(calData, function(cal) {
            cal.checked = false;
            _.each(google.calendars, function(aCal) {
               if(aCal === cal.id) {
                   cal.checked = true;
               }
            });
            checks.push(cal);
        });

        console.log(checks);
        this.setState({ checkboxes : checks });
    },
    handleCancel() {
        ReactDOM.unmountComponentAtNode(document.getElementById('dialog-root'));
        this.props.cancelCallback && this.props.cancelCallback();
    },
    handleSubmit() {
        var calsChecked = this.state.checkboxes.map(function(cal) {
            if(cal.checked) {
                return cal.id;
            }
        });
        var undRemoved = _.without(calsChecked, undefined);

        this.props.submitCallback(undRemoved);
        ReactDOM.unmountComponentAtNode(document.getElementById('dialog-root'));
    },
    onCheck(index, evt, checked) {
        var state = this.state;
        state.checkboxes[index].checked = checked;
        this.setState(state);
    },
    listCalendars() {
        var calData = this.props.calData;
        var els = [];

        for(var i = 0; i < calData.length; i++) {
            els.push(<ListItem
              key={calData[i].id}
              primaryText={calData[i].summary}
              leftCheckbox={
                  <Checkbox
                  checked={this.state.checkboxes[i].checked}
                  key={i}
                  onCheck={this.onCheck.bind(this, i)}/>}
            />)
        }
        return els;
    },
    render() {
        let actions = [
            <FlatButton
              label="Cancel"
              secondary={true}
              onTouchTap={this.handleCancel} />,
            <FlatButton
              label="Submit"
              primary={true}
              onTouchTap={this.handleSubmit} />
        ];

        return <Dialog
          title="Import Calendars"
          open={this.state.open}
          actions={actions}
          autoDetectWindowHeight={true}
          autoScrollBodyContent={true}>


            <List>
                {this.listCalendars()}
            </List>
        </Dialog>
    }
});

DialogController.dialogs.gCalPicker = GoogleCalendarDialog;