import React from 'react';
import ReactDOM from 'react-dom';
import { Dialog, FlatButton } from 'material-ui';


ErrorDialog = React.createClass({
  getInitialState() {
    return {
      open : true
    }
  },
  onSubmit() {

    Meteor.setTimeout( ()=> {
      this.setState({open: false});
      ReactDOM.unmountComponentAtNode(document.getElementById('dialog-root'));
      this.props.submitCallback && this.props.submitCallback();
    }, 50);
  },
  render() {
    let defaultProps = {
      title : "",
      textBody : "",
      submitLabel : "OK"
    };

    this.props = _.extend(defaultProps, this.props);

    if(!_.isString(this.props.title)) {
      this.props.title = "Unknown Error";
    }

    return <Dialog
      bodyStyle={{ padding: 0}}
      title={this.props.title}
      titleStyle={{fontFamily: "Oswald", textTransform: "uppercase", fontSize: ".9rem", textAlign: "center", color: "#006091", lineHeight: "1.2rem"}}
      open={this.state.open} >
      <p style={{ textAlign : "center", fontSize : "17px"}}>{this.props.textBody}</p>

      <FlatButton
        style={{ width:"100%", backgroundColor:"#00779c", color:"white", height:50, borderRadius: "0px"}}
        labelStyle={{fontFamily: "Oswald"}}
        label={this.props.submitLabel}
        primary={true}
        onTouchTap={this.onSubmit}/>
    </Dialog>
  }
});

DialogController.dialogs.error = ErrorDialog;