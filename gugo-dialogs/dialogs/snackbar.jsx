import React from 'react';
import ReactDOM from 'react-dom';
import { TrackerReactMixin } from 'meteor/ultimatejs:tracker-react';
import { Snackbar } from 'material-ui';

Session.setDefault('SnackBarState', { open : false });

SnackbarContainer =  React.createClass({
  mixins : [TrackerReactMixin],
  handleRequestClose() {
    Session.set('SnackBarState', { open : false });
  },
  snackBarState() {
    return Session.get('SnackBarState');
  },
  render() {
    var snackBarState = this.snackBarState();

    if(!snackBarState) return null;

    return <Snackbar
      open={snackBarState.open}
      style={{textAlign: "center", textTransform: "uppercase", fontFamily: "Oswald !important"}}
      bodyStyle={{backgroundColor: "#006091", fontFamily: "Oswald !important"}}
      message={snackBarState.message || ""}
      autoHideDuration={snackBarState.autohide || 1000}
      onRequestClose={this.handleRequestClose} />
  }
});