import React from 'react';
import ReactDOM from 'react-dom';
import { Dialog, List, ListItem } from 'material-ui';

let standardActions = [
    { text: 'Cancel' },
    { text: 'Submit', onTouchTap: this._onDialogSubmit, ref: 'submit' }
];


ModalOpen = new ReactiveVar(false);

ReactiveDialog = React.createClass({
    mixins : [ReactMeteorData],
    getMeteorData() {
        var open = ModalOpen.get();
        return {
            open : open
        }
    },
    render() {

        console.log("DATA", this.data);

        return <Dialog
            title="Import Google Calendars"
            open={this.data.open}
            actions={standardActions}
            onRequestClose={this.onClose}
            autoDetectWindowHeight={true}
            autoScrollBodyContent={true}>


            <List>
                <ListItem primaryText="Primary Calendar" />
                <ListItem primaryText="Stita Calendar" />
                <ListItem primaryText="Brians CALENDAR" />
            </List>
        </Dialog>
    }
});