import React from 'react';
import ReactDOM from 'react-dom';
import { Dialog, FlatButton } from 'material-ui';


ConfirmDialog = React.createClass({
  getInitialState() {
    return {
      open : true
    }
  },
  onCancel() {
    console.log("Submitted Dialog");

    Meteor.setTimeout( ()=> {
      this.setState({open: false});
      ReactDOM.unmountComponentAtNode(document.getElementById('dialog-root'));
      this.props.cancelCallback && this.props.cancelCallback();
    }, 50);

  },
  onSubmit() {
    console.log("Submitted Dialog");
    Meteor.setTimeout( ()=> {
      this.setState({open: false});
      ReactDOM.unmountComponentAtNode(document.getElementById('dialog-root'));
      this.props.submitCallback && this.props.submitCallback();
    }, 50);
  },
  render() {
    let defaultProps = {
      title : "Dialog",
      textBody : "",
      submitLabel : "Confirm",
      cancelLabel : "Cancel"
    };

    this.props = _.extend(defaultProps, this.props);

      const submitLabel = this.props.submitLabel;
      const cancelLabel = this.props.cancelLabel;

    return <Dialog
      bodyStyle={{ padding: 0}}
      title={this.props.title}
      titleStyle={{fontFamily: "Oswald", textTransform: "uppercase", fontSize: ".9rem", textAlign: "center", color: "#006091", lineHeight: "1.2rem"}}
      open={this.state.open} >
      <p>{this.props.textBody}</p>
      <FlexBox flexDirection="row" style={{paddingTop:30}}>
        <FlatButton
          style={{ width:"50%", backgroundColor:"#00779c", color:"white", height:50, borderRadius: "0px"}}
          labelStyle={{fontFamily: "Oswald"}}
          label={submitLabel}
          primary={true}
          onTouchTap={this.onSubmit}/>

        <FlatButton
          style={{ width:"50%", backgroundColor:"#71dde5", color:"white", height:50, borderRadius:"0px"}}
          labelStyle={{fontFamily: "Oswald"}}
          label={cancelLabel}
          secondary={true}
          onTouchTap={this.onCancel}/>
      </FlexBox>
    </Dialog>
  }
});

DialogController.dialogs.confirm = ConfirmDialog;