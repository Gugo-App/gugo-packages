import React from 'react';
import ReactDOM from 'react-dom';
import { Dialog } from 'material-ui';


PromptDialog = React.createClass({
  getInitialState() {
    return {
      open : true
    }
  },
  onCancel() {
    console.log("Submitted Dialog");

    this.setState({open: false});
    ReactDOM.unmountComponentAtNode(document.getElementById('dialog-root'));
    this.props.cancelCallback && this.props.cancelCallback();

  },
  onSubmit() {
    console.log("Submitted Dialog");

    this.setState({open: false});
    ReactDOM.unmountComponentAtNode(document.getElementById('dialog-root'));
    this.props.submitCallback && this.props.submitCallback();
  },
  render() {
    let defaultProps = {
      title : "Dialog",
      textBody : ""
    };

    this.props = _.extend(defaultProps, this.props);

    let standardActions = [
      { text: 'Cancel', onTouchTap : this.onCancel},
      { text: 'Submit', onTouchTap: this.onSubmit, ref: 'submit' }
    ];

    return <Dialog
      title={this.props.title}
      open={this.state.open}
      actions={standardActions}

    >
      {this.props.textBody}
    </Dialog>
  }
});

DialogController.dialogs.prompt = PromptDialog;