SnackBarController = {
  show(message, duration) {
    var msg = message || "";
    var dur = duration || 1000;
    Session.set("SnackBarState", { open : true, duration : dur, message : msg })
  }
};