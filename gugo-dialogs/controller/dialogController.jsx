import React from 'react';
import ReactDOM from 'react-dom';

DialogController = {
  currentlyOpenedDialog: null,
  dialogs: {},
  showError(options, submitCallback) {
    this.show("error", options, submitCallback);
  },
  showPrompt(options, submitCallback, cancelCallback) {
    this.show("prompt", options, submitCallback, cancelCallback);
  },
  showConfirm(options, submitCallback, cancelCallback) {
    this.show("confirm", options, submitCallback, cancelCallback);
  },
  showSelection(options) {
    this.show("selection", options);
  },
  showShare(options, submitCallback) {
    this.show("share", options, submitCallback);
  },
  showCDVCalendarPicker(submitCallback, cancelCallback) {
    var self = this;

    console.log("Listing calendars...");
    CDVCalendar.listAll(function(err, cals) {
      console.log("Err?", err, "cals", cals);
      if (err) {
        DialogController.showError({title: "Error Retrieving Data"});
        console.log(err);
      }

      self.show("CDVCalPicker", {calData: cals}, submitCallback, cancelCallback);

    });
  },
  showGoogleCalendarPicker(submitCallback, cancelCallback) {
    var self = this;

    GAPI.calendars.list(function(err, calendars) {
      if (err) {
        DialogController.showError({title: "Error Retrieving Data"});
        console.log(err);
      }

      var items = calendars.items;

      self.show("gCalPicker", { calData : items }, submitCallback, cancelCallback);
    });
  },
  show(type, options, submitCallback, cancelCallback) {
    if(!_.isUndefined(this.dialogs[type])) {
      if(!options) options = {};

      if(submitCallback && _.isFunction(submitCallback)) {
        options.submitCallback = submitCallback;
      }

      if(cancelCallback && _.isFunction(cancelCallback)) {
        options.cancelCallback = cancelCallback;
      }

      var el = React.createElement(this.dialogs[type], options);
      ReactDOM.render(el, document.getElementById('dialog-root'));
      this.currentlyOpenedDialog = el;
    }
  },
  close() {
    ReactDOM.unmountComponentAtNode(document.getElementById('dialog-root'));
  }
};
