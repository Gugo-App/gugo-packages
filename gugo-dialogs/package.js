Package.describe({
  name: 'gugo:dialogs',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use([
    'ecmascript',
    'underscore',
    'meteor-base',
    'session',
    'meteorhacks:subs-manager@1.6.3',
    'ultimatejs:tracker-react'
  ]);


  api.addFiles(['controller/dialogController.jsx', 'controller/snackbarController.jsx'], 'client');

  api.addFiles([
    'dialogs/calPickerDialog.jsx',
    'dialogs/confirmDialog.jsx',
    'dialogs/shareDialog.jsx',
    'dialogs/errorDialog.jsx',
    'dialogs/gCalPickerDialog.jsx',
    'dialogs/promptDialog.jsx',
    'dialogs/singleSelectionDialog.jsx',
    'dialogs/snackbar.jsx'
  ], 'client');

  api.export(['DialogController', 'SnackBarController', 'SnackbarContainer'], 'client');
});

Package.onTest(function(api) {

});
