/**
 * Login with a phone number and verification code.
 * @param phone The phone number.
 * @param code The verification code.
 * @param [callback]
 */
Meteor.loginWithSms = function (phone, code, callback) {
  function _intercept(err, res) {
    if(!err) {

      //var loginTokenKey = 'Meteor.loginToken',
      //  loginTokenExpiresKey = 'Meteor.loginTokenExpires';
      //
      //
      //console.log("Got intercept", err, res);
      //
      //Accounts.connection.setUserId(res.userId);
      //
      //Meteor._localStorage.setItem(loginTokenKey, result.token);
      //Meteor._localStorage.setItem(loginTokenExpiresKey, result.tokenExpires);

    }

    console.log("Got intercept", err, res);
    callback && callback(err, res);
  }


  Accounts.callLoginMethod({
    methodArguments: [{
      sms: true,
      phone: phone,
      code: code
    }],
    userCallback: callback
  });
};

/**
 * Request a verification code.
 * @param phone The phone number to verify.
 * @param [callback]
 */
Meteor.sendVerificationCode = function (phone, callback) {
  Meteor.call('accounts-sms.sendVerificationCode', phone, callback);
};

Meteor.sendVerificationCodeForExisting = function (phone, userId, callback) {
  Meteor.call('accounts-sms.sendVerificationCodeForExisting', phone, userId, callback);
};

Meteor.linkWithSms = function(phone, userId, code, callback) {
  Meteor.call('accounts-sms.verifyCodeForExisting', phone, userId, code, callback);
};

