var codes = new Mongo.Collection('meteor_accounts_sms');

var TwilioClient = new Twilio({
  from: Meteor.settings.twilio.numbers.generic,
  sid: Meteor.settings.twilio.sid,
  token: Meteor.settings.twilio.token
});

function buildTwilioInviteMessage(sender, eventId) {
  var event = Gugo.collections.events.findOne(eventId),
    userEvent = Gugo.collections.userEvents.findOne(eventId),
    path = event ? 'web/event/' : 'web/userEvent/',
    title = event ? event.title : userEvent.title;

  const url = Gugo.web_hostnameUrl(path + eventId);

  return sender.name + " has invited you to " + title + ". View the event at : " + url + ". Text STOP to stop any more texts from this number. Text DOWNLOAD to receive a link to GUGO.";
}

Meteor.methods({
  'accounts-sms.sendVerificationCode': function (phone) {
    this.unblock();

    check(phone, String);
    return Accounts.sms.sendVerificationCode(phone);
  },
  'accounts-sms.sendVerificationCodeForExisting': function(phone, userId) {
    this.unblock();

    return Accounts.sms.sendVerificationCode(phone, null, userId);
  },
  'accounts-sms.verifyCodeForExisting' : function (phone, userId, code) {
    this.unblock();

    var user = Meteor.users.findOne(userId);
    if (!user) throw new Meteor.Error('Invalid phone number');

    var validCode = codes.findOne({phone: phone, userId: userId, code: code});
    if (!validCode) throw new Meteor.Error('Invalid verification code');

    // Clear the verification code after a successful login.
    codes.remove({phone: phone});

    return Meteor.users.update(userId, { $set : {phone: phone} });
  },
  sendInvitationSMS : function(sender, receiver, eventId) {
    this.unblock();

    if(Gugo.settings.twilio.internalOnly) {
      console.log("Twilio Outgoing SMS is disabled.")
      return;
    }

    console.log(sender, receiver);
    var message = buildTwilioInviteMessage(sender, eventId);

    try {
      TwilioClient.sendSMS({to: receiver.phone, body: message});
    } catch(e) {
      console.log("[Twilio Send Invitation] Error", e);
    }
  },
  sendDownloadSMS : function(phone) {
    this.unblock();

    //if(Gugo.settings.twilio.internalOnly) {
    //  return;
    //}

    const url = Gugo.hostnameUrl('download');

    TwilioClient.sendSMS({ to : phone, body : "Download Gugo At: " + url })
  },
  sendSMS : function(phone, message) {
    this.unblock();

    if(Gugo.settings.twilio.internalOnly) {
      return;
    }

    try {
      TwilioClient.sendSMS({to: phone, body: message});
    } catch(e) {
      console.log("[Twilio Send Invitation] Error", e);
    }
  },
  checkOnExistingNumber : function(phone) {
    return Meteor.users.findOne({ phone : phone }) ? true : false;
  }
});

// Handler to login with a phone number and code.
Accounts.registerLoginHandler('sms', function (options) {
  if (!options.sms) return;

  check(options, {
    sms: Boolean,
    phone: MatchEx.String(1),
    code: MatchEx.String(1)
  });

  var verified = Accounts.sms.verifyCode(options.phone, options.code);

  console.log("Verified", verified);

  return verified;
});

/**
 * You can set the twilio from, sid and key and this
 * will handle sending and verifying sms with twilio.
 * Or you can configure sendVerificationSms and verifySms helpers manually.
 * @param options
 * @param [options.twilio]
 * @param {String} options.twilio.from The phone number to send sms from.
 * @param {String} options.twilio.sid The twilio sid to use to send sms.
 * @param {String} options.twilio.token The twilio token to use to send sms.
 * @param {Function} [options.sendVerificationCode] (phone)
 * Given a phone number, send a verification code.
 * @param {Function} [options.verifyCode] (phone, code)
 * Given a phone number and verification code return the { userId: '' }
 * to log that user in or throw an error.
 */
Accounts.sms.configure = function (options) {
  check(options, Match.OneOf(
    {
      twilio: {
        from: String,
        sid: String,
        token: String
      },
      message: Match.Optional(String)
    }, {
      lookup: MatchEx.Function(),
      sendVerificationCode: MatchEx.Function(),
      verifyCode: MatchEx.Function()
    }
  ));

  if (options.twilio) {
    Accounts.sms.client = new Twilio(options.twilio);
  } else {
    Accounts.sms.lookup = options.lookup;
    Accounts.sms.sendVerificationCode = options.sendVerificationCode;
    Accounts.sms.verifyCode = options.verifyCode;
  }
  Accounts.sms.message = options.message || 'Your verification code is {{code}}';
};

/**
 * Send a 4 digit verification sms with twilio.
 * @param phone
 */
Accounts.sms.sendVerificationCode = function (phone, message, userId) {
  if (!Accounts.sms.client) throw new Meteor.Error('accounts-sms has not been configured');

  var lookup = Accounts.sms.client.lookup(phone),
    textOptions = {};
  console.log("LOOKUP", lookup);



  if (lookup.carrier && lookup.carrier.type !== 'mobile') {
    throw new Meteor.Error('not a mobile number');
  }

  var code = (Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000) + '';

  console.log("Sending code", code);
  message = message || Accounts.sms.message;
  message = message.replace(/{{code}}/g, code);

  console.log("MEssage replaced", message);

  if(userId) {
    textOptions = {phone: phone, userId : userId, code: code}
  } else {
    // Create user if does not exist
    var user = Meteor.users.findOne({phone: phone});
    if (!user) {
      Meteor.users.insert({
        phone: phone,
        profile : {},
        createdAt: new Date()
      });
    }

    textOptions = {phone: phone, code: code};
  }


  // Clear out existing codes
  codes.remove({phone: phone});
  // Generate a new code.
  codes.insert(textOptions);
  // console.log('sending sms.', phone, code);
  Accounts.sms.client.sendSMS({
    to: phone,
    body: message
  }, function(err) {
    if(err) {
      console.log("Twilio Error", err);
      throw new Meteor.Error("twilio-error", err.message);
    }
  });
};

/**
 * Send a 4 digit verification sms with twilio.
 * @param phone
 * @param code
 */
Accounts.sms.verifyCode = function (phone, code) {
  var user = Meteor.users.findOne({ phone: phone });
  if (!user) throw new Meteor.Error('Invalid phone number');

  var validCode = codes.findOne({phone: phone, code: code});
  if (!validCode) throw new Meteor.Error('Invalid verification code');

  // Clear the verification code after a succesful login.
  codes.remove({phone: phone});

  //var stampedToken     = Accounts._generateStampedLoginToken();
  //var stampedTokenHash = Accounts._hashStampedToken(stampedToken);
  //
  //Meteor.users.update({
  //  _id: user._id
  //}, {
  //  $push: {
  //    "services.resume.loginTokens": stampedTokenHash
  //  }
  //});

  return { userId : user._id }
};

/**
 * Send a 4 digit verification sms with twilio.
 * @param phone
 * @param code
 */
Accounts.sms.verifyCodeForExisting = function (phone, userId, code) {
  var user = Meteor.users.findOne(userId);
  if (!user) throw new Meteor.Error('Invalid phone number');

  var validCode = codes.findOne({phone: phone, userId: userId, code: code});
  if (!validCode) throw new Meteor.Error('Invalid verification code');

  // Clear the verification code after a succesful login.
  codes.remove({phone: phone});

  //var stampedToken     = Accounts._generateStampedLoginToken();
  //var stampedTokenHash = Accounts._hashStampedToken(stampedToken);
  //
  //Meteor.users.update({
  //  _id: userId
  //}, {
  //  $push: {
  //    "services.resume.loginTokens": stampedTokenHash
  //  }
  //});

  return { userId : userId }
};