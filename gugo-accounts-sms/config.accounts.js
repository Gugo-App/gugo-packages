Accounts.sms.configure({
  twilio: {
    from: Meteor.settings.twilio.numbers.auth,
    sid: Meteor.settings.twilio.sid,
    token: Meteor.settings.twilio.token
  }
});