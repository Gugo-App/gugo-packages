Meteor.publish("venuesForCity", function (city) {
  return Gugo.collections.venues.find({ city: city });
});

Meteor.publish("singleVenue", function (id) {
  var venue = Gugo.collections.venues.find({ _id: id });

  if (venue) {
    return venue;
  } else {
    return this.ready();
  }
});

Meteor.publish("allVenues", function () {
  this.unblock();

  var venues = Gugo.collections.venues.find();

  if (venues) {
    return venues;
  } else {
    return this.ready();
  }
});

Meteor.publish("venuesLimit", function (limit, pageSize, query) {
  this.unblock();

  console.log("Query", query, "Limit", limit, "pageSize", pageSize);

  return Gugo.collections.venues.find(query, { sort: {name: 1}, limit : pageSize, skip : limit - pageSize });
});