Gugo.validate.Venue = function (doc, userId) {
  var pfx = "gugo-venues.validation.Venue";

  if (userId && !Match.test(userId, String)) {
    throw new Meteor.Error(pfx, "No valid user ID found");
  }

  if (!Match.test(doc.name, String)) {
    throw new Meteor.Error(pfx, "Venue name is not valid");
  }

  Gugo.validate.Address(doc.location.address);
  Gugo.validate.Geo(doc.location.geo);
  Gugo.validate.Location(doc.location);

  if (!Match.test(doc, Gugo.schemas.VenueSchema)) {
    console.error(pfx + " Venue is invalid", doc);
    throw new Meteor.Error(pfx, "Venue is not valid");
  }

  console.log(pfx + " Venue is valid", doc);

  return true;
};

Gugo.validate.Location = function (doc, userId) {
  var pfx = "gugo-venues.validation.Location";

  if (userId && !Match.test(userId, String)) {
    throw new Meteor.Error(pfx, "No valid user ID found");
  }

  if (!Match.test(doc, Gugo.schemas.LocationSchema)) {
    console.error(pfx + " Location is invalid", doc);
    throw new Meteor.Error(pfx, "Location is not valid");
  }

  console.log(pfx + " Location is valid", doc);

  return true;
};

Gugo.validate.Address = function (doc, userId) {
  var pfx = "gugo-venues.validation.Address";

  if (userId && !Match.test(userId, String)) {
    throw new Meteor.Error(pfx, "No valid user ID found");
  }

  if (!Match.test(doc, Gugo.schemas.AddressSchema)) {
    console.error(pfx + " Address is invalid", doc);
    throw new Meteor.Error(pfx, "Address is not valid");
  }

  console.log(pfx + " Address is valid", doc);

  return true;
};

Gugo.validate.Geo = function (doc, userId) {
  var pfx = "gugo-venues.validation.Geo";

  if (userId && !Match.test(userId, String)) {
    throw new Meteor.Error(pfx, "No valid user ID found");
  }

  if (!Match.test(doc, Gugo.schemas.GeoSchema)) {
    console.error(pfx + " Geo is invalid", doc);
    throw new Meteor.Error(pfx, "Geo is not valid");
  }

  console.log(pfx + " Geo is valid", doc);

  return true;
};
