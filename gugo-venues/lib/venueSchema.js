Gugo.schemas.VenueSchema = new SimpleSchema({
  name:             {
    type:     String,
    optional: true
  },
  description:      {
    type:     String,
    optional: true
  },
  location:         {
    type:     Gugo.schemas.LocationSchema,
    optional: true
  },
  venueUri:         {
    type:     String,
    optional: true
  },
  imageHasTitle:    {
    type:     Boolean,
    optional: true
  },
  "images.heroUri": {
    type:     String,
    optional: true
  },
  "images.tileUri": {
    type:     String,
    optional: true
  },
  createdAt:        {
    type:      Date,
    optional:  true,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  },
  createdBy:        {
    type:     String,
    optional: true
  },
  numEvents : {
    type : Number,
    optional : true,
    defaultValue : 0
  }
});

Gugo.collections.venues.attachSchema(Gugo.schemas.VenueSchema);
