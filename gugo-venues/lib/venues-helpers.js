Gugo.collections.venues.helpers({
  locationName: function() {
    return this.name;
  },
  address: function() {
    return this.location.address;
  },
  geo: function() {
    return this.location.geo;
  }
});