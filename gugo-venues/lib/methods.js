Meteor.methods({
  createVenue: function (venue) {
    var pfx = "Gugo:Venues.createVenue ",
        doc = {
          name:      venue.name,
          location:  {
            address: venue.location.address,
            geo:     {
              coordinates: venue.location.geo.coordinates
            }
          },
          createdBy: this.userId
        },
        _id;

    if (venue.description) {
      doc.description = venue.description;
    }
    if (venue.venueUri) {
      doc.venueUri = venue.venueUri;
    }

    if(venue.heroUri) {
      doc.images = {
        heroUri : venue.heroUri
      }
    }

    // Gugo.validate.Venue(doc, this.userId);

    console.log(pfx + " Have a valid user ID & document - Inserting Venue", doc);

    _id = Gugo.collections.venues.insert(doc, function (err, id) {
      if (err) {
        console.error(pfx + "Error inserting venue", { error: err, venue: venue });
      } else {
        console.log(pfx + "Successfully inserted venue", { id: id });
      }
    });

    return _id;
  },
  updateVenue: function (venue) {
    var pfx  = "Gugo:Venues.updateVenue",
        id   = venue._id || venue.id,
        $set = {};

    check(this.userId, String);

    $set["name"]                     = venue.name;
    $set["location.address"]         = venue.location.address;
    $set["location.geo.coordinates"] = venue.location.geo.coordinates;
    $set["description"] = venue.description;
    $set["venueUri"] = venue.venueUri;

    if(venue.imageHasTitle) {
      $set['imageHasTitle'] = venue.imageHasTitle;
    }

    if(venue.heroUri) {
      $set.images = {
        heroUri : venue.heroUri
      }
    }

    //if (venue.venueUri) {
    //  $set["venueUri"] = venue.venueUri;
    //}

    console.log(pfx + "Updating Venue", { venueId: id, update: $set });

    Gugo.collections.venues.update(id, { $set: $set }, function (err, docs) {
      if (err) {
        console.error(pfx + "Error updating venue", { error: err, venueId: id });
      } else {
        console.log(pfx + "Successfully updated venue", { docs: docs });
      }
    });
  },
  deleteVenue: function (venueId) {
    var pfx      = "Gugo:Venues.deleteVenue ",
        errorMsg = pfx + "Error: Non-admin user attempted to delete venue";

    check(this.userId, String);

    if (!Roles.userIsInRole(this.userId, Gugo.roles.globalAdmin)) {
      console.warn(errorMsg, { userId: this.userId, venueId: venueId });
      throw new Meteor.Error("delete-venue-not-allowed", errorMsg);
    }

    console.log(pfx + "Deleting Venue", { userId: this.userId, venueId: venueId });

    Gugo.collections.venues.remove(venueId, function (err, docs) {
      if (err) {
        console.error(pfx + "Error deleting venue", { error: err, venueId: venueId });
      } else {
        console.log(pfx + "Successfully deleted venue", { docs: docs });
      }
    });
  }
});
