Package.describe({
  name: 'gugo:venues',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use([
    'meteor-platform',
    'gugo:core',
    'aldeed:collection2@2.7.0',
    'ground:db@0.3.14'
  ]);

  api.addFiles('alib/collection.js',
    ['client', 'server'],
    { transpile : false });

  api.addFiles([
    'lib/allow_deny.js',
    'lib/methods.js',
    'lib/validate.js',
    'lib/venueSchema.js',
    'lib/venues-helpers.js'
  ], ['client', 'server'], { transpile : false });

  api.addFiles('server/publication.js', ['server']);
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('gugo:venues');
  api.addFiles('gugo-venues-tests.js');
});
