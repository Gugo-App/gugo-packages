Gugo.collections.venues = new Mongo.Collection('venues');

if(Meteor.isServer) {
  Gugo.collections.venues._ensureIndex({ 'location.city' : 1});
  Gugo.collections.venues._ensureIndex({'location.geo.coordinates': 1});
  Gugo.collections.venues._ensureIndex({ name : "text" });
}