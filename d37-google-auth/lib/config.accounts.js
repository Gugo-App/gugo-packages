if(!(Meteor.settings.google.clientId && Meteor.settings.google.clientSecret )) {
  throw new Meteor.Error("404", "Google Client ID or Client Secret Were not Found");
}

var google = {
  clientId: Meteor.settings.google.clientId,
  clientSecret: Meteor.settings.google.clientSecret
};

Meteor.startup(function() {

  Accounts.loginServiceConfiguration.remove({
    service: "google"
  });

  Accounts.loginServiceConfiguration.insert({
    service: "google",
    clientId: google.clientId,
    secret: google.clientSecret
  });

});


//Accounts.onCreateUser(function(opts, user) {
//
//  if(user && user.services) {
//    if(user.services.google && user.services.google.accessToken) {
//      // Try to refresh the token
//      var toks = Meteor.http.call("POST",
//        "https://www.googleapis.com/oauth2/v4/token",
//        {
//          params: {
//            'client_id': google.clientId,
//            'client_secret': google.clientSecret,
//            'code': user.services.google.accessToken,
//            'grant_type': 'authorization_code'
//          }
//        });
//
//      if(!toks) {
//        throw new Meteor.Error(500, "Google Did not return a result");
//      }
//    }
//  }
//
//  if(opts && opts.profile) {
//    user.profile = opts.profile
//  }
//
//  return user;
//});