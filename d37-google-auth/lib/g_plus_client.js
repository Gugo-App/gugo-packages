GPlusOauth = {
  plugin:    null,
  getPlugin: function () {
    if (window && window.plugins && window.plugins.googleplus && _.isObject(window.plugins.googleplus)) {
      this.plugin = window.plugins.googleplus;
    }
  },
  logout:    function (callback) {
    this.plugin.logout(callback);
    this.plugin.disconnect();
  }
};

//Store the browser method given to us through accounts-google so we can overload it with out own personal method
var BrowserLoginWithGoogle = Meteor.loginWithGoogle;

//This is our Native Method
var NativeLoginWithGoogle = function (request, callback) {
  /*
   * @function cordova_g_plus
   * @summary Function to call native google plus login only available in cordova apps
   * @memberof Meteor
   * @param {Object} request an object with google plus login details
   * @param {Boolean} request.cordova_g_plus `request.cordova_g_plus` expected to be true to start native google plus login
   * @param {Array} request.profile Is an array of profile properties required, eg. `["email", "email_verified", "gender"]`
   * @param {Function} callback `callback` function can have one argument `error` which will be containing the details of error if any
   */

  console.log("NativeLoginWithGoogle request", JSON.stringify(_.clone(request)));

  var opt    = { offline: true };
  var apiKey = Meteor.settings.public
    && Meteor.settings.public.google
    && Meteor.settings.public.google.clientId;

  if (request.scopes) {
    opt.scopes = request.scopes.join(" ");
  }

  opt.androidApiKey = apiKey;
  opt.webApiKey = apiKey;

  console.log("NativeLoginWithGoogle opt:", JSON.stringify(_.clone(opt)));

  window.plugins.googleplus.login(opt, function (response) {
      console.log("window.plugins.googleplus.login response:", JSON.stringify(response));

      request.scopes && delete request.scopes;

      request.email        = response.email;
      request.oAuthToken   = response.oauthToken;
      request.sub          = response.userId;

      if (request.linkOnly) {
        delete request.linkOnly;
        Meteor.call('linkGPlus', request, callback);
      } else {
        Accounts.callLoginMethod({ // call cordova_g_plus SignIn handler @ server
          methodArguments: [request],
          userCallback:    callback
        });
      }
    },
    function (error) {
      if (callback && (typeof callback == "function")) {
        callback(error);
      } else {
        DialogController.showError({
          title : error
        });
      }
    }
  );
};

Meteor.loginWithGoogle = function (request, callback) {
  if (Meteor.isCordova) {
    NativeLoginWithGoogle(request, callback);
  } else {
    if (request.scopes) {
      request.requestPermissions = request.scopes;
      console.log("REQUEST", request);
    }

    request.requestOfflineToken = true;
    request.forceApprovalPrompt = true;

    request.cordova_g_plus && delete request.cordova_g_plus;

    function _intercept (err) {
      if (!err && !Meteor.user().profile.avatar) {
        Meteor.users.update(Meteor.userId(), {
          $set: {
            'profile.avatar': Meteor.user().services.google.picture
          }
        });
      }

      callback && callback(err);
    }

    BrowserLoginWithGoogle(request, _intercept);
  }
};

//Todo : Timeout in case it cant reach the server,
//Todo : Wait at least a second before making the div return to 0%
Meteor.loginWithGoogleSplash = function (options, callback) {
  console.log("LoginWithGoogleSPlash: Logging in with", JSON.stringify(options));
  showGoogleSplash();

  var opt            = _.clone(options);
  opt.cordova_g_plus = true;

  Meteor.setTimeout(function () {
    Meteor.loginWithGoogle(opt, function (error) {
      hideGoogleSplash();
      callback(error);
    });
  }, 1000);
};


var showGoogleSplash = function () {
  var elemDiv  = document.createElement('div');
  var gIconDiv = document.createElement('h1');

  gIconDiv.innerText     = "Google";
  gIconDiv.style.cssText = "position: absolute; top:50%; left: 50%; color:white; margin-right: -50%; transform: translate(-50%, -50%);";

  elemDiv.style.cssText = 'position:absolute;width:100%;height:0%;z-index:9999;background:#dd4b39;bottom:0; left:0';
  elemDiv.className     = "extend-top";
  elemDiv.id            = "google-splash";
  elemDiv.appendChild(gIconDiv);

  document.body.appendChild(elemDiv);
};

var hideGoogleSplash = function () {
  var el = $('#google-splash');
  el.removeClass('extend-top');
  el.addClass('shrink-bottom');
  Meteor.setTimeout(function () {
    el.remove();
  }, 1000)
};


Meteor.startup(function () {
  if (Meteor.isCordova) {
    GPlusOauth.getPlugin();
  }
});