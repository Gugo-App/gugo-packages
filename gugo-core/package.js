Package.describe({
  name: 'gugo:core',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.3.1');

  api.use([
    'ecmascript',
    'modules',
    '37degrees:utils',
    'dburles:collection-helpers@1.0.4',
    'reywood:publish-composite@1.4.2',
    'aldeed:collection2@2.7.0',
    'edgee:slingshot@0.7.1',
    'http'
  ]);

  api.imply([
    '37degrees:utils',
    'dburles:collection-helpers@1.0.4',
    'aldeed:collection2@2.7.0',
    'edgee:slingshot',
    'http'
  ]);

  api.addFiles([
    'lib/shared/main.js',
    'lib/shared/handlers.js',
    'lib/shared/helpers.js',
    "lib/shared/enums.js"
  ], ['client', 'server'], { transpile : false });

  api.addFiles([
    'lib/collections.js',
    'lib/addressSchema.js',
    'lib/geoSchema.js',
    'lib/locationSchema.js'
  ], ['client', 'server'], { transpile : false });

  api.addFiles([
    'client/helpers.js'
  ], 'client', { transpile : false });

  api.addFiles([
    'server/fixtures.js',
    'server/methods.js',
    'server/publications.js',
    'server/indexes.js'
  ], 'server', { transpile : false });

  api.export('Gugo', ['client', 'server']);

  api.mainModule('slingshot/slingshot.js');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('gugo:core');
  api.addFiles('core-tests.js');
});
