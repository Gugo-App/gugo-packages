Gugo.fixtures.stockImages = {
  load: function () {
    var url, existing;

    _.each(Gugo.fixtures.stockImages.items, function (item) {
      url = item.url;
      console.log(url);
      existing = Gugo.collections.stockImages.findOne({ url: url });
      if (!existing) {
        Gugo.collections.stockImages.insert(item, function (err) {
          if (err) {
            console.log("Gugo.fixtures.stockImages insert error", { error: err });
          } else {
            console.log("Inserted stockImages", item);
          }
        });
      }
    });
  },

  get amazonS3BucketUrl () {
    return "https://s3.amazonaws.com/" + Gugo.settings.amazonS3.bucket + "/stock/event/";
  },

  get items () {
    return [
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "ArtGallery.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Birthday.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Boating.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Cabin.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Camping.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Celebrate.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Coffee.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Concert.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "DinnerOutside.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Festival.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Fishing.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "GNO.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "HappyHour.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Hiking.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Kids.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "RoadTrip.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "RockClimbing.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Shower.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "SnowSports.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "StudyDate.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Summer.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Vacation.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Winter.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "WorkingOut.jpg" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Basketball.png" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Bicycle.png" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Fishing.png" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Hiking.png" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Kayaking.png" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Picnic.png" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Running.png" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Sailing.png" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "snowshoeing.png" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Soccer.png" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "swim.png" },
      { url: Gugo.fixtures.stockImages.amazonS3BucketUrl + "Yoga.png" }
    ];
  }
};
