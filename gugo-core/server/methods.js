Meteor.methods({
  loadStockImageFixtures: function () {
    Gugo.fixtures.stockImages.load();
  },
  eraseUserAccount : function(userId) {
    check(userId, String);

    var user = Meteor.users.findOne(userId);

    if(user) {
      Gugo.collections.activity.remove({ triggeredBy : userId });
      Gugo.collections.comments.remove({ ownerId : userId });
      Gugo.collections.calendarEvents.remove({ ownerId : userId });
      Gugo.collections.interestedEvents.remove({ ownerId : userId});
      Gugo.collections.contacts.remove({ ownerId : userId });

      const events = Gugo.collections.userEvents.find({ ownerId : userId }).fetch();

      _.each(events, function(event) {
        Meteor.call('deletePrivateEvent', event._id);
      });


      return Meteor.users.remove(userId);
    }
  },
  addRolesToUser : function (roles) {
    if (!this.userId) {
      return
    }
    Roles.addUsersToRoles(this.userId, roles);
  },
  //Target can be either a username (prepended with @) or a channel name (prepended with #)
  sendMessageToSlack : function(target, name, message) {
    this.unblock();

    var data = { text: message };

    target = target || "#webhooks";

    if (target && _.isString(target)) {
      data.channel = target;
    }

    if (name && _.isString(name)) {
      data.username = name;
    }

    var statURL = 'https://hooks.slack.com/services/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';

    console.log('[Slack.postToSlack]', { channel : target, url: statURL, data: data });

    HTTP.post(
      statURL,
      { data: data },
      function (error, result) {
        if (error) {
          //console.log("ERROR", error);
        } else {
          //console.log("SUCCESS", result);
        }
      }
    );
  }
});
