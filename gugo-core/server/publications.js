Meteor.publish('myUser', function () {
  if (this.userId) {
    var interested = Gugo.collections.interestedEvents.find({ ownerId : this.userId });
    var calendar = Gugo.collections.calendarEvents.find({ ownerId : this.userId });

    return [
      Meteor.users.find(this.userId),
      interested,
      calendar
      ];
    
  } else {
    return this.ready();
  }
});

Meteor.publish('singleUser', function(userId) {
  check(userId, String);
  return Meteor.users.find(userId, { fields : Gugo.helpers.users.fieldRestrictions() });
});

Meteor.publishComposite('interestedEventUsers', function(eventId) {
  check(eventId, String);

  return {
    find: function() {
      return Gugo.collections.interestedEvents.find({ events : eventId }, { fields : { 'ownerId' : 1, 'events' : 1 } })

    },
    children : [{
      find: function (eventHolder) {
        return Meteor.users.find(eventHolder.ownerId, { fields : Gugo.helpers.users.fieldRestrictions() });
      }
    }]
  }
});

Meteor.publishComposite('calendarEventUsers', function(eventId) {
  check(eventId, String);

  return {
    find: function() {
      return Gugo.collections.calendarEvents.find({ events : eventId}, { fields : { 'ownerId' : 1, 'events' : 1 } })
    },
    children : [{
      find: function (eventHolder) {
        return Meteor.users.find(eventHolder.ownerId, { fields : Gugo.helpers.users.fieldRestrictions() });
      }
    }]
  }
});

Meteor.publish('allStockImages', function() {
  return Gugo.collections.stockImages.find();
});