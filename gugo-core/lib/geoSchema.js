Gugo.schemas.GeoSchema = new SimpleSchema({
  coordinates: {
    type:     [Number],
    decimal:  true,
    minCount: 2,
    maxCount: 2,
    optional: true
  }
});
