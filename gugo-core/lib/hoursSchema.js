HoursSchema = new SimpleSchema({
	open: {
		type: Number
	},
	close: {
		type: Number
	}
});

DaySchema = new SimpleSchema({
	day: {
		type: Number,
		optional: false,
		hours: {
			type: [HoursSchema],
			optional: true
		}
	}
});

Gugo.schema.ScheduleSchema = new SimpleSchema({
	venueId: {
		type: String,
		optional: false
	},
	days: {
		type: [DaySchema],
		optional: true
	}
});