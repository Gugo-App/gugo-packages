// 50 states + "DC"
Gugo.data.states = ["AL","AK","AZ","AR","CA","CO","CT","DC","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"];

Gugo.schemas.AddressSchema = new SimpleSchema({
  fullAddress : {
    type : String,
    optional : true
  },
  street : {
    type : String,
    optional : true
  },
  city : {
    type : String,
    optional : true
  },
  state : {
    type : String,
    optional : true,
    allowedValues : Gugo.data.states
  },
  zip : {
    type : String,
    optional : true,
    regEx : SimpleSchema.RegEx.ZipCode
  }
});
