Gugo.helpers.location = {
  getLocationOrNull: function (location) {
    var result = null;

    if (location) {
      if (location.address && !_.isEmpty(location.address)) {
        result = {
          address: location.address
        }
      }
      if (location.geo && location.geo.coordinates && location.geo.coordinates.length === 2) {
        result.location = result.location || {};
        result.location.geo = location.geo;
      }
    }
  }
};

Gugo.helpers.users = {
    fieldRestrictions : function() {
        return {
            'profile.avatar' : 1,
            'profile.name' : 1,
            'phone' : 1,
            '_id' : 1
        }
    }
}
