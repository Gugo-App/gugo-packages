
/*
* How To Use :
* bind this to a mongo callback with options for consistent functionality
* options : { msg : "Coll name" callback : yourCallbackFunction }
* ex collection.insert(query, Gugo.handlers.mong.bind(this, { msg : "collectionName" callback : callbackFunc});
*/
Gugo.handlers.mongo = function(options, err, docs) {
  if(err) {
    console.log("Mongo Error: ", err);
    var err = new Meteor.Error('mongo-error', err);
    throw err;
  } else {
    var isNum = parseInt(docs);
    if(_.isNumber(isNum)) {
      console.log("Mongo Updated", options.msg, docs);
    } else {
      console.log("Mongo inserted", options.msg, docs);
    }
  }

  options.callback && options.callback(err, docs)
};