Gugo = {
  collections: {
    events:                  null,
    recurringEvents :        null,
    userEvents:              null,
    messages:                null,
    comments:                null,
    suggestedDates:          null,
    suggestedLocations:      null,
    calendarEvents:          null, // These are events that you have added to your calendar through the add button
    interestedEvents:        null, // These are events that you have expressed interest in through the interest button
    activity:                null,
    contacts:                null,
    networks:                null,
    eventInvitations:        null,
    eventNetworkInvitations: null,
    eventRoles:              null,
    venues:                  null,
    userBlacklist :   null,
    eventReportList : null,
    userReportList : null,
    commentReportList : null,
    helpers:                 {}  // These are for any collection helpers that are needed
  },
  enums: {},
  components:  {},
  data:        {},
  errors:      {},
  fixtures:    {},
  handlers:    {},
  helpers:     {},
  schemas:     {},
  roles:       {
    globalAdmin: "global-admin"
  },
  router:      {},
  settings:    {},
  timezone:    {
    Pacific: "America/Los_Angeles"
  },
  validate:    {}
};
