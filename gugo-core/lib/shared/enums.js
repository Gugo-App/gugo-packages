Gugo.enums = {
	snippetDefaults: [
		{text: "All Ages", value: 0},
		{text: "Bar with ID", value: 1},
		{text: "21+ Only", value: 2},
		{text: "Will Call", value: 3},
		{text: "Pay Cash at Door", value: 4},
		{text: "Free", value: 5},
		{text: "RSVP Required", value: 6}
	]
}