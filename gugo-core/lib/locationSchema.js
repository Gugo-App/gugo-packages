Gugo.schemas.LocationSchema = new SimpleSchema({
  name : {
    type : String,
    optional : true
  },
  geo : {
    type : Gugo.schemas.GeoSchema,
    optional : true
  },
  address : {
    type : Gugo.schemas.AddressSchema,
    optional : true
  }
});
