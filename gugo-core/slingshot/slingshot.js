import initSettings from './settings';
import setupDirectives from './setup';

export default function initializeSlingshot() {
    console.log("Setup directives called");
    initSettings();
    setupDirectives();
}