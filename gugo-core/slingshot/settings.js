export default function initSettings() {

  Gugo.settings.upload = {
    tileImage: {
      limitInKB: d37utils.config.getNumber("public.upload.tileImage.limitInKB")
    },
    heroImage: {
      limitInKB: d37utils.config.getNumber("public.upload.heroImage.limitInKB")
    }
  };

  if(Meteor.isServer) {
    Gugo.settings.amazonS3 = {
      bucket: d37utils.config.getString("amazonS3.bucket"),
      region: d37utils.config.getString("amazonS3.region"),
      uploadUser: {
        accessKeyId: d37utils.config.getString("amazonS3.uploadUser.accessKeyId"),
        secretAccessKey: d37utils.config.getString("amazonS3.uploadUser.secretAccessKey")
      }
    }
  }
}