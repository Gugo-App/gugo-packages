export default function setupDirectives() {
  var directives = ["tileImageUpload", "heroImageUpload", "userAvatarUpload", 'userEventImageUpload'];

  if (typeof Slingshot !== "undefined" && Slingshot) {

    Slingshot.fileRestrictions("tileImageUpload", {
      allowedFileTypes: ["image/png", "image/jpeg", "image/gif"],
      maxSize: 1024 * Gugo.settings.upload.tileImage.limitInKB // use null for unlimited
    });
    Slingshot.fileRestrictions("heroImageUpload", {
      allowedFileTypes: ["image/png", "image/jpeg", "image/gif"],
      maxSize: 1024 * Gugo.settings.upload.heroImage.limitInKB // use null for unlimited
    });
    Slingshot.fileRestrictions("userAvatarUpload", {
      allowedFileTypes: ["image/png", "image/jpeg", "image/gif"],
      maxSize: 1024 * Gugo.settings.upload.heroImage.limitInKB // use null for unlimited
    });
    Slingshot.fileRestrictions("userEventImageUpload", {
      allowedFileTypes: ["image/png", "image/jpeg", "image/gif"],
      maxSize: 1024 * Gugo.settings.upload.heroImage.limitInKB // use null for unlimited
    });

    if (Meteor.isServer) {
      // Setup Slingshot for file image upload directives
      _.each(directives, function (directive) {

        Slingshot.createDirective(directive, Slingshot.S3Storage,
          {
            bucket: Gugo.settings.amazonS3.bucket,
            region: Gugo.settings.amazonS3.region,
            acl: "public-read",
            AWSAccessKeyId: Gugo.settings.amazonS3.uploadUser.accessKeyId,
            AWSSecretAccessKey: Gugo.settings.amazonS3.uploadUser.secretAccessKey,
            authorize: function () {
              // do some validation
              // e.g. deny uploads if user is not logged in.
              if (!this.userId) {
                throw new Meteor.Error(403, "Login Required");
              }
              return true;
            },
            key: function (file, meta) {
              var filename;

              console.log("directive file:", file);
              console.log("directive meta:", meta);

              // ensure we have a file and partner ID
              if (file && file.name && meta && meta.type && meta.id && meta.size) {
                filename = meta.type + "/" + meta.id + "/" + meta.size + "/" + file.name;
              } else {
                console.error("Slingshot upload error setting file name - missing params", {file: file, meta: meta});
              }

              return filename;
            }
          }
        );
      });

    }
  }
};
