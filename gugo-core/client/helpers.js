Gugo.helpers.user = {
  getUser: function () {
    var user = Meteor.user();
    return user && user.profile ? user.profile : null;
  },
  getProfile: function () {
    var user = Meteor.user();
    return user && user.profile ? user.profile : null;
  }
};
