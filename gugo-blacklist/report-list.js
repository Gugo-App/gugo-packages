Gugo.collections.eventReportList = new Mongo.Collection('event-report-list');

EventReportList = {
  add : function(eventId) {
    var user = Meteor.user();

    if(Meteor.isClient) {
      SnackBarController.show("Event has been reported");
    }

    return Gugo.collections.eventReportList.insert({
      eventId : eventId,
      reportedBy : user._id
    })
  }
};

Gugo.collections.eventReportList.allow({
  insert : function() {
    return true;
  }
});


Gugo.collections.userReportList = new Mongo.Collection('user-report-list');

UserReportList = {
  add : function(userId) {
    var user = Meteor.user();

    console.log("Adding", userId);

    if(Meteor.isClient) {
      SnackBarController.show("User has been reported");
    }

    return Gugo.collections.userReportList.insert({
      userId : userId,
      reportedBy : user._id
    })
  }
};

Gugo.collections.userReportList.allow({
  insert : function() {
    return true;
  }
});

Gugo.collections.commentReportList = new Mongo.Collection('comment-report-list');

CommentReportList = {
  add : function(commentId) {
    var user = Meteor.user();

    console.log("Adding", commentId);

    if(Meteor.isClient) {
      SnackBarController.show("Comment has been reported");
    }

    return Gugo.collections.commentReportList.insert({
      commentId : commentId,
      reportedBy : user._id
    })
  }
};

Gugo.collections.commentReportList.allow({
  insert : function() {
    return true;
  }
});



