Gugo.collections.userBlacklist = new Mongo.Collection('user-blacklist');

Blacklist = {
  add : function(blocked, blocker) {
    if(!blocker) {
      var user = Meteor.user();
      if(!user) {
        throw new Meteor.Error('Did nto provide a phone number and this is not originating from a user')
        return;
      }

      blocker = user.phone;
    }

    return Meteor.call('addBlacklistEntry', blocked, blocker, function(err, res) {
      console.log("Added blacklist entry", err, res);
    });
  },
  remove : function(blocked) {
    Meteor.call('removeBlacklistEntry', blocked);
  },
  check : function(blocked, blocker) {
    if(!blocker) {
      var user = Meteor.user();
      if(!user) {
        throw new Meteor.Error('Did nto provide a phone number and this is not originating from a user')
        return;
      }

      blocker = user.phone;
    }

    if(Meteor.isClient) {
      return Meteor.call('checkBlacklistForEntry', blocked, blocker);
    } else {
      return Gugo.collections.userBlacklist.findOne({ blocked : blocked, blocker : blocker });
    }
  }
};

if(Meteor.isServer) {
  Meteor.methods({
    addBlacklistEntry : function(blocked, blocker) {
      var blockedPhone = null;
      var blockedUser = null;

      if(blocked.phone) {
        blockedPhone = blocked.phone;
        blockedUser = Meteor.users.findOne({ phone : blocked.phone });
      } else if(blocked.userId) {
        blockedUser = Meteor.users.findOne(blocked.userId);
        blockedPhone = blockedUser.phone;
      }

      const exists = Gugo.collections.userBlacklist.findOne({
        blocker : blocker,
        blocked : blockedPhone
      });

      if(exists) return true;

      if(blockedUser) {
        Meteor.call('removeInvitationsForUser', blockedUser._id, {phone: blocker});
        Meteor.call('removeAllBlacklistedCalendarEvents', blockedUser._id)
        Meteor.call('removeAllBlacklistedInterestedEvents',  blockedUser._id)
      }

      return Gugo.collections.userBlacklist.insert({
        blocker : blocker,
        blocked : blockedPhone
      })
    },
    removeBlacklistEntry : function(blocked, blocker) {
      var user = Meteor.users.findOne(this.userId);
      Gugo.collections.userBlacklist.remove({ blocked : blocked, blocker : user.phone });
    },
    checkBlacklistForEntry : function(blocked, blocker) {
      var blockedPhone = null;

      if(blocked.phone) {
        blockedPhone = blocked.phone;
      } else if(blocked.userId) {
        const user = Meteor.users.findOne(blocked.userId);
        blockedPhone = user.phone;
      }

      return Gugo.collections.userBlacklist.findOne({ blocked : blockedPhone, blocker : blocker });
    }
  })

  Meteor.publish('myUserBlacklist', function() {
    this.unblock();
    if(!this.userId) return this.ready();

    var user = Meteor.users.findOne(this.userId);

    return Gugo.collections.userBlacklist.find({ blocker : user.phone });
  });

  Meteor.publishComposite('myUserBlacklistUsers', function() {
    if(!this.userId) return this.ready();

    var user = Meteor.users.findOne(this.userId);

    return {
      find : function() {
        //console.log("Event Invitations query", user.phone);
        return Gugo.collections.userBlacklist.find({ blocker : user.phone })
      },
      children : [
        {
          find : function(blacklistEntry) {
            return Meteor.users.find({ phone : blacklistEntry.blocked }, { limit : 1, fields : Gugo.helpers.users.fieldRestrictions() })
          }
        }
      ]
    }

  });

  Gugo.collections.userBlacklist._ensureIndex({ blocker : 1 })
}