Package.describe({
  name:          'gugo:user-blacklist',
  version:       '0.0.1',
  // Brief, one-line summary of the package.
  summary:       '',
  // URL to the Git repository containing the source code for this package.
  git:           '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function (api) {
  api.versionsFrom('1.2.1');

  api.use([
    'ecmascript',
    'meteor-platform',
    'underscore',
    'gugo:core'
  ]);

  api.addFiles(['user-blacklist.js', 'report-list.js']);

  api.export(['Blacklist', 'EventReportList', 'UserReportList', 'CommentReportList']);
});

Package.onTest(function (api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('gugo:user-blacklist');
  api.addFiles('user-blacklist-tests.js');
});
