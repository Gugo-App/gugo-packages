var Future = Npm.require('fibers/future');

Meteor.methods({
    linkFB : function(loginRequest) {
        var user = null,
            future = new Future();

        if(!this.userId) {
            throw new Meteor.Error("not-found", "User ID not found");
        } else {
            user = Meteor.users.findOne(this.userId);
            if(!user) {
                throw new Meteor.Error("not-found", "User Not Found");
            }
        }

        var whitelisted = ['id', 'email', 'name', 'first_name',
                           'last_name', 'link', 'gender', 'locale', 'age_range'];

        var identity = getIdentity(loginRequest.accessToken, whitelisted);

        var profilePicture = getProfilePicture(loginRequest.accessToken);

        console.log(profilePicture);

        var serviceData = {
            accessToken: loginRequest.accessToken,
            expiresAt: (+new Date) + (1000 * loginRequest.expiresIn)
        };

        var profile = Meteor.users.findOne(this.userId).profile;

        if(identity.name && !profile.name) {
            profile.name = identity.name;
        }

        if(profilePicture && !profile.avatar) {
            profile.avatar = profilePicture;
        }

        serviceData.picture = profilePicture;


        var fields = _.pick(identity, whitelisted);
        _.extend(serviceData, fields);

        Meteor.users.update(this.userId, {
            $set : {
                'services.facebook' : serviceData,
                'profile' : profile
            }
        }, function(err, docs) {
            if(err) {
                throw new Meteor.Error("Mongo Error", err)
            }

            future.return(this.userId);
        });

        return future.wait();
    },
    getFacebookExtras : function() {
        check(this.userId, String);

        var user = Meteor.users.findOne(this.userId);

        var whitelisted = ['id', 'email', 'name', 'first_name',
            'last_name', 'link', 'gender', 'locale', 'age_range'];

        var id = getIdentity(user.services.facebook.accessToken, whitelisted);
        var picture = getProfilePicture(user.services.facebook.accessToken);

        if(picture) {
            id.avatar = picture;
        }

        console.log("Got id", id, "got picture", picture);

        if(!user.profile.avatar) {
            Meteor.users.update(this.userId, {
                $set : {
                    'profile.avatar' : picture
                }
            })
        }

        var currentFacebook = user.services.facebook;
        var extend = _.extend(id, currentFacebook);

        Meteor.users.update(this.userId, {
            $set : {
                'services.facebook' : extend
            }
        });
    }
});

var getIdentity = function (accessToken, fields) {
    try {
        return HTTP.get("https://graph.facebook.com/me", {
            params: {
                access_token: accessToken,
                fields: fields
            }
        }).data;
    } catch (err) {
        throw _.extend(new Error("Failed to fetch identity from Facebook. " + err.message),
            {response: err.response});
    }
};

var getProfilePicture = function (accessToken) {
    try {
        return HTTP.get("https://graph.facebook.com/v2.0/me/picture/?redirect=false", {
            params: {access_token: accessToken}}).data.data.url;
    } catch (err) {
        throw _.extend(new Error("Failed to fetch identity from Facebook. " + err.message),
            {response: err.response});
    }
};