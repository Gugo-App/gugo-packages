FBUtils = {
  hasFacebookServices : function(user) {
    return user && user.services && user.services.facebook;
  }
};
