var origLogout = Meteor.logout;

Meteor.logout = function(myCallback) {
  var user = Meteor.user(),
    callback = myCallback || function() {};

  console.log("Started logout");

  if(!user) return origLogout(callback);
  if(!Meteor.isCordova) return origLogout(callback);

  var services = user.services;

  if(!services) return origLogout(callback);

  console.log("Checking Cordova Meteor Login Methods for User", Meteor.user());
  console.log("plugin" + facebookConnectPlugin + "googservices"  + FBUtils.hasFacebookServices(user));
  if(facebookConnectPlugin && FBUtils.hasFacebookServices(user)) {
    if(facebookConnectPlugin && services.facebook) {
      console.log("We have a facebook login, lets logout");

      facebookConnectPlugin.logout(function() {
        return origLogout(callback);
      }, function() {
        return origLogout(callback);
      });
    }
  } else {
    return origLogout(callback);
  }
};