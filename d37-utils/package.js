Package.describe({
  name:          '37degrees:utils',
  version:       '0.0.1',
  // Brief, one-line summary of the package.
  summary:       '',
  // URL to the Git repository containing the source code for this package.
  git:           '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function (api) {
  api.versionsFrom('1.2.1');

  api.use([
    'ecmascript',
    'underscore',  //TODO: use NPM when migrated to Meteor 1.3
    'momentjs:moment@2.10.6',
    'risul:moment-timezone@0.4.1',
    'joshowens:timezone-picker@0.1.2',
    'u2622:persistent-session@0.4.4'
  ]);

  api.imply([
    //'stevezhu:lodash',
    'momentjs:moment',
    'risul:moment-timezone',
    'joshowens:timezone-picker',
    'u2622:persistent-session'
  ]);

  api.addFiles([
    'lib/init/utilsInit.js',
    'lib/init/_mixins.js'
  ], ['client', 'server']);

  api.addFiles([
    'lib/config.js',
    'lib/date.js',
    'lib/number.js',
    'lib/text.js'
  ], ['client', 'server']);

  api.export('d37utils', ['server', 'client']);
});

Package.onTest(function (api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('utils');
  api.addFiles('utilsTests.js');
});
