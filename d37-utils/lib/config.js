d37utils.config = {
  pfx: "[d37utils.config] ",

  getObject: function (path, errorIfMissing) {
    if (!Meteor.settings) {
      console.log(this.pfx + "getString ERROR: Meteor.settings not found!");
      throw new Meteor.Error('app-config-error', 'getObject ERROR: Meteor.settings not found!');
    }

    var obj = Meteor.settings;

    // http://stackoverflow.com/questions/6491463/accessing-nested-javascript-objects-with-string-key
    path = path.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    path = path.replace(/^\./, '');           // strip a leading dot
    var a = path.split('.');
    while (a.length) {
      var n = a.shift();
      if (n in obj) {
        obj = obj[n];
      } else {
        console.log(this.pfx + "getObject path not found: " + path);
        if (errorIfMissing) {
          throw new Meteor.Error('app-config-error', 'getObject path not found: ' + path);
        }
        return null;
      }
    }

    //console.log(this.pfx + "getObject for: " + path);
    return obj;
  },

  getBoolean: function (path, errorIfMissing) {
    var value = this.getObject(path, errorIfMissing);
    if (value === null) {
      return false;
    } else if (typeof value === 'string') {
      return value.toLowerCase() === 'true';
    } else if (typeof value === Boolean) {
      return value;
    }
    return false;
  },

  getString: function (path, errorIfMissing) {
    var obj = this.getObject(path, errorIfMissing);
    if (obj) {
      try {
        obj = obj.toString();
      }
      catch (err) {
        console.log(this.pfx + 'getString error: ' + err);
      }
      if (obj && typeof obj === 'string') {
        return obj;
      }
    }
    return null;
  },

  getNumber: function (path, errorIfMissing) {
    var obj = this.getObject(path, errorIfMissing);
    if (obj && _.isNumber(obj)) {
      return obj;
    }
    return null;
  },

  get isProdEnvironment () {
    if (Meteor.isServer) {
      if (typeof process !== 'undefined' && process.env && process.env.NODE_ENV) {
        return process.env.NODE_ENV.toLowerCase() !== 'development'
      }
      throw Meteor.Error(this.pfx + 'ERROR: "process.env.NODE_ENV" not found.');
    }
    throw Meteor.Error(this.pfx + 'ERROR: "isProdEnvironment" is not available to client.');
  },

  get isDevEnvironment () {
    return !this.isProdEnvironment;
  }

};