_.mixin({
  skipTake:        function (array, options) {
    options = _.extend({ skip: 0, take: 0 }, options || {});

    return _(array)
      .chain()
      .rest(options.skip)
      .first(options.take || array.length - options.skip)
      .value();
  },
  propOrNull:      function (obj, property) {
    return _.get(obj, property, null);
  },
  isEqualStringValue: function (value1, value2, isCaseInsensitive = false) {
    if (_.isString(value1) && _.isString(value2)) {

      if (isCaseInsensitive) {
        return value1.toLowerCase() === value2.toLowerCase();
      }

      return value1 === value2;
    }

    return false;
  }
});
