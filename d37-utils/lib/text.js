import Similarity from 'similarity';

d37utils.text = {
  //Please don't edit any of this, was a complete pain to write
  urlify : function(text) {
    if(!text) return '';

    return text;

    var urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, function(url) {
      if(Meteor.isCordova && cordova && cordova.InAppBrowser) {
        return '<a href="#" onClick=cordova.InAppBrowser.open("' + url + '"' + ',' + '"_blank"' + ')' + '  >' + url + '</a>';
      } else {
        return '<a href="#" onClick=window.open("' + url  + '"' + ',' + '"_system"' + ')' + '  >' + url + '</a>';
      }
    });
  },
  //Sorts objects by textScore var, which is obtained by getDistanceForObjects
  sortByTextScore : function(input) {
    var sorted = null;

    if(_.isArray(input)) {
      if(_.isObject(input[0])) {
        sorted = _.sortBy(input, 'textScore');
      } else {
        console.error("Error, Object Sorting is only supported");
      }
    }

    return sorted;
  },
  getDistanceForObjects : function(textObjects, key, wordComparison) {
    var self = this;

    return textObjects.map( obj => {
      if(typeof obj[key] !== 'undefined') {
        obj.textScore = self.getDistance(obj[key], wordComparison);
        return obj;
      }
    })
  },
  //Get the levenshtein difference of two pieces of text
  getDistance : function(text1, text2) {
    return Similarity(text1, text2);
  },
  //Takes a string and adds <b> </b> tags around the aforementioned subtext
  highlightSubtext : function(text, subtext) {
    var match = new RegExp(subtext, "i");
    var replaced = text.replace(match, '<b>' + subtext + '</b>')

    //console.log("Match", match, "Replaced", replaced);

    return replaced;
  }
};

