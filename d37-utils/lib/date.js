d37utils.date = {
  getStartOfDay:    function (date, timezone) {
    date = date || new Date();
    return new Date(d37utils.date.moment.getStartOfDay(date, timezone));
  },
  getEndOfDay:      function (date, timezone) {
    date = date || new Date();
    return new Date(d37utils.date.moment.getEndOfDay(date, timezone));
  },
  getDateFromToday: function (valueOut, unitsOut, timezone) {
    return new Date(d37utils.date.moment.getDateFromToday(valueOut, unitsOut, timezone));
  },
  getTimezone:      function () {
    if (TimezonePicker && TimezonePicker.detectedZone) {
      return TimezonePicker.detectedZone();
    }

    //TODO: replace this with a setting?
    return "America/Los_Angeles";
  },

  getHoursFromNow: function (hours, timezone) {
    return new Date(d37utils.date.moment.getHoursFromNow(hours, timezone));
  },

  getMonthName: function (date) {
    if (date) {
      return d37utils.date.monthNames[date.getMonth()];
    }
    return null;
  },

  // region Moment-specific methods
  moment: {
    getLocalDate:     function (date, timezone) {
      timezone = timezone || d37utils.date.getTimezone();
      return moment(date).tz(timezone);
    },
    getFormatted:     function (date, format, timezone) {
      var dt = d37utils.date.moment.getLocalDate(date, timezone);
      return dt.format(format);
    },
    getStartOfDay:    function (date, timezone) {
      date     = date || new Date();
      timezone = timezone || d37utils.date.getTimezone();
      //console.log("Date", date, "Timezone", timezone);
      return moment(date).tz(timezone).startOf("day");
    },
    getEndOfDay:      function (date, timezone) {
      date     = date || new Date();
      timezone = timezone || d37utils.date.getTimezone();
      return moment(date).tz(timezone).endOf("day");
    },
    getDateFromToday: function (valueOut, unitsOut, timezone) {
      if (!valueOut || !unitsOut) {
        return null;
      }

      timezone = timezone || d37utils.date.getTimezone();
      return moment().tz(timezone).add(valueOut, unitsOut);
    },
    getHoursFromNow: function (hours, timezone) {
      let now = d37utils.date.moment.getLocalDate(timezone);
      return now.add(hours, 'hours');
    }
    //endregion
  }
};

Object.defineProperty(d37utils.date, 'monthNames', {
  get: function () {
    return ["January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December"];
  }
});

Object.defineProperty(d37utils.date, 'dayNames', {
  get: function () {
    return ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
  }
});
