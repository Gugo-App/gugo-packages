d37utils.number = {
  randomNumber: function (min, max) {
    return Math.random() * (max - min) + min;
  },

  randomInt: function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },

  randomIntArray: function (min, max, count, allowDuplicates = true) {
    var array = [],
        int;

    while (array.length < count) {
      int = d37utils.number.randomInt(min, max);
      if (allowDuplicates || !_.includes(array, int)) {
        array.push(int);
      }
    }

    return array;
  },

  convertMS : function(ms) {
    var d, h, m, s;
    s = Math.floor(ms / 1000);
    m = Math.floor(s / 60);
    s = s % 60;
    h = Math.floor(m / 60);
    m = m % 60;
    d = Math.floor(h / 24);
    h = h % 24;
    return { d: d, h: h, m: m, s: s };
  },

  getRandomArrayItem: function (array) {
    var idx;
    array = array || [];
    idx   = d37utils.number.randomInt(0, array.length - 1);
    return array[idx];
  },

  getNumericTotal: function () {
    var args, total = 0;
    if (arguments) {
      args = Array.prototype.slice.call(arguments, 0);
      for (var i = 0; i < args.length; i++) {
        total += d37utils.number.getNumberOrZero(args[i]);
      }
    }
    // TODO: this rounds 0.005 to 0.01 --- is this the desired rounding for currency?
    return d37utils.number.getNumericValue(total);
  },

  getNumberOrZero: function (value) {
    var number = d37utils.number.getNumberOrNull(value);
    return number || 0;
  },

  getNumberOrNull: function (value) {
    if (isNaN(value)) {
      return null;
    }
    var number = parseFloat(value);
    return isNaN(number) ? null : number;
  },

  getNumericValue: function (value, roundTo) {
    var amount = 0;

    if (d37utils.number.isNumber(value)) {
      amount = d37utils.number.getNumberOrZero(value);
    }

    // TODO: verify rounding for currency
    if (d37utils.number.isNumber(roundTo)) {
      return Math.round10(amount, roundTo);
    }

    return amount;
  },

  isNumber: function (value) {
    return !isNaN(value);
  }


};
