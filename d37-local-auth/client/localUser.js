d37localAuth.createLocalUser = function (callback) {
  var pfx      = "[d37localAuth.createLocalUser] ",
      location = Location && Location.getLastPosition ? Location.getLastPosition() : null,
      profile,
      deviceId,
      userId;

  // First clear all data for current session
  d37localAuth.clearSession();

  profile = {
    'device':   d37mobile.device,
    'name':     'Guest',
    'timezone': TimezonePicker.detectedZone()
  };

  deviceId = d37mobile.device.uuid;

  console.log(pfx + "Meteor.localLogin", { deviceId: deviceId, profile: profile });

  Meteor.localLogin(deviceId, profile, function (err) {
    if (err) {
      console.error(pfx + "Meteor.localLogin Error",
        { error: err, deviceId: deviceId, profile: profile, location: location });
      callback && callback(err);
      return;
    }

    // Ensure we have a Meteor user
    userId = Meteor.userId();
    if (!userId) {
      console.error(pfx + 'Meteor.localLogin: No user ID returned', { profile: profile, location: location });
      callback && callback(err);
      return;
    }

    Meteor.call("addRolesToUser", ['guest']);

    if(Gugo.settings.env === "production") {
      const msg = "New User: " + "Guest" + " _id: " + Meteor.userId();
      Meteor.call('sendMessageToSlack', "#new_users", "Users Bot", msg);
    }

    console.log(pfx + "Meteor.localLogin called", { deviceId: deviceId, profile: profile });

    callback && callback();
  });
};
