//// This is for logging in "local" users
// This needs to be fixed to not accept all login requests
//  - this should work now that it returns undefined
//Accounts.registerLoginHandler(function (loginRequest) {
//  if (!loginRequest.local) {
//    console.log("NOT a Guest login request", loginRequest);
//    return undefined;
//  }
//
//  console.log("Guest login request", loginRequest);
//
//  var user = Meteor.users.find({
//    _id: loginRequest.username
//  });
//  if (user) {
//    return user._id;
//  }
//
//  console.log("Inserting guest user");
//  var userId = Accounts.insertUserDoc({
//    username: loginRequest.username
//  });
//
//  Meteor.users.update(userId, {
//    $set: { profile: loginRequest.profile }
//  }, function (err, docs) {
//    if (err) {
//      throw new Meteor.Error("Mongo Error", err)
//    }
//  });
//
//  return {
//    userId: userId
//  };
//});
