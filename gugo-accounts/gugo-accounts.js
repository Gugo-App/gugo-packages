//import Meteor from 'meteor/meteor';
import { doesAccountExist } from './methods';


function sanitizeLoginError(err) {
  switch(err.error) {
    case 403 :
      return "Your email or password did not match";
  }
}


export const name = 'gugo-accounts';

export default GugoAccounts = {
  login({ email, password, phone }, callback) {
    console.log("Accounts, login", email, password, phone);

    if(email && password) {
      Meteor.loginWithPassword({ email : email }, password, function(err) {
        if(err) {
          var sanitizedError = sanitizeLoginError(err);
          console.log("Error", sanitizedError);
          callback && callback(sanitizedError);
          return;
        }

        callback && callback();
      })
    } else if (phone) {

    }
  },
  register({ email, password, phone }, callback) {
    if(email && password) {
      var account = doesAccountExist(email);
      if(account.exists) {
        callback && callback("An account with that email already exists, please login to continue")
      }

    } else if(phone) {

    } else {
      callback && callback("You must supply an email, password or phone number, please try again");
      throw new Meteor.Error('invalid-params', 'Must supply an email & password or a phone');
      return false;
    }

  }
};

