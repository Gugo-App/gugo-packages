Package.describe({
  name: 'gugo:accounts',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.3.1');
  api.use(['ecmascript', 'accounts-base', 'accounts-password', 'alanning:roles@1.2.15', 'mdg:validated-method@1.1.0', 'aldeed:simple-schema@1.5.3']);
  api.mainModule('gugo-accounts.js');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('gugo:accounts');
  api.mainModule('accounts-tests.js');
});
