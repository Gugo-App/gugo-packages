import { Accounts } from 'meteor/accounts-base';

export const doesAccountExist = new ValidatedMethod({
  name : 'GugoAccounts.methods.doesAccountExist',
  validate : new SimpleSchema({
    email : { type : String }
  }).validator(),
  run({ email }) {
    const emailExists = Accounts.findUserByEmail(email);
    //This will be email / phone / facebook / etc
    const accountType = "email";

    if(emailExists) {
      return {
        exists : true,
        accountType : accountType
      }
    } else {
      return false;
    }
  }
});