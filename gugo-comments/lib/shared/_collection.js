Gugo.collections.comments = new Mongo.Collection('comments');

if(Meteor.isServer) {
  Gugo.collections.comments._ensureIndex({ eventId : 1});
}