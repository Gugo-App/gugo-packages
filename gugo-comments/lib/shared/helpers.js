function convertMS(ms) {
  var d, h, m, s;
  s = Math.floor(ms / 1000);
  m = Math.floor(s / 60);
  s = s % 60;
  h = Math.floor(m / 60);
  m = m % 60;
  d = Math.floor(h / 24);
  h = h % 24;
  return { d: d, h: h, m: m, s: s };
};

Gugo.collections.comments.helpers({
  timeSinceCommented : function() {
    const now = new Date();
    const minutesToShowMins = 59;

    const converted = convertMS(now.getTime() - this.createdAt.getTime());

    console.log("Converted", converted);

    //Show minutes if the time is under 10 minutes, show time stamp if after
    if(converted.d === 0 && converted.h === 0) {
        if(converted.m > minutesToShowMins) {
          return moment(this.createdAt.getTime).format("h:mmaa")
        } else {
          return converted.m <= 0 ? "now" : converted.m + " minutes ago";
        }
    } else {
      return moment(this.createdAt).format("h:mm A");
    }
  }
});