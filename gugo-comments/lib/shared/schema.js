Gugo.schemas.CommentsSchema = new SimpleSchema({
  createdAt : {
    type : Date,
    autoValue: function() {
      if(this.isInsert) {
        return new Date();
      }
    }
  },
  text : {
    type : String
  },
  eventId : {
    type : String
  },
  userId : {
    type : String
  }
});

Gugo.collections.comments.attachSchema(Gugo.schemas.CommentsSchema);