Meteor.methods({
  addComment : function(eventId, text) {

    Gugo.collections.comments.insert({
      eventId : eventId,
      text : text,
      userId : this.userId
    }, function(err, docs) {
      console.log("Inserted doc", docs, "err", err);

      Meteor.call('newActivity', { eventId : eventId, userId : this.userId, type :  ActivityTypes.NEW_COMMENT });
    })
  }

});