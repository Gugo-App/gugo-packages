Meteor.publishComposite('eventComments', function(eventId) {
  check(eventId, String);

  return {
    find: function() {
      return Gugo.collections.comments.find({ eventId : eventId });
    },
    children : [{
      find: function (comment) {
        return Meteor.users.find(comment.userId, { fields : Gugo.helpers.users.fieldRestrictions() });
      }
    }]
  }
});

