Gugo.collections.suggestedDates.helpers({
  fullDateTime: function() {
    const start = this.startDate;
    const end = this.endDate;
    const allDay = this.isAllDay;

    var str = "TBA";

    if(!start) return str;

    console.log("Start", start, "end", end);

    if(allDay) {
      if (start.getDate() === end.getDate() && start.getMonth() === end.getMonth()) {
        str = moment(start).format("MMM Do") + " All Day";
      } else if(start.getMonth() === end.getMonth()) {
        str = moment(start).format("MMM Do") + moment(end).format(" - Do") + " All Day";
      } else {
        str = moment(start).format("MMM Do") + moment(end).format(" - MMM Do") + " All Day";
      }
    } else {
      if (start && !end) {
        str = moment(start).format("MMM Do, hh:mm A") + " - TBA";
      } else if (start.getDate() === end.getDate()) {
        str = moment(start).format("MMM Do, hh:mm A") + " - " + moment(end).format("hh:mm A");
      } else {
        str = moment(start).format("MMM Do, hh:mm A") + " - " + moment(end).format("MMM Do, hh:mm A");
      }
    }

    return str;
  },
  fullStartDateTime: function() {
    if(!this.startDate) {
      return "TBA";
    }

    return moment(this.startDate).format("dddd MMMM D, h:mm A")
  }
});