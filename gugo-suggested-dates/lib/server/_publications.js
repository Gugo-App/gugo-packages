Meteor.publish('eventSuggestedDates', function(eventId) {
  console.log("Event Id", eventId);
  check(eventId, String);

  var suggestedDates = Gugo.collections.suggestedDates.find({ eventId : eventId });

  if(suggestedDates) {
    return suggestedDates;
  } else {
    return this.ready();
  }
});