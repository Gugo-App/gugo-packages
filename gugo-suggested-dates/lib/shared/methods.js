
function dateEquals(a, b) {
  return a.getDate() === b.getDate()
    && a.getMonth() === b.getMonth()
    && a.getYear() === b.getYear();
}

Meteor.methods({
  addSuggestedDate: function(startDate, endDate, isAllDay, eventId) {
    if(!(startDate && eventId && this.userId)) {
      throw new Meteor.Error('invalid-params', 'Did not pass date or userId')
    }

    var insertObj = {
      isAllDay : isAllDay,
      startDate : startDate,
      endDate : endDate,
      eventId : eventId,
      createdBy : this.userId,
      suggestedBy : []
    };

    if(endDate) {
      insertObj.endDate = endDate;
    }

    const suggestedDates = Gugo.collections.suggestedDates.find({ eventId : eventId }).fetch();
    var doesDateExist = false;

    //console.log("SUGGESTED DATES", suggestedDates);
    if(suggestedDates && suggestedDates.length > 0) {
      _.each(suggestedDates, function( sd ) {
        if(isAllDay && sd.isAllDay) {
          //console.log("Both all day", isAllDay, sd.isAllDay);
          var startEqual = (sd.startDate && startDate && dateEquals(sd.startDate, startDate));
          var endEqual = (sd.endDate && endDate && dateEquals(sd.endDate, endDate));

          //console.log("Checking suggested Dates....", sd, startEqual, endEqual);

          if(startEqual && endEqual) {
            doesDateExist = true;
          }

        } else if(!isAllDay && !sd.isAllDay) {
          //console.log("Both Are Not All Day", startDate && startDate.getTime(), sd.startDate && sd.startDate.getTime(), endDate && endDate.getTime(), sd.endDate && sd.endDate.getTime());

          var startEqual = (startDate && startDate.getTime()) === (sd.startDate && sd.startDate.getTime());
          var endEqual =  (endDate && endDate.getTime()) === (sd.endDate && sd.endDate.getTime());

          //console.log("Checking suggested Dates....", sd, startEqual, endEqual);

          if(startEqual && endEqual) {
            doesDateExist = true;
          }
        }

      });
    }

    if(doesDateExist) {
      console.log("Duplicate Suggested Date, Return");
      return;
    }

    console.log("All checks passed, add suggested Date");

    function cb(err, docs) {
      if(!err) {
        Meteor.call('newActivity', {
          eventId : eventId,
          type : ActivityTypes.SUGGEST_DATE
        });
      }
    }
    return Gugo.collections.suggestedDates.insert(insertObj, Gugo.handlers.mongo.bind(this, {msg : "Suggested Dates", callback : cb }));
  },
  suggestExistingDate : function(dateId) {
    if(!(dateId && this.userId)) {
      throw new Meteor.Error('invalid-params', 'Did not pass date id or userId')
    }

    Gugo.collections.suggestedDates.update(dateId, {
      $push : {
        'suggestedBy' : this.userId
      },
      $inc : {
        suggestedCount : 1
      }
    }, Gugo.handlers.mongo.bind(this, {msg : "Suggested Dates" }));

  },
  unSuggestExistingDate : function(dateId) {
    if(!(dateId && this.userId)) {
      throw new Meteor.Error('invalid-params', 'Did not pass date id or userId')
    }

    Gugo.collections.suggestedDates.update(dateId, {
      $pull : {
        'suggestedBy' : this.userId
      },
      $inc : {
        suggestedCount : -1
      }
    }, Gugo.handlers.mongo.bind(this, {msg : "Suggested Dates" }));

  },
  switchSuggestedDate : function(fromDateId, toDateId) {
    Meteor.call("unSuggestExistingDate", fromDateId);
    Meteor.call("suggestExistingDate", toDateId);
  },
  lockDateTime : function(dateId) {
    var dt = Gugo.collections.suggestedDates.findOne(dateId);

    if(!dt) {
      throw new Meteor.Error("Could not find Suggested Date");
    }


    console.log("Date", dt);

    function cb(err) {
      if(!err) {
        Meteor.call('newActivity', {eventId: dt.eventId, type : ActivityTypes.OWNER_LOCKED_DATE});
      }
    }

    Gugo.collections.userEvents.update(dt.eventId, { $set : {
      'startDate' : dt.startDate || null,
      'endDate' : dt.endDate || null,
      'isDateTimeLocked' : true
    }}, Gugo.handlers.mongo.bind(this, {msg : "Suggested Dates", callback : cb }))
  }
});