Gugo.collections.suggestedDates = new Mongo.Collection('suggested-dates');

Gugo.collections.suggestedDates.attachSchema(Gugo.schemas.SuggestedDatesSchema);

if(Meteor.isServer) {
  Gugo.collections.suggestedDates._ensureIndex({ eventId : 1 })
}