Gugo.schemas.SuggestedDatesSchema = new SimpleSchema({
  createdBy : {
    type : String,
    optional : false
  },
  isAllDay : {
    type : Boolean,
    optional : true,
    defaultValue : false
  },
  startDate : {
    type : Date,
    optional: false
  },
  endDate : {
    type : Date,
    optional : true
  },
  eventId : {
    type : String,
    optional: false
  },
  suggestedBy : {
    type : [String],
    optional: false
  },
  suggestedCount : {
    type : Number,
    defaultValue : 0,
    optional : false
  }
});