Package.describe({
  name: 'gugo:suggested-dates',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use(['ecmascript', 'gugo:core', 'aldeed:collection2@2.7.0']);

  api.addFiles(['lib/server/_publications.js'], 'server', { transpile : false });
  api.addFiles(['lib/shared/_schema.js', 'lib/shared/_collection.js', 'lib/shared/methods.js'], ['client', 'server'], { transpile : false });

  api.addFiles('suggested-date-helpers.js');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('gugo:suggested-dates');
  api.addFiles('suggested-dates-tests.js');
});
