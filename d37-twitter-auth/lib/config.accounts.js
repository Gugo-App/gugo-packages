if(!(Meteor.settings.twitter.consumerKey && Meteor.settings.twitter.secret )) {
  throw new Meteor.Error("404", "Twitter Consomer Key or Secret Were not Found");
}

var twitter = {
  consumerKey: Meteor.settings.twitter.consumerKey,
  secret: Meteor.settings.twitter.secret
};

Meteor.startup(function() {

  Accounts.loginServiceConfiguration.remove({
    service: "twitter"
  });

  Accounts.loginServiceConfiguration.insert({
    service: "twitter",
    consumerKey: twitter.consumerKey,
    secret: twitter.secret
  });

});