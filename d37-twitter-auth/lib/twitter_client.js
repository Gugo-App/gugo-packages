var browserLoginWithTwitter = Meteor.loginWithTwitter;

Meteor.loginWithTwitter = function(callback) {
  //Check if we are cordova or browser here and choose which method to use
  browserLoginWithTwitter(callback);
};

//Todo : Timeout in case it cant reach the server,
//Todo : Wait at least a second before making the div return to 0%
Meteor.loginWithTwitterSplash = function (callback) {
  showTwitterSplash();
  Meteor.setTimeout(function() {
    Meteor.loginWithTwitter(function (error) {
      hideTwitterSplash();
      callback(error);
    });
  }, 1000);
};


var showTwitterSplash = function () {
  var elemDiv  = document.createElement('div');
  var gIconDiv = document.createElement('h1');

  gIconDiv.innerText     = "Twitter";
  gIconDiv.style.cssText = "position: absolute; top:50%; left: 50%; color:white; margin-right: -50%; transform: translate(-50%, -50%);";

  elemDiv.style.cssText = 'position:absolute;width:100%;height:0%;z-index:100;background:#00aced; bottom:0; left:0';
  elemDiv.className     = "extend-top";
  elemDiv.appendChild(gIconDiv);

  document.body.appendChild(elemDiv);
};

var hideTwitterSplash = function () {
  var el = $('.extend-top');
  el.removeClass('extend-top');
  el.addClass('shrink-bottom');
};
