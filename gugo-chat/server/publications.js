import * as ChatCollections from '../shared/collections';

Meteor.publish('chatRoom', function(chatId) {
  this.unblock();

  console.log("ChatRoom id: ", chatId, "Publish ", chatId,  ChatCollections.ChatRooms.findOne(chatId));

  return [
    ChatCollections.ChatRooms.find(chatId),
    ChatCollections.ChatMessages.find({ chatId : chatId }),
    ChatCollections.ChatClients.find({ chatId : chatId })
  ]
});

Meteor.publishComposite('chatRoomsInvitesLimit', function(options) {
  this.unblock();

  const userId = this.userId;
  const user = Meteor.users.findOne(userId);
  const phone = user.phone;

  return {
    find: function() {
      return ChatCollections.ChatInvites.find({ phone : phone });
    },
    children : [
      {
        find: function(invite) {
          return ChatCollections.ChatRooms.find(invite.chatId, { limit : 1 });
        }
      },
      {
        find: function(invite) {
          return ChatCollections.ChatMessages.find(invite.chatId, { limit : 1, sort : { createdAt : -1 } })
        }
      }
    ]
  }
});

Meteor.publishComposite('chatRoomsOwnedLimit', function(options) {
  this.unblock();

  const userId = this.userId;

  return {
    find: function() {
      return ChatCollections.ChatRooms.find({ ownerId : userId });
    },
    children : [
      {
        find: function(chatRoom) {
          return ChatCollections.ChatMessages.find(chatRoom.chatId, { limit : 1, sort : { createdAt : -1 } })
        }
      }
    ]
  }
});

// I was trying to boil down the invites and owned events down into one collection but it was too complicated
//Meteor.publish('chatRoomsLimit', function(options) {
//  this.unblock();
//
//  const userId = this.userId;
//  const user = Meteor.users.findOne(userId);
//  const phone = user.phone;
//
//  //return {
//  //  find: function() {
//  //    return ChatCollections.ChatInvites.find({ phone : phone }).fetch();
//  //  },
//  //  children : [
//  //    {
//  //      find: function(invite) {
//  //        return ChatCollections.ChatRooms.find(invite.chatId);
//  //      },
//  //      find: function() {
//  //        return ChatCollections.ChatRooms.find({ ownerId : this.userId })
//  //      }
//  //    }
//  //  ]
//  //}
//
//  console.log("Chat Rooms Limit");
//
//
//
//  //this.autorun( () => {
//  //  const chatRoomInvites = ChatCollections.ChatInvites.find({ phone : phone }).fetch();
//  //  const chatRoomInviteIds = chatRoomInvites.map( (invite) => {
//  //    return invite._id;
//  //  });
//  //
//  //  console.log("Chat ROom Invite IDS", chatRoomInviteIds);
//  //  console.log("Chats I own", ChatCollections.ChatRooms.find({ ownerId : userId }).fetch())
//  //
//  //
//  //  return {
//  //    find : function() {
//  //      //Get all chat rooms that you either own or have been invited to
//  //      const query = {
//  //        $or : [
//  //          {
//  //            _id : { $in : chatRoomInviteIds }
//  //          },
//  //          {
//  //            ownerId : userId
//  //          }
//  //        ]
//  //      };
//  //
//  //      return ChatCollections.ChatRooms.find(query);
//  //    },
//  //    children : [
//  //      //for each chat room found we want to find a the latest chat message
//  //      {
//  //        find: function(chatRoom) {
//  //          return ChatCollections.ChatMessages.find({ chatId : chatRoom._id }, { limit : 1, sort : { createdAt : -1 } });
//  //        }
//  //      }
//  //    ]
//  //  }
//  //});
//
//  var cursors = [];
//
//  this.autorun( () => {
//
//    const chatRoomInvites = ChatCollections.ChatInvites.find({phone: phone}).fetch();
//    const chatRoomInviteIds = chatRoomInvites.map((invite) => {
//      return invite._id;
//    });
//
//    console.log("Chat ROom Invite IDS", chatRoomInviteIds);
//    console.log("Chats I own", ChatCollections.ChatRooms.find({ownerId: userId}).fetch());
//
//    const chatRoomQuery =  {
//            $or : [
//              {
//                _id : { $in : chatRoomInviteIds }
//              },
//              {
//                ownerId : userId
//              }
//            ]
//          };
//
//    var cursors = [];
//    cursors.push(ChatCollections.ChatRooms.find(chatRoomQuery, { limit : 50 }));
//
//    Tracker.nonreactive( () => {
//      const chatRooms = ChatCollections.ChatRooms.find(chatRoomQuery, { limit : 50 }).fetch();
//
//      const messageCursors = chatRooms.map( (chatRoom) => {
//        return ChatCollections.ChatMessages.find({ chatId : chatRoom._id }, { limit : 1, sort : { createdAt : -1 } });
//      });
//
//      cursors = cursors.concat(messageCursors);
//    });
//
//    console.log("CURSORS LENGTH", cursors.length);
//
//    return cursors;
//  });
//
//
//
//  //
//  //return {
//  //  find : function() {
//  //    //Get all chat rooms that you either own or have been invited to
//  //    const query = {
//  //      $or : [
//  //        {
//  //          _id : { $in : chatRoomInviteIds }
//  //        },
//  //        {
//  //          ownerId : userId
//  //        }
//  //      ]
//  //    };
//  //
//  //    return ChatCollections.ChatRooms.find(query);
//  //  },
//  //  children : [
//  //    //for each chat room found we want to find a the latest chat message
//  //    {
//  //      find: function(chatRoom) {
//  //        return ChatCollections.ChatMessages.find({ chatId : chatRoom._id }, { limit : 1, sort : { createdAt : -1 } });
//  //      }
//  //    }
//  //  ]
//  //}
//
//
//  //
//  //console.log("CHAT ROOM", chatId,  ChatCollections.ChatRooms.findOne(chatId));
//  //
//  //return [
//  //  ChatCollections.ChatRooms.find(chatId),
//  //  ChatCollections.ChatMessages.find({ chatId : chatId }),
//  //  ChatCollections.ChatClients.find({ chatId : chatId })
//  //]
//
//
//
//
//});