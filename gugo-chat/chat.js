//This is our index

import * as collections from './shared/collections';
import * as inviteMethods from './methods/invites';
import * as chatRoomMethods from './methods/chatRooms';
import * as chatMessageMethods from './methods/chatMessages';

console.log("CHAT MESSAGE MTHODS", chatMessageMethods);

export {
  collections,
  inviteMethods,
  chatRoomMethods,
  chatMessageMethods
}