import * as ChatSchemas from './schemas';

export const ChatInvites = new Mongo.Collection('chat-invites');
ChatInvites.attachSchema(ChatSchemas.ChatInvitesSchema);

export const ChatRooms = new Mongo.Collection('chat-rooms');
ChatRooms.attachSchema(ChatSchemas.ChatRoomSchema);

ChatRooms.allow({
  insert : function() {
    return true;
  },
  update : function() {
    return true;
  },
  remove: function() {
    return true;
  }
})

export const ChatClients = new Mongo.Collection('chat-clients');
ChatClients.attachSchema(ChatSchemas.ChatClientSchema);

export const ChatMessages = new Mongo.Collection('chat-messages');
ChatMessages.attachSchema(ChatSchemas.ChatMessageSchema);