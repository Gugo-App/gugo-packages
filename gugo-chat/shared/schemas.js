export const ChatInvitesSchema = new SimpleSchema({
  createdAt:        {
    type:      Date,
    optional:  true,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  },
  chatId : {
    type : String
  },
  phone : {
    type : String
  }
});

export const ChatMessageSchema = new SimpleSchema({
  createdAt:        {
    type:      Date,
    optional:  true,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  },
  chatId : {
    type : String
  },
  userId : {
    type : String
  },
  'content.text' : {
    type : String
  }
});

export const ChatClientSchema = new SimpleSchema({
  chatId : {
    type : String
  },
  userId : {
    type : String
  },
  lastMessageSeen : {
    type : String,
    optional : true
  },
  isActive : {
    type : String,
    optional : true
  },
  isTyping : {
    type : String,
    optional : true
  }
});

export const ChatRoomSchema = new SimpleSchema({
  createdAt:        {
    type:      Date,
    optional:  true,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  },
  lastMessageAt : {
    type : Date,
    optional: true
  },
  'lastMessage' : {
    type : String,
    optional : true
  },
  ownerId : {
    type : String
  },
  name : {
    type : String
  },
  slug : {
    type : String,
    optional : true
  },
  //A Chat Room can be centered around an eventId
  eventId : {
    type : String,
    optional : true
  }
});