Package.describe({
  name: 'gugo:chat',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.3.1');
  api.use(['ecmascript', 'aldeed:collection2@2.7.0', 'mdg:validated-method', 'reywood:publish-composite@1.4.2', 'peerlibrary:reactive-publish@0.2.0', 'todda00:friendly-slugs']);
  api.mainModule('chat.js', ['client', 'server']);
  api.mainModule('chat-server.js', 'server');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('gugo:chat');
  api.mainModule('chat-tests.js');
});
