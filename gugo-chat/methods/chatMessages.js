import * as ChatCollections from '../shared/collections';

export const AddMessage = new ValidatedMethod({
  name : 'chat.messages.add',
  validate(args) {
    new SimpleSchema({
      userId : {
        type : String
      },
      chatId : {
        type : String
      },
      content : {
        type : Object
      }
    }).validator(args);
  },
  run({ chatId, userId, content }) {
    return __addMessage(chatId, userId, content)
  }
});

function __addMessage(chatId, userId, content) {
  const insertObj = { chatId : chatId, userId : userId, content: content };

  console.log("About to insert", insertObj);
  var msgId = ChatCollections.ChatMessages.insert({ chatId : chatId, userId : userId, content: content });
  ChatCollections.ChatRooms.update(chatId, {
    $set : {
      lastMessage : msgId
    }
  }, Gugo.handlers.mongo.bind(this, { message : "Chat Rooms Add Message" }));

  return msgId;
}