import * as ChatCollections from '../shared/collections';

export const SendBatchChatInvites = new ValidatedMethod({
  name : 'chat.sendBatchInvites',
  validate(args) {
    new SimpleSchema({
      chatId : { type : String },
      contactIds : { type : [String] }
    })
  },
  run({ chatId, contactIds}) {
    for(var contactId in contactIds) {
      const contact = Gugo.collections.contacts.findOne(contactId);
      sendChatInvite(chatId, contact);
    }
  }
})

export const SendChatInvite = new ValidatedMethod({
  name : 'chat.sendInvite',
  validate(args) {
    new SimpleSchema({
      chatId : { type : String },
      contact : { type : Object }
    }).validator(args);
  },
  run({ chatId, phone }) {
    sendChatInvite(chatId, phone)
  }
});

function sendChatInvite(chatId, phone) {
  //Search for chat invite, don't invite if we already have an invite for that person
  const existingInvite = ChatCollections.ChatInvites.findOne({ chatId : chatId, phone : phone });

  if(existingInvite) {
    return existingInvite._id;
  }

  const id = ChatCollections.ChatInvites.insert({ chatId : chatId, phone : phone });

  //send twilio url with id

  return id;

}