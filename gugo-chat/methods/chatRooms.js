import * as ChatCollections from '../shared/collections';

//Creates a chat room based on the ownerId and context, context is an object containing (for now) an eventId
//but in the future might be used to create rooms for other contexts
export const CreateChatRoom = new ValidatedMethod({
  name : 'chat.createChatRoom',
  validate(args) {
    new SimpleSchema({
      ownerId : {
        type : String
      },
      name : {
        type : String
      },
      context : {
        type : Object,
        optional : true
      }
    }).validator(args);
  },
  run({ ownerId, name, context }) {
    console.log(Meteor.isServer ? "server" : "client");
    return __createChatRoom(ownerId, name, context)
  }
});

function __createChatRoom(ownerId, name, context) {
  var insertObj = {
    ownerId : ownerId,
    name : name
  };

  if(context.eventId) {
    insertObj.eventId = context.eventId;
  }

  var id = ChatCollections.ChatRooms.insert(insertObj);

  console.log("Create Chat Room", ChatCollections.ChatRooms.findOne(id));

  return id;
};

export const DestroyChatRoom = new ValidatedMethod({
  name : 'chat.destroyChatRoom',
  validate(args) {
    new SimpleSchema({
      chatId : {
        type : String
      }
    })
  },
  run({ chatId }) {
    return __destroyChatRoom(chatId);
  }
})

function __destroyChatRoom(chatId) {
  return ChatCollections.ChatRooms.remove(chatId)
}