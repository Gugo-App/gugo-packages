Package.describe({
  name:          'gugo:test-fixtures',
  version:       '0.0.1',
  // Brief, one-line summary of the package.
  summary:       '',
  // URL to the Git repository containing the source code for this package.
  git:           '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md',
  // Only use fixtures package outside of production
  debugOnly:     true
});

Package.onUse(function (api) {
  api.versionsFrom('1.2.1');

  api.use([
    'ecmascript',
    'underscore',
    'gugo:core',
    'gugo:events',
    'gugo:venues',
    'meteorhacks:aggregate@1.3.0',
    'anti:fake@0.4.1'
  ]);

  api.addFiles([
    'server/fixtures/categories-fixture.js',
    'server/fixtures/events-fixture.js',
    'server/fixtures/venues-fixture.js'
  ], ['server'], { transpile : false });
  api.addFiles('server/methods.js', ['server'], { transpile : false });
  api.addFiles('server/startup.js', ['server'], { transpile : false });
});

Package.onTest(function (api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('gugo:test-fixtures');
  api.addFiles('test-fixtures-tests.js');
});
