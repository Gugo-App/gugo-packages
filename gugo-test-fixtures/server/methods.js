Meteor.methods({
  loadTestFixtures: function () {
    if (Gugo.settings.useFixtures) {
      console.log("Loading fixtures");

      Gugo.fixtures.categories.load();
      Gugo.fixtures.venues.load();
      Gugo.fixtures.events.load();

      //TODO: testing - sequential event names, one per day (1,000 of them)
      //Gugo.fixtures.testEvents.load();
    }
  }
});
