//region Normal test fixtures
Gugo.fixtures.events = {
  options: {
    daysOut:     30,
    createCount: 300
  },
  venues:  [],

  load: function () {

    const eventDays = Gugo.fixtures.events.getDayRangeForFixture();
    //console.log("Event Days to create:", _.uniq(eventDays));

    var date,
        eventWords = [],
        title,
        event,
        startDate,
        endDate,
        randomVenue;

    console.log("Gugo.fixtures.events Creating " + eventDays.length + " events");

    _.each(eventDays, function (daysOut) {
      //TODO: handle different timezones?
      date = moment().tz(Gugo.timezone.Pacific).startOf("day").add(daysOut, "d");

      eventWords = Gugo.fixtures.events.getFakeWordsArray(10);

      title     = eventWords[0] + " " + eventWords[1] + " " + Fake.fromArray(Gugo.fixtures.eventTextArray);
      startDate = Gugo.fixtures.events.getRandomStartDate(date);
      endDate   = moment(startDate.format());
      endDate.add(d37utils.number.randomInt(1, 3), "h");

      randomVenue = Gugo.fixtures.venues.getRandomVenue();

      event = {
        title:            title,
        description:      eventWords.join(" ") + ".",
        isPrivate:        false,
        isExternalSource: false,
        startDate:        new Date(startDate.toISOString()),
        endDate:          new Date(endDate.toISOString()),
        venueId:          randomVenue._id,
        category:         Gugo.fixtures.categories.getRandomCategory(),
        attendeesCount:   d37utils.number.randomInt(0, 9999),
        favoriteCount:    d37utils.number.randomInt(0, 9999),
        images:           {
          heroUri: "//dummyimage.com/480x270",
          tileUri: "//dummyimage.com/100x100"
        },
        source:           {
          name: "events-fixture"
        }
      };

      check(event, Gugo.schemas.EventSchema);
      //d37log.debug("Gugo.fixtures.events Inserting Event:", event);

      Gugo.collections.events.insert(event, function (err, id) {
        if (err) {
          d37log.error("Gugo.fixtures.events insert error", { error: err });
        } else {
          //d37log.debug("Inserted Event", { id: id });
        }
      });
    });
  },

  getDayRangeForFixture: function () {
    var array,
        eventAggr,
        lastFixtureRun,
        lastRunEndDate,
        gapInDays,
        thirtyDaysFromToday = moment().add("30", "d"),
        startDaysOut        = 0,
        endDaysOut          = Gugo.fixtures.events.options.daysOut,
        eventCount          = Gugo.fixtures.events.options.createCount;

    // Find last time fixture ran. Load average of 10 events/day for next 30 days. Fill in the gap as needed.
    eventAggr = Gugo.collections.events.aggregate([
      { $match: { "source.name": "events-fixture" } },
      { $group: { _id: null, lastCreated: { $max: "$createdAt" } } }
    ]);

    if (eventAggr && eventAggr[0] && eventAggr[0].lastCreated) {
      lastFixtureRun = moment(eventAggr[0].lastCreated);
      lastRunEndDate = lastFixtureRun.add(endDaysOut, "d");

      gapInDays = thirtyDaysFromToday.diff(lastRunEndDate, "d");
      if (gapInDays === 0) {
        return [];
      } else {
        startDaysOut = endDaysOut - gapInDays;
        eventCount   = eventCount / gapInDays;
      }
    }

    console.log("getDayRangeForFixture: ", startDaysOut, endDaysOut, eventCount);
    array = d37utils.number.randomIntArray(startDaysOut, endDaysOut, eventCount).sort();
    //console.log("getDayRangeForFixture array:", array);

    return array
  },

  getFakeWordsArray: function (count) {
    var array = [];
    for (i = 0; i < count; i++) {
      array.push(Fake.word());
    }
    return array;
  },

  getRandomStartDate: function (date) {
    var hour = d37utils.number.randomInt(8, 20);
    return date.set("hour", hour);
  }

  //get sources () {
  //  return [
  //    { id: "", name: "", eventUri: "", ticketingUri: "" }
  //  ];
  //}

};
//endregion

//region Sequential Events Test Fixture
//TODO: Testing fixture for creating many sequential, easily identifiable events
Gugo.fixtures.testEvents = {
  _categories: null,
  load:        function () {
    var
      start    = d37utils.date.moment.getStartOfDay().add(-1, "days"),
      existing = Gugo.collections.events.find().fetch();

    if (existing.length <= 0) {
      console.log("Gugo.fixtures.testEvents Creating events");

      for (var i = 1; i <= 1000; i++) {
        var event = {
          title:            "Event # " + i,
          isPrivate:        false,
          isExternalSource: false,
          startDate:        new Date(start.add(1, "days")),
          venueId:          Gugo.fixtures.venues.getRandomVenue()._id,
          category:         Gugo.fixtures.categories.getRandomCategory(),
          attendeesCount:   d37utils.number.randomInt(0, 9999),
          favoriteCount:    d37utils.number.randomInt(0, 9999),
          images:           {
            heroUri: "//dummyimage.com/480x270",
            tileUri: "//dummyimage.com/100x100"
          },
          source:           {
            name: "events-fixture"
          }
        };

        Gugo.collections.events.insert(event, function (err, id) {
          if (err) {
            d37log.error("Gugo.fixtures.events insert error", { error: err });
          } else {
            //d37log.debug("Inserted Event", { id: id });
          }
        });
      }
    }
  }
};

Gugo.fixtures.eventTextArray = [
  "Gala",
  "Event",
  "Party",
  "Show",
  "Opening"
];
//endregion