var Future = Npm.require('fibers/future');
var Fiber  = Npm.require('fibers');

Gugo.fixtures.categories = {
  array: [],

  load: function () {
    var future   = new Future(),
        existing = Gugo.collections.categories.findOne();

    if (existing) {
      console.log("Gugo.fixtures.categories Creating 0 categories");
      return future.return();
    }

    console.log("Gugo.fixtures.categories Creating "
      + Gugo.fixtures.categories.items.length + " categories");

    Fiber(function () {
      var fiber = Fiber.current;
      _.each(Gugo.fixtures.categories.items, function (category) {
        console.log("Inserting category", category);

        Gugo.collections.categories.insert(category, function (err) {
          if (err) {
            console.log("Gugo.fixtures.categories insert error", { error: err });
          } else {
            console.log("Inserted category", category);
          }
          fiber.run();
        });
        Fiber.yield();
      });
      future.return();
    }).run();

    return future.wait();
  },

  getRandomCategory: function () {
    var categories;

    if (Gugo.fixtures.categories.array.length <= 0) {
      categories = Gugo.collections.categories.find().fetch();
      _.each(categories, function (category) {
        Gugo.fixtures.categories.array.push(category);
      });
      console.log("Gugo.fixtures.categories.array", Gugo.fixtures.categories.array);
    }

    return d37utils.number.getRandomArrayItem(Gugo.fixtures.categories.array);
  },

  get items () {
    return [
      { "name": "Art" },
      { "name": "Comedy" },
      { "name": "Community & Civics" },
      { "name": "Festivals" },
      { "name": "Film Festivals & Events" },
      { "name": "Food & Drinks" },
      { "name": "Geek & Gaming" },
      { "name": "Kids" },
      { "name": "Music" },
      { "name": "Outdoors" },
      { "name": "Queer" },
      { "name": "Reading & Talks" },
      { "name": "Sports" },
      { "name": "Theatre & Dance" },
      { "name": "Weed Stuff" }
    ];
  }
};
