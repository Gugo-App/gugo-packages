var Future = Npm.require('fibers/future');
var Fiber  = Npm.require('fibers');

Gugo.fixtures.venues = {
  array: [],

  load: function () {
    var future   = new Future(),
        existing = Gugo.collections.venues.findOne();

    if (existing) {
      console.log("Gugo.fixtures.venues Creating 0 venues");
      return future.return();
    }

    console.log("Gugo.fixtures.venues Creating "
      + Gugo.fixtures.venues.items.length + " venues");

    Fiber(function () {
      var fiber = Fiber.current;
      _.each(Gugo.fixtures.venues.items, function (venue) {
        console.log("Inserting venue", venue);

        Gugo.collections.venues.insert(venue, function (err) {
          if (err) {
            console.log("Gugo.fixtures.venues insert error", { error: err });
          } else {
            console.log("Inserted venue", venue);
          }
          fiber.run();
        });
        Fiber.yield();
      });
      future.return();
    }).run();

    return future.wait();
  },

  getRandomVenue: function () {
    var venues;

    if (Gugo.fixtures.venues.array.length <= 0) {
      venues = Gugo.collections.venues.find().fetch();
      _.each(venues, function (venue) {
        Gugo.fixtures.venues.array.push(venue);
      });
      console.log("Gugo.fixtures.venues.array", Gugo.fixtures.venues.array);
    }

    return d37utils.number.getRandomArrayItem(Gugo.fixtures.venues.array);
  },

  get items () {
    return [
      {
        "name": "Neptune Theatre",
        "location": {
          "address": {
            "fullAddress": "1303 NE 45th St, Seattle, WA 98105, United States",
            "street": "1303 NE 45th St",
            "city": "Seattle",
            "state": "WA",
            "zip": "98105"
          },
          "geo": {
            "coordinates": [
              47.661107000000008327,
              -122.31403000000000247
            ]
          }
        }
      },
      {
        "name": "Paramount Theatre",
        "location": {
          "address": {
            "fullAddress": "911 Pine St, Seattle, WA 98101, United States",
            "street": "911 Pine St",
            "city": "Seattle",
            "state": "WA",
            "zip": "98101"
          },
          "geo": {
            "coordinates": [
              47.613285099999998806,
              -122.33137950000002547
            ]
          }
        }
      },
      {
        "name": "Showbox SoDo",
        "location": {
          "address": {
            "fullAddress": "1700 1st Avenue South, Seattle, WA 98134, United States",
            "street": "1700 1st Avenue South",
            "city": "Seattle",
            "state": "WA",
            "zip": "98134"
          },
          "geo": {
            "coordinates": [
              47.587928599999997914,
              -122.33370230000002721
            ]
          }
        }
      },
      {
        "name": "The Crocodile",
        "location": {
          "address": {
            "fullAddress": "2200 2nd Ave, Seattle, WA 98121, United States",
            "street": "2200 2nd Ave",
            "city": "Seattle",
            "state": "WA",
            "zip": "98121"
          },
          "geo": {
            "coordinates": [
              47.613593400000013389,
              -122.34430240000000367
            ]
          }
        }
      },
      {
        "name": "The Moore Theatre",
        "location": {
          "address": {
            "fullAddress": "1932 2nd Ave, Seattle, WA 98101, United States",
            "street": "1932 2nd Ave",
            "city": "Seattle",
            "state": "WA",
            "zip": "98101"
          },
          "geo": {
            "coordinates": [
              47.611827299999987417,
              -122.34142919999999322
            ]
          }
        }
      },
      {
        "name": "The Showbox",
        "location": {
          "address": {
            "fullAddress": "1426 1st Ave, Seattle, WA 98101, United States",
            "street": "1426 1st Ave",
            "city": "Seattle",
            "state": "WA",
            "zip": "98101"
          },
          "geo": {
            "coordinates": [
              47.608517499999997824,
              -122.33938339999997424
            ]
          }
        }
      }
    ];
  }
};
