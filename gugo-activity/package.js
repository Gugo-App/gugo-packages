Package.describe({
  name: 'gugo:activity',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use(['ecmascript', 'gugo:core', 'reywood:publish-composite@1.4.2', 'peerlibrary:reactive-publish@0.2.0']);
  api.addFiles(['lib/server/publication.js'], 'server',  { transpile : false });

  api.addFiles(['lib/shared/collection.js', 'lib/shared/methods.js', 'lib/shared/schema.js', 'lib/shared/helpers.js'], ['client', 'server'],  { transpile : false });

  api.export("ActivityTypes", ['client', 'server']);
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('gugo:activity');
  api.addFiles('activity-tests.js');
});
