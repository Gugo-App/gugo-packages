convertMS = function(ms) {
  var d, h, m, s;
  s = Math.floor(ms / 1000);
  m = Math.floor(s / 60);
  s = s % 60;
  h = Math.floor(m / 60);
  m = m % 60;
  d = Math.floor(h / 24);
  h = h % 24;
  return { d: d, h: h, m: m, s: s };
};

Gugo.collections.activity.helpers({
  timeSince : function() {
    var now = new Date();
    var minutesToShowMins = 60;

    var converted = convertMS(now.getTime() - this.createdAt.getTime());

    //Show minutes if the time is under 10 minutes, show time stamp if after
    if(converted.d && converted.d > 0) {
      return converted.d + " days ago.";
    } else if (converted.h && converted.h) {
      return converted.h + " hours ago.";
    } else {
      return converted.m <= 0 ? "now" : converted.m + " minutes ago.";
    }
  }
});