ActivityTypes = {
  NEW_INTEREST : 'newInterest',
  NEW_CALENDAR : 'newCalendar',
  NEW_COMMENT : 'newComment',
  SUGGEST_DATE : "suggestDate",
  SUGGEST_LOCATION : 'suggestLocation',
  OWNER_LOCKED_DATE : 'ownerLockedDate',
  OWNER_LOCKED_LOCATION : 'ownerLockedLocation',
  EVENT_CANCELED : "eventCanceled",
  EVENT_MODIFIED : 'eventModified'
};

activityTypes = [
  'newInterest', //private
  'newCalendar', //private
  'newComment', //private / public if commented on
  'suggestDate', //private
  'suggestLocation', //private
  'ownerLockedDate', //private
  'ownerLockedLocation', //private
  'eventCanceled', //private / public
  'eventModified' //private / public
];

ActivityTypes.getSlideFromActivity = function(activityType) {
  var slide = null;
  switch(activityType) {
    case ActivityTypes.NEW_CALENDAR || ActivityTypes.NEW_INTEREST :
      slide = 2;
      break;
    case ActivityTypes.NEW_COMMENT :
      slide = 0;
      break;
    case ActivityTypes.SUGGEST_DATE || ActivityTypes.SUGGEST_LOCATION || ActivityTypes.OWNER_LOCKED_DATE || ActivityTypes.OWNER_LOCKED_LOCATION :
      slide = 1;
      break;
    default :
      slide = 1;
      break;
  }

  return slide;
};

ActivitySchema = new SimpleSchema({
  createdAt : {
    type : Date,
    autoValue : function() {
      if(this.isInsert) {
        return new Date()
      }
    }
  },
  eventId : {
    type : String
  },
  triggeredBy : {
    type : String,
    optional : true
  },
  type : {
    type : String,
    allowedValues : activityTypes
  },
  message : {
    type : String
  }
});

Gugo.collections.activity.attachSchema(ActivitySchema);