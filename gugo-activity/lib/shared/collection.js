Gugo.collections.activity = new Mongo.Collection('activity');

if(Meteor.isServer) {
  Gugo.collections.activity._ensureIndex({ eventId : 1 });
  Gugo.collections.activity._ensureIndex({ eventId : 1, triggeredBy : 1 });
}