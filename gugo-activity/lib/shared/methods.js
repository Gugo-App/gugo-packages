Meteor.methods({
  /*
  Options must contain:
    triggeredBy : A userId <--- this can be supplied from the caller automatically
    eventId : an event id
    type : one of the ActivityTypes
   */
  newActivity: function(options) {
    var userId = options.userId || this.userId;
    if(!userId) {
      throw new Meteor.Error('New Activity Requires a UserId')
    }

    var doc = {
      triggeredBy : userId,
      eventId : options.eventId,
      type : options.type
    };

    const user = Meteor.users.findOne(userId);
    const userName = user && user.profile && user.profile.name || "Someone"; // This is probably not needed, but just in case
    const event = Gugo.collections.events.findOne(options.eventId);
    // console.log("THIS IS THE USER EVENT ", userEvent);
    var slideNum;

    console.log("GOt type");

    switch(options.type) {
      case ActivityTypes.NEW_CALENDAR :
        // doc.message = userName + " has added the event to their calendar";
        // slideNum = 2;
        break;
      case ActivityTypes.NEW_INTEREST :
        // doc.message = userName + " has shown interest in the event";
        // slideNum = 2;
        break;
      case ActivityTypes.NEW_COMMENT :
          doc.message = userName + " has added a comment";
          slideNum = 0;
        break;
      case ActivityTypes.SUGGEST_DATE :
        // if (!userEvent) {
        //   return;
        // }
        // doc.message = userName + " has suggested a new date.";
        // slideNum = 1;
        break;
      case ActivityTypes.SUGGEST_LOCATION :
        // if (!userEvent) {
        //   return;
        // }
        // doc.message = userName + " has suggested a new location";
        break;
      case ActivityTypes.OWNER_LOCKED_DATE :
        // if (!userEvent) {
        //   return;
        // }
        // doc.message = userName + " has locked the date of the event";
        // slideNum = 1;
        break;
      case ActivityTypes.OWNER_LOCKED_LOCATION :
        // if (!userEvent) {
        //   return;
        // }
        // doc.message = userName + " has locked the location of the event";
        // slideNum = 1;
        break;
      default :
        throw new Meteor.Error("No supported activity type given for : " + options.type);
        break;
    }

    if (event && doc.message) {
      var cEvents = Gugo.collections.calendarEvents.find({ events : options.eventId, ownerId: { $ne: userId }}, { fields : { 'ownerId' : 1, 'events' : 1 } }).fetch();
      console.log(cEvents);
      var userIds = cEvents.map(function(cEvent) {
        var user = Meteor.users.findOne(cEvent.ownerId);

        return user._id;
      });

       _.each(userIds, function(userId) {
          Push.send({
           from: "GUGO",
           title: 'New Event Activity',
           text: doc.message,
           badge: 1, //optional, use it to set badge count of the receiver when the app is in background.
           query: {
             userId: userId
           },
           payload: { view: "event", viewParams : { eventId: options.eventId, slide: slideNum } }
         });
       });

      console.log(userIds);

      Gugo.collections.activity.insert(doc, Gugo.handlers.mongo.bind(this, {msg : "Insert Activity"}));
    }
  },
  updateUsersLastSeenRecord: function() {
    if (!this.userId) return;

    Meteor.users.update(this.userId , { $set : { 'profile.lastSeenRecord' : new Date() } });
  },
  sendViewPush : function(opts, payload) {
    Push.send({
      from: opts.from,
      title: opts.title,
      text: opts.text,
      badge: opts.badge || 1, //optional, use it to set badge count of the receiver when the app is in background.
      query: {
        userId: opts.userId
      },
      payload: payload
    });
  }

});