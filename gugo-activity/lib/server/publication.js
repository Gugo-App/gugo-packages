Meteor.publish('recentActivities', function(limit, pageSize) {
  this.unblock();

  //if(!limit) {
  //  limit = 5;
  //}
  //
  //if(!pageSize) {
  //  pageSize = 5;
  //}

  console.log("Page size", pageSize, "limit", limit);

  if(!this.userId) return this.ready();

  this.autorun(function(comp) {

    const interests = Gugo.collections.interestedEvents.findOne({ownerId: this.userId});
    const calAdds = Gugo.collections.calendarEvents.findOne({ownerId: this.userId});
    const myEvents = Gugo.collections.events.find({ownerId: this.userId}).fetch();

    var eventIds = myEvents.map(function (event) {
      return event._id;
    });

    //console.log("Personal Event Ids");

    if (interests) {
      //console.log("We have interests", interests, "boot up eventIds");
      eventIds = eventIds.concat(interests.events);
    }

    if (calAdds) {
      //console.log("We have cal Adds", interests, "boot up eventIds");
      eventIds = eventIds.concat(calAdds.events);
    }

    const deduped = _.uniq(eventIds);

    //console.log("Deduped ids", deduped);
    //, limit : limit, skip : (limit - pageSize)

    return Gugo.collections.activity.find({eventId: { $in: deduped }, triggeredBy: { $ne: this.userId }}, { sort : { createdAt : -1 } })
  });

});

//Meteor.publishComposite('interestedActivities', function() {
//  if(!this.userId) return;
//
//  return {
//    find: function() {
//      return Gugo.collections.interestedEvents.find({ ownerId : this.userId });
//    },
//    children : [{
//      find: function (obj) {
//        return Gugo.collections.activity.find({ eventId : { $in : obj.events } , triggeredBy : { $ne : this.userId  } });
//      }
//    }]
//  }
//});

Meteor.methods({
  createActivityTree : function() {
    var userId = this.userId;

    Meteor.call('createPrivateEvent', {
      title : "Hello"
    }, function(err, res) {
      if(err) {
        console.log("Err", err);
        return;
      }
      //console.log("Created private event", res);
      Meteor.call('addEventToInterested', res);

      Meteor.call('newActivity', { eventId : res, userId : 'User 1', type :  ActivityTypes.NEW_COMMENT });
      Meteor.call('newActivity', { eventId : res, userId : this.userId, type :  ActivityTypes.NEW_COMMENT });
      Meteor.call('newActivity', { eventId : res, userId : 'User 2', type :  ActivityTypes.NEW_COMMENT });

    })
  }
});