export const EventVenueSearch = new ValidatedMethod({
  name : 'search.eventAndVenue',
  validate(args) {
    console.log("Validating");
    new SimpleSchema({
      text : { type: String }
    }).validator(args);
  },
  run({ text }) {
    if(!text || (text && text === "")) {
      return;
    }

    const eventsItemCap = 10;
    const venuesItemCap = 5;

    var slicedEvents = [];
    var slicedVenues = [];

    if(Meteor.isServer) {
      var regexQwry = {$regex: text, $options: 'i'};

      //console.log("Regex: ", regexQwry);

      // You notice were getting a lot of results here because were going to cut the amount we send to the client down
      // to send to the client by applying levenshtein distance calculations
      var events = Gugo.collections.events.find({ title : regexQwry }, { fields : { title : 1, _id : 1, images : 1 }, limit : 30 }).fetch();
      var venues = Gugo.collections.venues.find({ name : regexQwry }, { fields : { name : 1, _id : 1, images : 1 }, limit : 10 }).fetch();

      d37utils.text.getDistanceForObjects(events, 'title', text);
      events = _.sortBy(events, 'title').reverse();
      d37utils.text.getDistanceForObjects(venues, 'name', text);
      venues = _.sortBy(venues, 'name').reverse();

      slicedEvents = events.slice(0, eventsItemCap);
      slicedVenues = venues.slice(0, venuesItemCap);


      //console.log("Events after distnace calc", events, venues);

      //console.log("Ret Sliced", {
      //  events: slicedEvents,
      //  venues : slicedVenues
      //});

      return {
        events: slicedEvents,
        venues : slicedVenues
      }
    }
  }
});

//This is how you would do text search instead of regex

//var events = Gugo.collections.events.find({
//  $text: { $search: text }
//}, {
//  fields : {
//    score: { $meta: "textScore" },
//    title : 1,
//    images : 1,
//    _id : 1
//  }, sort : {
//    score: {
//      $meta: "textScore"
//    }
//  }, limit : 10}).fetch();
//
//
//var venues = Gugo.collections.venues.find({
//  $text: { $search: text }
//}, {
//  fields : {
//    score: { $meta: "textScore" },
//    name : 1,
//    images : 1,
//    _id : 1
//  }, sort : {
//    score: {
//      $meta: "textScore"
//    }
//  }, limit : 5 }).fetch();