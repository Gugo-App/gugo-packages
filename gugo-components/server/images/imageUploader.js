Meteor.methods({
  updateEventImage:   function (eventId, imageType, url) {
    var Future = Npm.require("fibers/future"),
        fut    = new Future(),
        pfx    = "gugo-components.methods.updateEventImage",
        $set   = {};

    if (!eventId || !imageType || !url) {
      console.error(pfx + " error", { eventId: eventId, imageType: imageType, url: url });
      throw new Meteor.Error('invalid-parameter', 'Attempted to set event image with invalid ID, imageType or URL');
    }

    //console.log("updateEventImage:", eventId, imageType, url);
    $set["images." + imageType] = url;

    Gugo.collections.events.update({
      _id: eventId
    }, {
      $set: $set
    }, function (err, docs) {
      if (err) {
        console.log(err);
        throw new Meteor.Error('mongo-error', err);
      }
      fut.return(200);
    });
    return fut.wait();
  },
  updateVenueImage:   function (venueId, imageType, url) {
    var Future = Npm.require("fibers/future"),
        fut    = new Future(),
        pfx    = "gugo-components.methods.updateVenueImage",
        $set   = {};

    if (!venueId || !imageType || !url) {
      console.error(pfx + " error", { venueId: venueId, imageType: imageType, url: url });
      throw new Meteor.Error('invalid-parameter', 'Attempted to set venue image with invalid ID, imageType or URL');
    }

    $set["images." + imageType] = url;

    Gugo.collections.venues.update({
      _id: venueId
    }, {
      $set: $set
    }, function (err, docs) {
      if (err) {
        console.log(err);
        throw new Meteor.Error('mongo-error', err);
      }
      fut.return(200);
    });
    return fut.wait();
  },
  updateVenueImage:   function (venueId, imageType, url) {
    var Future = Npm.require("fibers/future"),
        fut    = new Future(),
        pfx    = "gugo-components.methods.updateVenueImage",
        $set   = {};

    if (!venueId || !imageType || !url) {
      console.error(pfx + " error", { venueId: venueId, imageType: imageType, url: url });
      throw new Meteor.Error('invalid-parameter', 'Attempted to set venue image with invalid ID, imageType or URL');
    }

    $set["images." + imageType] = url;

    Gugo.collections.venues.update({
      _id: venueId
    }, {
      $set: $set
    }, function (err, docs) {
      if (err) {
        console.log(err);
        throw new Meteor.Error('mongo-error', err);
      }
      fut.return(200);
    });
    return fut.wait();
  },
  updateRecurringTemplateImage:   function (id, imageType, url) {
    var Future = Npm.require("fibers/future"),
        fut    = new Future(),
        pfx    = "gugo-components.methods.updateRecurringTemplateImage",
        $set   = {};

    if (!id || !imageType || !url) {
      console.error(pfx + " error", { venueId: venueId, imageType: imageType, url: url });
      throw new Meteor.Error('invalid-parameter', 'Attempted to set venue image with invalid ID, imageType or URL');
    }

    $set["template.images." + imageType] = url;

    Gugo.collections.recurringEvents.update({
      _id: id
    }, {
      $set: $set
    }, function (err, docs) {
      if (err) {
        console.log(err);
        throw new Meteor.Error('mongo-error', err);
      }
      fut.return(200);
    });
    return fut.wait();
  },
  updateFeatureImage: function (featureId, imageType, url) {
    var Future = Npm.require("fibers/future"),
        fut    = new Future(),
        pfx    = "gugo-components.methods.updateFeatureImage",
        $set   = {};

    if (!featureId || !imageType || !url) {
      console.error(pfx + " error", { featureId: featureId, imageType: imageType, url: url });
      throw new Meteor.Error('invalid-parameter', 'Attempted to set feature image with invalid ID, imageType or URL');
    }

    //console.log("updateFeatureImage:", featureId, imageType, url);
    $set["images." + imageType] = url;

    Gugo.collections.features.update({
      _id: featureId
    }, {
      $set: $set
    }, function (err, docs) {
      if (err) {
        console.log(err);
        throw new Meteor.Error('mongo-error', err);
      }
      fut.return(200);
    });
    return fut.wait();
  }
});
