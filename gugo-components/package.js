Package.describe({
  name:          'gugo:components',
  version:       '0.0.1',
  // Brief, one-line summary of the package.
  summary:       '',
  // URL to the Git repository containing the source code for this package.
  git:           '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function (api) {
  api.use([
    'ecmascript',
    'templating',
    'blaze',
    'meteor-base',
    'underscore',
    'gugo:core',
    'fourseven:scss@3.4.1',
    'anti:fake@0.4.1',
    'react-meteor-data@0.2.9',
    'ultimatejs:tracker-react',
    'jeremy:geocomplete',
    'sergeyt:typeahead@0.11.1_8',
    'meteorhacks:subs-manager',
    'mdg:validated-method'
  ]);

  api.addFiles([
    'lib/init.js',
    'lib/methods.js'
  ], ['client', 'server']);

  api.addFiles([
    'server/images/imageUploader.js'
  ], ['server']);

  api.mainModule('client/index.js', 'client');

});

Package.onTest(function (api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('gugo:components');
  api.addFiles('gugo-components-tests.js');
});
