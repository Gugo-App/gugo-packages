import React from 'react';
import Article from 'meteor/gugo:components/client/article';

export default Privacy = React.createClass({
  render() {
    var body = <div>
      <h3>EFFECTIVE DATE: FEBRUARY 2016</h3>

      <p>
        At <strong>“GUGO”</strong>, we know you care about how your personal information is used and shared, and we take your privacy seriously. Please read the following to learn more about our Privacy Policy. By using or accessing our websites or the Services in any manner, you acknowledge that you accept the practices and policies outlined in this Privacy Policy, and you hereby consent that we will collect, use, and share your information in the following ways. Remember that your use of the Services is at all times subject to the GUGO Terms of Service, which incorporates this Privacy Policy. Any terms we use in this Privacy Policy without defining them have the definitions given to them in the Terms of Service.
      </p>

      <h3>What Does This Privacy Policy Cover?</h3>
      <p>
        This Privacy Policy covers our treatment of personally identifiable information (“Personal Information”) that we gather when you are accessing or using our Services, but not to the practices of companies we don’t own or control, or people that we don’t manage. We gather various types of Personal Information from our users and others, as explained in more detail below, and we use this Personal Information internally in connection with our Services, for example, to personalize, provide, and monitor and improve our Services, to allow you to set up a user account and profile, to contact you and allow other users to contact you through the Services, to respond to customer care and other inquiries, to process and fulfill your requests for certain products and services, and to analyze how you use the Services. In certain cases, we may also share some Personal Information with third parties, but only as described below.
      </p>
      <p>
        This Privacy Policy also covers our treatment of Personal Information about our customers’ users and other third parties that we receive from our customers and others. If you provide us with any Personal Information about another person, you represent and warrant that you have that person’s consent to do so and that such person has given explicit consent for us to collect, process, use and store their Personal Information for the purpose for which you have sent it to us and as set forth in this Privacy Policy and the Terms of Service.
        We do not knowingly collect or solicit personal information from anyone under the age of 13. If you are under 13, please do not attempt to register for the Services or send any personal information about yourself to us. If we learn that we have collected personal information from a child under age 13, we will delete that information as quickly as possible. If you believe that a child under 13 may have provided us personal information, please contact us at privacy@gugo.io
      </p>

      <h3>Will GUGO Ever Change This Privacy Policy?</h3>
      <p>
        We’re constantly trying to improve our Services, so we may need to change this Privacy Policy from time to time as well, but we will alert you to changes by placing a notice on our website or by some other means. Please note that if you’ve opted not to receive legal notice emails from us (or you haven’t provided us with your email address), those legal notices will still govern your use of the Services, and you are still responsible for reading and understanding them. If you use the Services after any changes to the Privacy Policy have been posted, that means you agree to all of the changes. Use of information we collect now is subject to the Privacy Policy in effect at the time such information is used.
      </p>

      <h3>What Information Does GUGO Collect?</h3>
      <p>
        <ul className="item-titles">
          <li>Information You Provide To Us Or Collected By The Services</li>
          <p>
            We receive and store any information that you provide to us. For example, through the registration process and/or through your account settings, requests for support through customer care, and otherwise using the Services, we may collect Personal Information such as your name, email address, location, phone number, and third-party account credentials (for example, your log-in credentials for Facebook or other third party sites). Certain information may be required to register with us or to take advantage of some of our features.
            In connection with your use of the Services, we may also collect information created or provided by you, or that we otherwise receive, in connection therewith.
          </p>
          <p>
            We may communicate with you if you’ve provided us the means to do so. For example, if you’ve given us your email address, we may email you about your use of the Services. Also, we may receive a confirmation when you open an email from us. This confirmation helps us make our communications with you more interesting and improve our services. If you do not want to receive communications from us, please indicate your preference by emailing unsubscribe@gugo.io
          </p>

          <li>Information Collected Automatically</li>
          <p>
            Whenever you interact with our Services, we may automatically receive and record information on our server logs from your browser or device, which may include your IP address, device identification, “cookie” information, the type of browser and/or device you’re using to access our Services, and the page or feature you requested. “Cookies” are identifiers we transfer to your browser or device that allow us to recognize your browser or device and tell us how and when pages and features in our Services are visited and by how many people. You may be able to change the preferences on your browser or device to prevent or limit your device’s acceptance of cookies, but this may prevent you from taking advantage of some of our features.
            Our advertising partners or customers may also transmit cookies to your browser or device, when you click on ads that appear on or through the Services. Also, if you click on a link to a third party website or service, a third party may also transmit cookies to you. Again, this Privacy Policy does not cover the use of cookies by any third parties, and we aren’t responsible for their privacy policies and practices. Please be aware that cookies placed by third parties may continue to track your activities online even after you have left our Services, and those third parties may not honor “Do Not Track” requests you have set using your browser or device.
            We may use the data described above to customize content for you that we think you might like, based on your usage patterns. We may also use it to improve the Services – for example, this data can tell us how often users use a particular feature of the Services, and we can use that knowledge to make the Services interesting to as many users as possible.
          </p>

          <li>Information Collected From Other Websites And Do Not Track Policy</li>
          <p>Through cookies we place on your browser or device, we may collect information about your online activity after you leave our Services. Just like any other usage information we collect, this information allows us to improve the Services and customize your online experience, and otherwise as described in this Privacy Policy. Your browser may offer you a “Do Not Track” option, which allows you to signal to operators of websites and web applications and services (including behavioral advertising services) that you do not wish such operators to track certain of your online activities over time and across different websites.</p>
        </ul>
      </p>

      <h3>Will GUGO Share Any Of The Personal Information It Receives?</h3>
      <p>
        We may share your Personal Information with third parties as requested or directed by you through the Services and as described below or elsewhere in this Privacy Policy:
      </p>
      <ul className="item-titles">
        <li>Information That Is No Longer Personally Identifiable</li>
        <p>
          We may anonymize your Personal Information so that you are not individually identified, and provide that information to our partners. We may also provide aggregate usage information to our partners, who may use such information to understand how often and in what ways people use our Services, so that they, too, can provide you with an optimal online experience. However, we never disclose aggregate usage information to a partner in a manner that would identify you personally, as an individual except as required to perform the Services or as otherwise stated in this Privacy Policy.
        </p>

        <li>Agents, Consultants, Affiliated Companies, And Similar Third Parties</li>
        <p>
          We employ other companies and people to perform tasks on our behalf and need to share your information with them to provide products or services to you; for example, data storage services, marketing services, database management services, and payment processing company to receive and process financial transactions for us. Unless we tell you differently, our agents do not have any right to use the Personal Information we share with them beyond what is necessary to assist us. Regardless of what country you reside or supply information from, you authorize us to use, process and store your information in the United States and any other country where we operate, which may have different rules, regulations and protections regarding privacy than those in your jurisdiction.
        </p>

        <li>User Profiles And Public Forum Submissions</li>
        <p>
          Certain user profile information, including name, location, and any text or other content that such user has submitted to a public forum on the Services, may be displayed to other users to facilitate user interaction within the Services or address your request for our services. Please remember that any Personal Information or other content you submit to public forum features on our Site, (e.g. on discussion boards, blog comments, chat areas, etc.) becomes publicly available, and can be collected and used by anyone. Your user name may also be displayed to other users in connection with the foregoing.
        </p>

        <li>Business Transfers</li>
        <p>
          We may choose to buy or sell assets, and may share and/or transfer customer and other user information in connection with the evaluation of and entry into such transactions. Also, if we (or our assets) are acquired, merged, reorganized, or if we go out of business, enter bankruptcy, or go through some other change of control or similar event, you acknowledge and explicitly consent that Personal Information could be one of the assets transferred to or acquired by a third party.
        </p>

        <li>Protection Of GUGO And Others</li>
        <p>
          Notwithstanding anything to the contrary, we reserve the right to access, read, preserve, and disclose any information that we believe is reasonably necessary to comply with law, legal obligations, regulations, law enforcement, governmental and other legal requests, or court order; enforce or apply our Terms of Service and other agreements; address fraud, security or technical issues; or protect the rights, property, or safety of GUGO, our employees, our users, or others.
        </p>
      </ul>

      <h3>Is Personal Information About Me secure?</h3>
      <p>
        Your account is protected by a password for your privacy and security. You must prevent unauthorized access to your account and Personal Information by selecting and protecting your password appropriately and limiting access to your computer or device and browser by signing off after you have finished accessing your account.
      </p>
      <p>
        We endeavor to protect the privacy of your account and other Personal Information we hold in our records, but unfortunately, we cannot guarantee complete security. Unauthorized entry or use, hardware or software failure, and other factors, may compromise the security of user information at any time.
      </p>

      <h3>What Personal Information Can I Access?</h3>
      <p>
        Through your account settings, you may access, and, in some cases, edit or delete the following information you’ve provided to us:
      </p>
      <ul>
        <li>name</li>
        <li>user name and password</li>
        <li>organization name</li>
        <li>email address</li>
        <li>location</li>
        <li>telephone number(s)</li>
        <li>payment information</li>
      </ul>
      <p>
        The information you can view, update, and delete may change as the Services change. If you have any questions about viewing or updating information we have on file about you, please contact us at privacy@gugo.io.
      </p>
      <p>
        Under California Civil Code Sections 1798.83-1798.84, California residents are entitled to ask us for a notice identifying the categories of Personal Information which we share with our affiliates and/or third parties for marketing purposes, and providing contact information for such affiliates and/or third parties. If you are a California resident and would like a copy of this notice, please submit a written request to: privacy@gugo.io or 2212 Queen Anne Ave N #514 Seattle WA 98109.
      </p>

      <h3>What Choices Do I Have?</h3>
      <p>
        Our customers can always opt not to disclose information to us, but keep in mind some information may be needed to register with us or to take advantage of some of our features.
      </p>
      <p>
        Our customers may be able to add, update, or delete information as explained above. When you update information, however, we may maintain a copy of the unrevised information in our records. You may request deletion of your account by emailing support@getupgetout.com. Some information may remain in our records after your deletion of such information from your account. We may use any aggregated data derived from or incorporating your Personal Information after you update or delete it, but not in a manner that would identify you personally unless otherwise stated in this Privacy Policy.
      </p>

      <h3>What If I have Questions About This Policy?</h3>
      <p>
        If you have any questions or concerns regarding our privacy policies, please send us a detailed message to privacy@gugo.io , and we will try to resolve your concerns.
      </p>
    </div>;

    return <Article title="GUGO Privacy Policy" body={body}/>
  }
});
