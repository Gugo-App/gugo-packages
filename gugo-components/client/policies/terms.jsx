import React from 'react';
import  Article  from 'meteor/gugo:components/client/article';


export default Terms = React.createClass({
  render() {
    console.log("RENDERING", Article);
    var body = <div>
      <h3>EFFECTIVE DATE: FEBRUARY 2016</h3>

      <p>
        Thank you for using <strong>"GUGO"</strong> (the <strong>"Service"</strong> or <strong>"Services"</strong>).  We are proud to offer our users an exciting and innovative way to receive information about their favorite activities and to plan and coordinate events with friends.
      </p>
      <p>
        Please read these Terms of Service (<strong>"Terms"</strong>) carefully, as they constitute legally binding terms and conditions that apply to your use of the Service, whether you are accessing the Service via a wireless or mobile device, a tablet, a personal computer or any other technology or device (each, a <strong>"Device"</strong>).   These Terms will remain in full force and effect while you use the Services and do not cover other services, websites or content made available by any other company or third party, unless specifically stated.  From time to time, we may modify these Terms.  We recommend that you check back frequently and review these Terms regularly so you are aware of the most current rights and obligations that apply to you.  Your use of the Services following the posting or other notification of changes will signify your agreement to be bound by the changes.
      </p>
      <p>
        By using the Service you expressly agree to comply with these Terms and any additional terms and conditions that we provide to you, including in connection with your use of other products and services we may offer or make available to you (<strong>"Additional Terms"</strong>). The Services may also provide rules of participation for certain activities and services, including contests, sweepstakes and other initiatives (<strong>"Rules"</strong>).  The Services’ Privacy Policy, the Additional Terms and the Rules are hereby incorporated into these Terms by reference as though fully set forth herein.  Any capitalized terms that are not defined in these Terms shall have the meaning ascribed in the Privacy Policy, Additional Terms and Rules, as applicable.
      </p>
      
      <h3>Overview of The Services</h3>
      <p>
        The Service may allow registered users to access certain features, functionality, information and services provided by us or our affiliates, which may include the ability to access personalized information and content regarding restaurants, events, festivals, or museums, such as notifications and recommendations regarding live concerts, performances, events, tickets, audio-visual content and other related products and services.
      </p>
      <p>
        Some portions of the Services (e.g., the GUGO Mobile App, etc.) may be dependent on and/or interoperate with third-party owned and/or operated platforms and services (e.g., Apple (iTunes, etc.), Facebook, Twitter, Instagram, Evite, LinkedIn, etc.) (each, a <strong>"Third Party Platform"</strong>) and may require that you be a registered member of the Third Party Platforms and provide certain account credentials and other information in order to access such Services. The Third Party Platforms may provide us with certain additional information about you, which may include your email address, legal name, country of residence, location, date of birth, and other preferences and usage data, all as more fully described herein and subject to the terms and conditions hereof, including the Privacy Policy.
      </p>

      <h3>Eligibility to Access the Services</h3>
      <p>
        Use of the Services is limited to users 13 years of age and older.  By using the Services, you represent and warrant that (a) you are 13 years of age or older and (b) your use of the Services does not violate (i) any applicable law, rule or regulation or (ii) any applicable terms, conditions or requirements of any provider of a Third Party Platform. Certain features of the Services may be subject to heightened age and/or other eligibility requirements and restrictions. If you are a user between the ages of 13 and 18, please review these Terms with your parent or guardian. Your parent or guardian should agree to these Terms on your behalf and parental discretion is advised for all users under the age of 18.
      </p>

      <h3>User Registration</h3>
      <p>
        In order to access and use certain content, features and functionality of the Services, we may require that you (a) register for the applicable Services, whether on the GUGO website, a Third Party Platform or otherwise, including, in some instances, creating and/or providing a username and password combination (<strong>"User Credentials"</strong>) and (b) provide to us and/or make available (e.g., via Third Party Platform permissions and consents, etc.) certain additional information, which may include your email address, legal name, country of residence, location, date of birth, other preferences, and other information (collectively, a <strong>"User Account"</strong>).
      </p>
      <p>
        You represent and warrant that all registration and account information you submit and/or make available is truthful and accurate and you shall maintain and promptly update the accuracy of such information. Further, if you elect to become a registered user of the Services, you are responsible for maintaining the confidentiality of your User Credentials, and you are responsible and liable for any access to or use of the Services by you or any person or entity using your User Credentials, whether or not such access or use has been authorized by you or on your behalf.  You agree to immediately notify GUGO of any unauthorized use of your User Credentials or User Account, or any other breach of security. It is your sole responsibility to (a) control the dissemination and use of your User Credentials and User Account, (b) update, maintain and control access to your User Credentials and User Account, and (c) cancel your User Account on the Services. GUGO is not be responsible or liable for any loss or damage arising from your failure to comply with these Terms.
      </p>

      <h3>Personal Information</h3>
      <p>
        We respect the privacy of all GUGO users and the use and protection of our Users’ personally identifiable information. Our information collection and use policies with respect to the privacy of such personal information are described in GUGO Privacy Policy.  We encourage you to read the GUGO Privacy Policy, and to use it to help make informed decisions regarding the collection, use and disclosure of your personal information. You acknowledge and agree that you are solely responsible for the accuracy and content of personal information.
      </p>

      <h3>Proprietary Rights</h3>
      <p>
        We require all GUGO users to respect our copyrights, trademarks, and other intellectual property rights, and likewise respect the intellectual property of others.  Content contained and/or made available through the Services (excluding User Postings, as defined below), audio/visual content, artwork, photographs, graphics, logos, trademarks, copy, text, computer code, software, information, materials and/or other intellectual property and/or proprietary rights therein (<strong>"Content"</strong>), may be owned, controlled and/or licensed by Get Up Get Out LLC or its licensors.  The Content may be protected by copyright, trademark and other applicable proprietary rights laws, including under United States federal and state, as well as applicable foreign laws, rules, regulations and treaties.
      </p>
      <p>
        All Services are to be used solely for your non-exclusive, non-assignable, non-transferable, non-commercial and limited personal use and for no other purposes.  You shall not, nor shall you allow any third party to, reproduce, modify, create derivative works from, display, perform, publish, distribute, disseminate, broadcast or circulate to any third party (including on or via a third party website or platform), or otherwise use, any Content (a) without the express, prior written consent of the respective owners, or (b) in any way that violates the rights of any third party. If you believe that the Services contain elements that infringe your copyrights in your work, please follow the procedures set forth in Section 10 below.
      </p>

      <h3>User Conduct</h3>
      <p>
        You are solely responsible for your conduct in connection with the Services. We want to keep the Services safe and enjoyable for everyone and the use of the Services for unlawful or harmful activities is expressly prohibited. You agree that, while using the Services, you will not:
      </p>
      <ul>
        <li>engage in or encourage conduct that would violate any applicable law, rule, regulation, judicial or government order or give rise to civil liability or violate or infringe upon any intellectual property, proprietary, privacy, moral, publicity or other rights of ours or of any other person or entity;</li>
        <li>submit, post, email, display, transmit or otherwise make available through the Services any material or take any action that is or is likely to be unlawful, harmful, threatening, abusive, tortious, defamatory, or is patently offensive, promotes racism, bigotry, hatred or physical harm of any kind against any group or individual;</li>
        <li>engage in or encourage conduct that affects adversely or reflect negatively on GUGO, the Services, our goodwill, name or reputation or causes duress, distress or discomfort to us or anyone else, or discourage any person or entity from using all or any portion, features or functions of the Services;</li>
        <li>submit, post, email, display, transmit or otherwise make available through the Services any material that contains a software virus, worm, spyware, Trojan horse or other computer code, file or program designed to interrupt, impair, destroy or limit the functionality of any computer software or hardware or telecommunications equipment;</li>
        <li>except as expressly permitted herein, use the Services for commercial or business purposes, including  pyramid schemes, advertising, marketing or offering goods or services or exploiting information or material obtained on, through or in connection with the Services, whether or not for financial or any other form of compensation or through linking with another website, platform or service;</li>
        <li>modify, disrupt, impair, alter or interfere with the use, features, function, operation or maintenance of the Services or the rights or use or enjoyment of the Services by any other user;</li>
        <li>impersonate any person or entity or falsely state or otherwise represent your affiliation with any person or entity;</li>
        <li>solicit passwords or personal identifying information for commercial or unlawful purposes from other users or engage in spamming, flooding, harvesting of email addresses or other personal information, "spidering", "screen scraping", "phishing", "database scraping", or any other activity with the purposes of obtaining lists of other users or other information, including transmitting or facilitating in the transmission of junk email, chain letters, duplicative or unsolicited messages; or</li>
        <li>modify, reverse engineer, decompile or disassemble any part of the Services, whether in whole or in part, or create any derivative works from any part of the Services, or encourage, assist or authorize any other person to do so.</li>
      </ul>

      <p>
        GUGO assumes no responsibility for removing, or monitoring the Services for, any inappropriate, false, incorrect, misleading, deceptive or unlawful content, information, materials or conduct. GUGO reserves the right to investigate and take appropriate action against anyone who, in GUGO’s sole discretion, violates, or is suspected of violating, these Terms, including removing any Content or User Posting from the Services at any time, and/or reporting you to law enforcement authorities.
      </p>
      
      <h3>User Postings</h3>
      <p>
        The Services may provide you and other users with an opportunity to submit, post, email, display, transmit or otherwise make available comments, reviews, links, materials, ideas, images, opinions, messages and other content and information via the Services (each, a <strong>"User Posting"</strong>, and collectively, <strong>"User Postings"</strong>).  When you submit User Postings, you may also be asked to provide information about you and your submission. You acknowledge and agree that all User Postings made by means of or in connection with any portion of the Services are public and that (a) you have no expectation of privacy in any User Posting, and (b) no confidential, fiduciary, contractually implied or other relationship is created between you and GUGO by reason of your transmitting a User Posting to any area of or in connection with the Services.
      </p>
      <p>
        In connection with all User Postings you submit, post, email, display, transmit or otherwise make available, you grant to GUGO the unrestricted, worldwide, non-exclusive, irrevocable, perpetual, fully paid-up  and royalty-free right and license, in any form or format, on or through any media or medium and with any technology or Devices now known or hereafter developed, to use, host, cache, store, maintain, use, reproduce, distribute, display, exhibit, perform, publish, broadcast, transmit, modify, prepare derivative works of, adapt, reformat, translate, and otherwise exploit all or any portion of your User Posting on the Services and in connection with any of GUGO businesses (including its successors and assigns), including for promotional purposes.
      </p>
      <p>
        You also represent, warrant and covenant that (a) you own the User Posting or are otherwise authorized to grant the rights, licenses and privileges described in these Terms and to perform and comply with all of the requirements set forth herein; (b) your submission, uploading, posting, emailing, displaying, transmission and/or making available of User Postings does not violate these Terms, any rights of any other party or entity, any of your obligations, any law, rule or regulation; (c) you have the legal right and capability to enter into these Terms and perform and comply with all of its terms and conditions; and (d) you hold and shall continue to hold all ownership, license, proprietary and other rights necessary to enter into, authorize, grant rights and perform your obligations under these Terms and shall pay for all royalties, fees, and any other monies owing to any person or entity by reason of your User Postings.
      </p>

      <h3>Digital Millennium Copyright Act</h3>
      <p>
        If you are a copyright owner or an agent thereof and believe that any content on the Services infringes upon your copyrights, your may submit a notification pursuant to the Digital Millennium Copyright Act (<strong>"DMCA"</strong>) by providing our Designated Agent (as set forth below) with the following information in writing (see 17 U.S.C. 512(c)(3) for further details):
      </p>
      <ul>
        <li>A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed;</li>
        <li>Identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works on the Services are covered by a single notification, a representative list of such works on the Services;</li>
        <li>Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled and information reasonably sufficient to permit us to locate the material;</li>
        <li>Information reasonably sufficient to permit us to contact you, such as an address, telephone number, and, if applicable, e-mail address;</li>
        <li>A statement that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent or the law; and</li>
        <li>A statement that the information in the notification is accurate, and under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</li>
      </ul>

      <p>
        Written notification of claimed infringement must be submitted to the following Designated Agent:
      </p>

      <p>
        Name and Address of Designated Agent:
        <br/>
        <br/>John Morrison
        <br/>2212 Queen Anne Ave N.
        <br/>#514
        <br/>Seattle WA 98109
        <br/>Email: legal@gugo.io
      </p>

      <p>
        For clarity, only DMCA notices should be sent to the Designated Agent and any other feedback, comments, requests for technical support, and other communications should be directed to GUGO customer service by sending an email to legal@gugo.io. You acknowledge and agree that if you fail to comply with all of the requirements of this section, your DMCA notice may not be valid.
      </p>

      <h3>Fee-Based Services</h3>
      <p>
        We may make available certain fee-based content and other e-commerce services on and/or in connection with the Services or through third party service providers (<strong>"Fee-Based Services"</strong>). For example, you may be able to purchase event tickets and songs, or order certain music-related products and/or licenses through the Services or third party service providers, including additional Services features and functionality. You acknowledge and agree that certain Fee-Based Services may utilize third party service providers (e.g., Ticketmaster, Apple, etc.), and all purchases made through these third party service providers are subject to their respective terms and conditions, and in the event of a conflict between such third party’s terms and conditions and these Terms, the terms and conditions of the third party service provider shall govern and control.  GUGO is not responsible and has no liability whatsoever for goods or services you obtain through our third party service providers or other web sites or web pages. We encourage you to make whatever investigation you feel necessary or appropriate before proceeding with any online transaction with any of these third parties.
      </p>
      <p>
        You may only use the Fee-Based Services if, and you hereby represent, warrant and agree that, (a) you are 18 years of age or older and a legal resident of the United States of America and (b) you shall pay in full the prices and fees (including  all applicable taxes) for any purchases you, or anyone using the User Account registered to you, make via PayPal, credit, debit or charge card or other payment means then acceptable to GUGO concurrent with your order. Certain payment means acceptable to GUGO may be subject to certain additional restrictions and conditions, including territory restrictions, bank/payment card restrictions, spending limits, third party service provider restrictions or otherwise, which may prevent the processing of your order. If a transaction has been declined online due to payment card or other payment service issues, please ensure all data is correct and resubmit. If the transaction is not accepted, you will be unable to use that card or payment method for your transaction and should use another card or payment method acceptable to GUGO and/or third party services providers, as applicable.
      </p>
      <p>
        GUGO or any of its third party service providers does not guarantee that product descriptions or other content will be accurate, complete, reliable, current, or error-free. Descriptions and images of, and references to, products on the Services do not imply our or any of our affiliates' endorsement of such products. GUGO and its third party operational service providers reserve the right, with or without prior notice, (i) to change the product descriptions, images, and references, (ii) to limit the available quantity of any product, (iii) to honor, or impose conditions on the honoring of, any coupon, coupon code, promotional code or other similar promotions, (iv) to bar any user from conducting any or all transaction(s) and (v) and/or to refuse to provide any user with any product. Prices and availability of any product and/or service are subject to change without notice.
      </p>

      <h3>Third Party Platforms, Services and Content</h3>
      <p>
        The Services may include links to or otherwise allow you to access URLs, hyperlinks, third party websites or third party materials, including Third Party Platforms (collectively, <strong>"Third Party Services"</strong>).  The inclusion of such Third Party Services does not constitute an endorsement by GUGO, or any association in connection therewith.  We do not verify, endorse, or have any responsibility for Third Party Services and any third party business practices, whether the Services', GUGO’s or its affiliates’ logos, marks, names and/or sponsorship or other identification is on the Third Party Services.  We will not have any responsibility or liability for any loss or damage caused by or related to your use of any Third Party Services.   Therefore, we encourage you to be aware when you leave the Services and to read the terms and conditions and privacy policy of each Third Party Service you use.
      </p>

      <h3>Data and Wireless Access Charges</h3>
      <p>
        Certain portions of the Services (e.g., the GUGO App) may require data access, and the provider of data access (e.g., network operator, wireless carrier, etc.) for your Device may charge you data access fees in connection with your use of such GUGO App and other Services, including wireless carrier messaging and other communication, messaging and data fees and charges. Under no circumstances will GUGO be responsible for any such data access fees and charges in connection with your use of any GUGO Apps or other Services, including wireless internet, email, text messaging or other charges or fees incurred by you (or any person that has access to your Device, telephone number, email address, User Account or other similar information). Further, the use or availability of certain GUGO App and other Services may be prohibited or restricted by your wireless carrier and/or data access provider, and not all Services may work with all wireless carriers, networks, platforms, services or Devices.
      </p>

      <h3>Messaging; Other Technology</h3>
      <p>
        We may provide you with emails, text messages, push notifications, alerts and other messages related to the Services, such as offers, products, events, and other promotions.  After downloading the GUGO App, you may be asked to accept or deny push notifications/alerts.  If you deny, you will not receive any push notifications/alerts.  If you accept, push notifications/alerts will be automatically sent to you.  If you no longer wish to receive push notifications/alerts from the GUGO App, you may opt out by changing your notification settings on your Device.  With respect to other types of messaging or communications, such as emails, text messages, etc., you can unsubscribe or opt out by either following the specific instructions included in such communications, or by emailing us with your request at privacy@getupgetout.com.
      </p>

      <h3>Advertisements</h3>
      <p>
        From time to time, you may choose to communicate with, interact with, or obtain Third Party Services from our advertisers, sponsors, or other promotional partners (collectively, <strong>"Advertisers"</strong>) found on or through the Services or via a hyperlinked website, service or platform. All such communication, interaction and participation is strictly and solely between you and such Advertisers and we shall not be responsible or liable to you in any way in connection with these activities or transactions.
      </p>

      <h3>Assignment</h3>
      <p>
        These Terms, and any rights, licenses and privileges granted herein, may not be transferred or assigned by you, but may be assigned or transferred by GUGO without restriction, notice or other obligation to you.
      </p>

      <h3>Indemnity</h3>
      <p>
        You agree to indemnify, defend and hold GUGO and its directors, officers, employees, representatives, agents, licensors, suppliers and service providers harmless from any and all claims, liabilities, damages, losses, costs and expenses (including reasonable attorneys’ fees), arising in any way out of or in connection with (a) your breach or violation these Terms or any applicable law, (b) any third party claims regarding your use of the Services, and (b) your User Postings.  GUGO reserves the right to assume the exclusive defense and control of any matter subject to indemnification by you and all negotiations for its settlement or compromise, and you agree to fully cooperate with us upon our request.
      </p>

      <h3>DISCLAIMER AND LIMITATIONS OF LIABILITY</h3>
      <p>
        THE SERVICES, AND ALL CONTENT, PRODUCTS, SERVICES AND USER POSTINGS MADE AVAILABLE ON, THROUGH OR IN CONNECTION THEREWITH, ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS, WITHOUT ANY REPRESENTATION, WARRANTY OR CONDITION OF ANY KIND, EXPRESS OR IMPLIED, OR ANY GUARANTY OR ASSURANCE THE SERVICES WILL BE AVAILABLE FOR USE, OR THAT ANY PRODUCTS, FEATURES, FUNCTIONS, SERVICES OR OPERATIONS WILL BE AVAILABLE OR PERFORM AS DESCRIBED. ALL IMPLIED REPRESENTATIONS, WARRANTIES AND CONDITIONS RELATING TO THE SERVICES, AND ALL CONTENT, PRODUCTS, SERVICES AND USER POSTINGS ARE HEREBY DISCLAIMED.  WITHOUT LIMITING THE FOREGOING, WE ARE NOT RESPONSIBLE OR LIABLE FOR ANY MALICIOUS CODE, DELAYS, INACCURACIES, ERRORS, OR OMISSIONS ARISING OUT OF YOUR USE OF THE SERVICES. YOU UNDERSTAND, ACKNOWLEDGE AND AGREE THAT YOU ARE ASSUMING THE ENTIRE RISK AS TO THE QUALITY, ACCURACY, PERFORMANCE, TIMELINESS, ADEQUACY, COMPLETENESS, CORRECTNESS, AUTHENTICITY, SECURITY AND VALIDITY OF ANY AND ALL FEATURES AND FUNCTIONS OF THE SERVICES. FURTHER, WITHOUT LIMITING THE FOREGOING, GUGO, ITS SUCCESSORS AND ASSIGNS, OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, REPRESENTATIVES, LICENSORS, PARTNERS, SERVICE PROVIDERS, ADVERTISERS AND SUPPLIERS MAKE NO REPRESENTATION, WARRANTY OR CONDITION OF ANY KIND, EXPRESS OR IMPLIED, REGARDING ANY PRODUCTS OR SERVICES ORDERED OR PROVIDED VIA THE SERVICES, AND HEREBY DISCLAIM, AND YOU HEREBY WAIVE, ANY AND ALL REPRESENTATIONS, WARRANTIES AND CONDITIONS OF ANY KIND, EXPRESS OR IMPLIED, MADE IN CONNECTION WITH PRODUCT OR SERVICES LITERATURE, FREQUENTLY ASKED QUESTIONS DOCUMENTS, ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU IN CONNECTION WITH THE SERVICES, INCLUDING CORRESPONDENCE WITH COMPANY OR ITS AGENTS OR OTHERWISE.
      </p>
      <p>
        YOU UNDERSTAND AND AGREE THAT, TO THE FULLEST EXTENT PERMISSIBLE BY LAW, GUGO, ITS SUCCESSORS AND ASSIGNS, OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, REPRESENTATIVES, LICENSORS, PARTNERS, SERVICE PROVIDERS, ADVERTISERS AND SUPPLIERS, SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE, OF ANY KIND, DIRECT OR INDIRECT, IN CONNECTION WITH OR ARISING FROM USE OF THE SERVICES OR FROM THESE TERMS, INCLUDING COMPENSATORY, CONSEQUENTIAL, INCIDENTAL, INDIRECT, SPECIAL OR PUNITIVE DAMAGES.
      </p>
      <p>
        YOU FURTHER UNDERSTAND AND ACKNOWLEDGE THE CAPACITY OF THE SERVICES, IN THE AGGREGATE AND FOR EACH USER, IS LIMITED. CONSEQUENTLY, SOME MESSAGES, CONTENT AND TRANSMISSIONS, INCLUDING USER POSTINGS, MAY NOT BE PROCESSED IN A TIMELY FASHION OR AT ALL, AND SOME FEATURES OR FUNCTIONS MAY BE RESTRICTED OR DELAYED OR BECOME COMPLETELY INOPERABLE. AS A RESULT, YOU ACKNOWLEDGE AND AGREE THAT COMPANY ASSUMES NO LIABILITY, RESPONSIBILITY OR OBLIGATION TO TRANSMIT, PROCESS, STORE, RECEIVE OR DELIVER TRANSACTIONS OR USER POSTINGS OR FOR ANY FAILURE OR DELAY ASSOCIATED WITH ANY USER POSTINGS AND YOU ARE HEREBY EXPRESSLY ADVISED NOT TO RELY UPON THE TIMELINESS OR PERFORMANCE OF THE SERVICES FOR ANY TRANSACTIONS OR USER POSTINGS. SOME JURISDICTIONS DO NOT ALLOW FOR THE EXCLUSION OF CERTAIN WARRANTIES OR CERTAIN LIMITATIONS ON DAMAGES AND REMEDIES, ACCORDINGLY SOME OF THE EXCLUSIONS AND LIMITATIONS DESCRIBED IN THESE TERMS MAY NOT APPLY TO YOU.
      </p>

      <h3>Governing Law; Miscellaneous</h3>
      <p>
        These Terms and your use of the Services is governed by, construed and enforced in accordance with the internal substantive laws of the State of Washington (notwithstanding the state's conflict of laws provisions) applicable to contracts made, executed and wholly performed in Washington, and, for the purposes of any and all legal or equitable actions, you specifically agree and submit to the exclusive jurisdiction and venue of the State and Federal Courts situated in the State and County of King County, Washington and agree you shall not object to such jurisdiction or venue on the grounds of lack of personal jurisdiction, forum non conveniens or otherwise.
      </p>
      <p>
        These Terms contains the entire understanding and agreement between you and GUGO concerning the Services and supersedes any and all prior or inconsistent understandings relating to the Services and your use thereof. These Terms cannot be changed orally. If any provision of these Terms is held to be illegal, invalid or unenforceable, this shall not affect any other provisions and these Terms shall be deemed amended to the extent necessary to make it legal, valid and enforceable. The terms "include," "includes," and "including," whether or not capitalized, mean "include, but are not limited to," "includes, but is not limited to," and "including, but not limited to," respectively and are to be construed as inclusive, not exclusive.  Any provision which must survive in order to allow us to enforce its meaning shall survive the termination of these Terms; however, no action arising out of these Terms or your use of the Services, regardless of form or the basis of the claim, may be brought by you more than one year after the cause of action has arisen (or if multiple causes, from the date the first such cause arose). The failure of GUGO to exercise or enforce any right or provision of these Terms will not operate as a waiver of such right or provision.
      </p>
      <p>
        The Services are controlled by GUGO from its offices in the United States, and GUGO makes no representation or warranty that the Services or Content contained on or made available in connection therewith is legal, appropriate or available for use in other locations. Those who choose to access the Services from other locations do so at their own risk and are responsible for compliance with any and all local laws, rules and regulation, if and to the extent local laws, rules and regulations are applicable. No software made available in connection with the Services may be downloaded, exported or re-exported into (or to a national or resident of) any countries that are subject to United States export restrictions.
      </p>
      <p>
        We reserve the right to terminate or deny access, use and registration privileges to any user of the Services at any time, for any or no reason, with or without prior notice, and without obligation to you or any third party.   You acknowledge, consent and agree that GUGO may access and disclose your account and registration information if required to do so by law or if based on a good faith belief that such access or disclosure is necessary to comply with the legal process, enforce the Agreement, response to claims that any content or information violates the rights to any third party, respond to customer service requests, or otherwise protect the rights, property or safety of GUGO, its users or third parties.
      </p>
      <p>
        To the extent that there is a conflict between these Terms and the Additional Terms, the Additional Terms shall govern. To the extent that there is a conflict between these Terms and the specific Rules for the activity in which you choose to participate, the Rules shall govern. To the extent that there is a conflict among these Terms, the Additional Terms and/or the Rules, the following order of precedence shall apply: first, the Rules, second, the Additional Terms, and third, the Agreement.
      </p>
      <p>
        These Terms was last modified on the date indicated above and are effective immediately.
      </p>

    </div>;

    return <Article title="GUGO Terms of Service" body={body}/>
  }
});
