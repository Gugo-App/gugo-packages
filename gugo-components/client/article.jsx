import React from 'react';

export default Article = React.createClass({
  getDefaultProps() {
    return {
      bodyFontSize: 14,
      color:        "#424242",
      padding:      "1% 5%"
    }
  },
  render() {
    return <div className="comp-article" style={{padding: this.props.padding}}>
      <h1>{this.props.title}</h1>
      <div style={{fontSize: this.props.bodyFontSize, color: this.props.color}}>
        {this.props.body}
      </div>
    </div>
  }
});
