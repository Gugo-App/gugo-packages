// general loading circle for when content is waiting to load
import React from 'react';
import { FlexBox } from 'meteor/gugo:components/client/flex/FlexBox';
import { CircularProgress } from 'material-ui';

let container = {
  height: "100%",
  width : "100%"
};

export default FullRingLoader = React.createClass({
  getDefaultProps() {
    return {
      size: 2
    }
  },
  render() {
    return <FlexBox alignItems="center" flexDirection="column" style={container} >
      <div style={{flexGrow : "1"}}></div>
      <CircularProgress mode="indeterminate" size={this.props.size}/>
      <div style={{flexGrow : "1"}}></div>
    </FlexBox>
  }
});
