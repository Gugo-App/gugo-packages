// overlay loader when waiting for an action to complete (creating bulletin, logging out, deleting an account, etc.)
// can include a message about the event it's waiting for
import React from 'react';
import { TrackerReactMixin } from 'meteor/ultimatejs:tracker-react';
import { CircularProgress } from 'material-ui';

let container = {
  height: "100%",
  width : "100%",
  position : "absolute",
  zIndex : "99999",
  backgroundColor: "rgba(0,0,0,.3)"
};

export const OverlayLoader = {
  show(message) {
    var options = {};

    if(message) {
      options.message = message;
    }

    Session.set('overlayRingLoader', options)
  },
  hide() {
    Session.set('overlayRingLoader', null);
  }
};

export const OverlayRingLoader = React.createClass({
  mixins : [TrackerReactMixin],
  status() {
    return Session.get('overlayRingLoader');
  },
  render() {
    var status = this.status();
    var msg = null;
    if(!status) {
      return null;
    }


    if(status.message) {
      msg = <h3> {status.message} </h3>
    }

    return <FlexBox alignItems="center" flexDirection="column" style={container} >
      <div style={{flexGrow : "1"}}></div>
      <CircularProgress mode="indeterminate" size={2}/>
      { msg }
      <div style={{flexGrow : "1"}}></div>
    </FlexBox>
  }
});