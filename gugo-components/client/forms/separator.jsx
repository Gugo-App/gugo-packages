import React from 'react';

import { Styles } from 'material-ui';
import { ThemeManager, LightRawTheme } from 'material-ui/lib/styles';
import AppConfig from 'meteor/gugo:components/client/config/appConfig';

export const HorizontalSeparator = React.createClass({
  getDefaultProps() {
    return {
      height:      100,
      text:        "Or",
      isUpperCase: true,
      lineColor:   Styles.Colors.grey300,
      fontColor:   Styles.Colors.grey500,
      fontSize:    ".55rem",
      bgColor:     AppConfig.palette.white
    }
  },
  render() {
    var wrapStyle   = {
          position:        "relative",
          margin:          "0 auto",
          padding:         0,
          WebkitBoxSizing: "border-box",
          MozBoxSizing:    "border-box",
          boxSizing:       "border-box",
          height:          this.props.height || 50
        },
        beforeStyle = {
          content:         "",
          position:        "absolute",
          width:           "100%",
          height:          1,
          top:             "50%",
          left:            0,
          zIndex:          -1,
          backgroundColor: this.props.lineColor
        },
        lineStyle   = {
          textAlign: "center",
          position:  "absolute",
          top:       "50%",
          left:      "50%",
          width:     "40%",
          transform: "translate(-50%, -50%)",
          margin:    0
        },
        textStyle   = {
          padding:         "1rem",
          display:         "inline-block",
          color:           this.props.fontColor,
          fontSize:        this.props.fontSize,
          backgroundColor: this.props.bgColor,
          paddingTop:      0,
          paddingBottom:   0,
          fontWeight: "light",
          textTransform: "uppercase"
        };

    var text = this.props.isUpperCase ? this.props.text.toUpperCase() : this.props.text;

    return (
      <div style={wrapStyle}>
        <div style={beforeStyle}></div>
        <h3 style={lineStyle}>
          <span style={textStyle}>{text}</span>
        </h3>
      </div>
    );
  }
});

export const HorizontalLine = React.createClass({
  render() {
    let horizontalLine = {
      borderTop : "solid lightgray 1px",
      width: "100%"
    };
    return <div style={horizontalLine} ></div>
  }
});