import React from 'react';

class HTMLDiv extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {
    return <div
      className={this.props.className}
      id={this.props.id}
      dangerouslySetInnerHTML={ { __html : this.props.html } }
      style={this.props.style} />
  }

  componentDidMount() {
    $("a").on('click', function (e) {
      //Prevents Default Behavior
      e.preventDefault();

      var url = $(this).attr('href');
      url = decodeURIComponent(url);
      console.log("URL", url);

      // Calls Your Function with the URL from the custom data attribute
       if(Meteor.isCordova && cordova.InAppBrowser) {
         cordova.InAppBrowser.open(url, '_system');
       } else {
         window.open(url);
       }
    });
  }
};

HTMLDiv.defaultProps = {
  style : {},
  html : <div></div>
}

class HTMLP extends React.Component {
  constructor(props) {
    super(props);
    this.props = {
      style : {},
      html : <p></p>
    }
  }

  render() {
    return <p
      dangerouslySetInnerHTML={ { __html : this.props.html } }
      style={this.props.style} />
  }

  componentDidMount() {
    $("a").on('click', function (e) {
      //Prevents Default Behavior
      e.preventDefault();

      var url = $(this).attr('href');
      url = decodeURIComponent(url);
      console.log("URL", url);

      // Calls Your Function with the URL from the custom data attribute
      if(Meteor.isCordova && cordova.InAppBrowser) {
        cordova.InAppBrowser.open(url, '_system');
      } else {
        window.open(url);
      }
    });
  }
};

export { HTMLDiv, HTMLP }