import React from 'react';
import {Checkbox} from 'material-ui';

export default SnippetCheckboxes = React.createClass({
	getCheckboxValues() {
		var self = this;
		var values = [];
		_.each(Gugo.enums.snippetDefaults, snippet => {
			if (self.refs[snippet.text].isChecked()) {
				values.push(snippet.value);
			}
		});

		return values;
	},
	renderCheckboxes() {
		var self = this;
		var elements = [];
		_.each(Gugo.enums.snippetDefaults, snippet => {
				elements.push(<Checkbox 
					label={snippet.text} 
					labelStyle={this.props.labelStyle}
					onCheck={this.getCheckboxValues}
					key={snippet.text}
					ref={snippet.text}
					defaultChecked={_.contains(self.props.defaultValues, snippet.value)}/>);		
		});	

		return elements;
	},
	render() {
		return <div>{this.renderCheckboxes()}</div>
	}
})