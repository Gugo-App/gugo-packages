import React from 'react';

import { TextField, AutoComplete } from 'material-ui';

let autocompleteInput = {
  width: "100%"
};

var types = [
  "geocode",
  "establishment"
];

export const GooglePlaces = {
  addressComponents: {
    streetNumber: { type: "street_number", value: "short_name" },
    streetName:   { type: "route", value: "short_name" },
    city:         { type: "locality", value: "short_name" },
    state:        { type: "administrative_area_level_1", value: "short_name" },
    country:      { type: "country", value: "short_name" },
    zip:          { type: "postal_code", value: "short_name" }
  }
};

// autocompletes with locations from Google map's database
export const PlacesAutoCompleteInput = React.createClass({
  render() {
    return <div>
      {/*<p style={{ fontWeight : "bold", color : "black" }}> Event Location </p>*/}
      <input className="box-input" value={this.props.value} style={ { display : "inline", border: "none" } } type="text"
             ref="autocomplete" onChange={this.props.onChange} onBlur={this.props.onChange} placeholder="Location"/>
    </div>
  },
  componentDidMount() {
    $(this.refs.autocomplete).geocomplete({
      types: types
    }).bind("geocode:result", this.props.onGeocodeResult);
  }
});

export const PlacesAutoComplete = React.createClass({
  getDefaultProps() {
    return {
      onGeocodeResult: function () {
      },
      onChange:        function () {
      }
    }
  },
  mixins: [ReactMeteorData],
  getMeteorData() {
    return {
      isLoading: !GoogleMaps.loaded()
    }
  },
  render() {
    if (this.data.isLoading) {
      return <div>
        <p style={{ fontWeight : "bold", color : "black" }}> Event Location </p>
        <input className="box-input" style={ { display : "block" } } type="text" placeholder="Loading Google Places"/>
      </div>
    }

    return <PlacesAutoCompleteInput value={this.props.value} onChange={this.props.onChange}
                                    onGeocodeResult={this.props.onGeocodeResult}/>
  }
});

export class TextSearchInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value : ""
    };

    this.onChange = this.onChange.bind(this);

  }

  onChange(evt) {
    const val = evt.target.value;
    this.setState({
      value : val
    })

    this.props.onUpdate && this.props.onUpdate(val);

    // if(val.length > this.props.charsRequired - 1) {
    //   this.props.onUpdate && this.props.onUpdate(val);
    // } else {
    //   this.props.onUpdate && this.props.onUpdate("");
    // }
  }

  render() {
    return <TextField onChange={this.onChange} value={this.state.value} hintText={this.props.hintText} />
  }
}

TextSearchInput.defaultProps = {
  autoSearch  : true,
  charsRequired : 3
};

