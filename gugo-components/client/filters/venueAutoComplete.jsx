import React from 'react';
import {AutoComplete, MenuItem} from 'material-ui';
import { createContainer } from 'meteor/react-meteor-data';

var SubsMgr = new SubsManager({
	cacheLimit: 9999,
  	expireIn : 45
});

class VenueAutoComplete extends React.Component {
	constructor(props) {
		super(props);

		// this.state = {
		// 	defaultVenue: this.props.defaultVenue || null
		// }
	}

	// componentWillReceiveProps(nextProps) {
	// 	this.setState({
	// 		defaultVenue: this.props.defaultVenue
	// 	});
	// }

	getDataSource() {
		var venues = Gugo.collections.venues.find().fetch();

		return venues.map(venue => {
			return {
				text: venue.name,
				value: (
					<MenuItem
					primaryText={venue.name}
					onTouchTap={this.props.venueSelected.bind(this, venue._id)} />
				)
			}
		});
	}

	render() {
		if (this.props.isLoading) {
			return <div></div>;
		}

		var venues = this.props.venues;
		
		console.log("Venues", venues);
		if(venues.length === 0) return <div> </div>

		var venueText = '';
		if (this.props.defaultVenue) {
			var venue = Gugo.collections.venues.findOne(this.props.defaultVenue);
			if(venue) {
				venueText = venue.name;
			}
		}


		return <div>
		<AutoComplete
          hintText="Name of Venue"
          filter={AutoComplete.caseInsensitiveFilter}
          dataSource={this.getDataSource()}
          searchText={venueText}/> 
          </div>
	}
};

export default createContainer(({params}) => {
    var handle = SubsMgr.subscribe("allVenues");
    var venueSelected = params.venueSelected;
    var defaultVenue = params.defaultVenue;
    console.log("VENUE AutoComplete", params);

    return {
      isLoading: !handle.ready(),
      venues:     Gugo.collections.venues.find().fetch(),
      venueSelected: venueSelected,
      defaultVenue: defaultVenue
    }
}, VenueAutoComplete);

