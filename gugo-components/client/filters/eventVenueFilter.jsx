import React from 'react';
import {AutoComplete, MenuItem, TextField, FloatingActionButton} from 'material-ui';
import FullRingLoader from '../loaders/fullRingLoader';
import { EventVenueSearch } from 'meteor/gugo:components/lib/methods';

export class TitleVenueAutoComplete extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      data : []
    };

    this.onUpdateInput = this.onUpdateInput.bind(this);
  }

  getDataSource() {
    return this.data.map( dataPoint => {
      return <MenuItem />
    })
  }

  onUpdateInput(evt) {
    console.log("Updated input", evt);
  }

  render() {
    return <AutoComplete
        hintText="Name of Venue"
        noFilter={true}
        filter={AutoComplete.caseInsensitiveFilter}
        dataSource={this.getDataSource()}
        onUpdateInput={this.onUpdateInput} />
  }
}

//This is the search used in the application
export class EventVenueTextSearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      text : "",
      data : [],
      isLoading : false
    };

    this.onChangeInput = this.onChangeInput.bind(this);
  }

  queryData(text) {
    var self = this;

    if(self.intervalBeforeSearch) {
      Meteor.clearTimeout(self.intervalBeforeSearch);
    }

    self.intervalBeforeSearch = Meteor.setTimeout( () => {
      self.setState(_.extend(self.state, { isLoading : true }));
      if(text !== "") {
        EventVenueSearch.call({text}, (err, res) => {
          //console.log("Err", err, "res", res);
          self.setState(_.extend(self.state, {data: res, isLoading : false}))
          self.props.onReceivedData && self.props.onReceivedData(res, this.state.text);
        })
      } else {
        self.props.onReceivedData && self.props.onReceivedData([], this.state.text);
      }
    }, 500);
  }

  onChangeInput(evt) {
    var updt = {};
    updt.text = evt.target.value;

    this.queryData(updt.text);

    this.setState(_.extend(this.state, updt));
  }

  onClickClear() {
    this.setState({
      text : "",
      data : [],
      isLoading : false
    });

    this.props.onReceivedData && this.props.onReceivedData([], this.state.text);
  }

  getClearTextbutton() {
    return <i key={0} style={{ position : "absolute", top : 35, right : 13, color : "gray", fontSize : 20 }} className="fa fa-times-circle" onTouchTap={this.onClickClear.bind(this)} />;
  }

  getLoader() {
    console.log("LOADER", FullRingLoader);
    return <div style={ { position : "absolute", top : -5, right : 35 } }> <FullRingLoader size={.35}/> </div>
  }

  render() {
    const clearTextButton = this.state.text === "" ? null : this.getClearTextbutton();
    const loader = this.state.isLoading ? this.getLoader() : null;

    return <div>
      <div style={{margin: "0 auto", textAlign: "center", paddingTop: "20px"}}>
      <TextField fullWidth={false} style={{border: "1px solid #C4C4C4", padding: "0 10px", width: "70%", borderRadius: "3px"}} underlineShow={false} hintText="Event Title Or Venue Name" value={this.state.text} onChange={this.onChangeInput} />
      </div>
      {loader}
      {clearTextButton}
    </div>
  }
}

EventVenueSearch.intervalBeforeSearch = null;