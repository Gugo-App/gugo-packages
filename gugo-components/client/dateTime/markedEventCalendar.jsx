import React from 'react';

import MobiCalendar from 'meteor/gugo:components/client/dateTime/calendar';

// calendar that has dots marking dates where the user has an event listed
MarkedEventCalendar = React.createClass({
  mixins : [ReactMeteorData],
  getInitialState() {
    return {
      currentMonth : new Date()
    }
  },
  instance : null,
  getMeteorData() {
    var ownedHandle = SubsMgr.subscribe('myUserEvents');
    var intEventsHandle = SubsMgr.subscribe('myInterestedEvents');
    var calEventsHandle = SubsMgr.subscribe('myCalendarEvents');

    var mo = this.state.currentMonth;
    var start = new Date(moment(mo).startOf('month'));
    var end = new Date(moment(mo).endOf('month'));


    return {
      isLoading : !(ownedHandle.ready() && calEventsHandle.ready() &&  intEventsHandle.ready()),
      events : Gugo.collections.userEvents.find({
        ownerId : Meteor.userId(),
        startDate : { $gte : start },
        endDate : { $lte : end }  }).fetch()
    }
  },
  convertMS(ms) {
    var d, h, m, s;
    s = Math.floor(ms / 1000);
    m = Math.floor(s / 60);
    s = s % 60;
    h = Math.floor(m / 60);
    m = m % 60;
    d = Math.floor(h / 24);
    h = h % 24;
    return { d: d, h: h, m: m, s: s };
  },
  createDateArr(lowest, highest) {
    if (this.dateEquals(lowest, highest)) {
      return [lowest]
    }

    var arr    = [lowest];
    var cursor = new Date(lowest);

    // console.log("Arr", arr, "Cursor", cursor, "Lowest", lowest);

    while (!this.dateEquals(cursor, highest)) {
      cursor.setDate(cursor.getDate() + 1);
      // console.log("Cursor now at", cursor.getDate());
      arr.push(new Date(cursor))
    }

    // console.log("Created Date arr", arr);

    return arr;
  },
  dateEquals(a, b) {
    return a.getDate() === b.getDate()
      && a.getMonth() === b.getMonth()
      && a.getYear() === b.getYear();
  },
  buildCalendarMarkers() {
    var calendarEvents = [];
    var self = this;

    // console.log("Building calendar markers", this.data.events);

    _.each(this.data.events, function(event) {
      if(event.startDate && event.endDate) {
        var converted = self.convertMS(event.endDate.getTime() - event.startDate.getTime());
        if(converted.d >= 1) {
          var arr = self.createDateArr(event.startDate, event.endDate);
          calendarEvents = calendarEvents.concat(arr);
        } else {
          calendarEvents.push({
            d : event.startDate,
            color : (event.source && event.source.name) === "GoogleCalendar" ? "red" : "red"
          });
        }
      }
    });

    return calendarEvents;
  },
  onMonthChange(year, month, inst) {
    var dt = new Date();
    dt.setYear(year);
    dt.setMonth(month);

    var state = this.state;
    state.currentMonth = dt;
    this.setState(state);

    this.instance = inst;
  },
  onInit(inst) {
    // console.log("Calendar initialized", inst);
    this.instance = inst;
  },
  getCalendarRef() {
    return this.refs['mobi-cal']
  },
  clear() {
    if(this.instance) {
      this.instance.clear();
    }
  },
  render() {
    // console.log("DATA", this.data, this.instance);

    var options = this.props.calendarOptions;

    if(this.instance) {
      const marked = this.buildCalendarMarkers();
      this.refs['mobi-cal'].setMarked(marked, this.instance);
    } else if(!this.instance && !this.data.isLoading) {
      const marked = this.buildCalendarMarkers();
      options.marked = marked;
    }


    return <MobiCalendar ref="mobi-cal" options={options} onSelectDate={this.props.onSelectDate} onMonthChange={this.onMonthChange} onInit={this.onInit} />

  }
});