import React from 'react';

// basis for calendar used in organize tab and in date picker when creating bulletin
export default class MobiCalendar extends React.Component {

  constructor(props) {
    super(props);

    this.calendar = null;
    this.setMarked = this.setMarked.bind(this);
  }


  render() {
    return <div>
      <div ref="cal-container" style={{ height: "100%", width: "100%" }}> </div>
    </div>
  }

  setMarked(marked, inst) {
    inst.settings.marked = marked;

    inst.refreshing = true;
    inst.refresh();
    inst.refreshing = false;
  }

  componentDidMount() {
    this.calendar = $(this.refs['cal-container']);

    var options = this.props.options;

    if(this.props.onSelectDate) {
      options.onSetDate = this.props.onSelectDate;
    }

    if(this.props.onMonthChange) {
      options.onMonthChange = this.props.onMonthChange;
    }

    if(this.props.onInit) {
      options.onInit = this.props.onInit;
    }


    this.calendar.mobiscroll().calendar(options);
  }

};