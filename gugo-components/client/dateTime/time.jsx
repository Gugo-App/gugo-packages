import React from 'react';
import { TextField } from 'material-ui';

// basis for selecting times for creating bulletins
export const MobiTime = React.createClass({
  render() {
    return <div ref="time-container" style={{display : "inline-block" }}></div>
  },
  decodeTime(value) {
    if (value === null) {
      return;
    }

    var parsedValues = value.split(/:| /),
        chunks       = {
          hours:   parsedValues[0],
          minutes: parsedValues[1],
          period:  parsedValues[2]
        };

    //console.log("decodeTime", value);
    //console.log("Parsed Value", parsedValues);
    //console.log("Time decoded", chunks);

    return chunks;
  },
  _onSetTimeIntercept(timeString, inst) {
    const decoded = this.decodeTime(timeString);
    this.props.onSetTime(decoded, inst)
  },
  componentDidMount() {
    var $dt = $(this.refs["time-container"]);
    //console.log("Time Mounting", this.props);

    var options = this.props.options;

    if (this.props.onSetTime) {
      options.onClosed = this._onSetTimeIntercept;
    }

    if (options.stepMinutes) {
      options.steps = {
        minute: options.stepMinutes
      }
    }

    $dt.mobiscroll().time(options);

    if (this.props.handler) {
      const $handle = $("#" + this.props.handler);

      //console.log("Setting up handler for", this.props.handler);
      $dt.mobiscroll("tap", $handle, function () {
        //console.log("Handler tapped");
        $dt.mobiscroll("show");
      })
    }
  }
});

export const MobiTimeWithTextField = React.createClass({
  tag :  '[Mobi Time With Text Field]',
  getInitialState() {
    return {
      date : null,
      time : null
    }
  },
  onSetTime(value) {
    this.mergeTimeVals(this.state.date, value);
  },
  mergeTimeVals(date, times) {
    var state       = this.state,
      dt     = this.constructDateTime(date, times);
    state.date = dt;
    state.time = this.encodeTime(dt);
    //console.log("[Mobi On Set]", state.date, state.time);

    this.props.onChange && this.props.onChange(state.date);

    this.setState(state);
  },
  //This function takes a date object, and rips the hours / minutes / period from it
  //Returns an encoded object that we usually get from decoding a mobiscroll time
  //Is used mostly for intaking a previously selected date, for instance, the create event, or edit event view
  encodeTime(dt) {
    if (!dt) {
      return null;
    }

    var period = dt.getHours() >= 12 ? "PM" : "AM";

    //console.log("[Encode Time] Date", dt, dt.getHours(), dt.getMinutes());

    return {
      minutes: dt.getMinutes(),
      hours:   period === "PM" && dt.getHours() !== 12 ? dt.getHours() - 12 : dt.getHours(),
      period:  period
    }
  },
  //Parses our encoded time object into a string friendly representation
  //Returns TBD if null
  constructDateTime(date, time) {
    var dt      = date && date instanceof Date ? new Date(date) : new Date(),
      timeObj = {
        hours:   time ? parseInt(time.hours) : 0,
        minutes: time ? parseInt(time.minutes) : 0,
        period:  time ? time.period : "PM"
      };

    //console.log("constructDateTime", date, timeObj);

    //If we have period is PM we need to add 12 hours to the date
    if (timeObj.period === "PM" && timeObj.hours !== 12) {
      timeObj.hours += 12;
    } else if(timeObj.period === "AM" && timeObj.hours === 12) {
      timeObj.hours -= 12;
    }

    dt.setHours(timeObj.hours);
    dt.setMinutes(timeObj.minutes);

    return dt;
  },
  getTimeString(obj) {
    var mins;
    //console.log("getTimeString", obj);

    if (!obj) {
      return null;
    }

    if(obj instanceof Date) {
      //console.log(this.tag, "object is instance of DATE")
      obj = this.encodeTime(obj);
    }

    mins = obj.minutes.toString().length > 1 ? obj.minutes : "0" + obj.minutes;
    return obj.hours + ":" + mins + " " + obj.period;
  },
  receiveDateProp(props) {
    if(props.mobiOptions.defaultValue) {
      var def = props.mobiOptions.defaultValue;
      var tVals = null;
      var updt = {};

      //console.log("Default before:", def);

      if(def instanceof Date) {
        tVals = this.encodeTime(def);
      }

      updt.time = tVals;
      updt.date = def;

      //console.log("Default:", def);


      this.setState(_.extend(this.state, updt));
    }
  },
  componentWillMount() {
    //console.log("[Mobi Time With Text Field]  Mounting");
    this.tag = '[Mobi Time With Text Field]' + this.props.handler;
    this.receiveDateProp(this.props);
  },
  componentWillReceiveProps(nextProps) {
    //console.log(this.tag, "Received Props", nextProps);
    this.receiveDateProp(nextProps);
  },
  componentDidUpdate() {
    //console.log(this.tag, "Component Did Update");
  },
  render() {
    var value = this.getTimeString(this.state.time || this.props.mobiOptions.defaultValue);

    //console.log("RENDER", this.tag, "Date Value: ", value, this.state.time, this.props.mobiOptions.defaultValue);

    return (
      <div>
        <TextField id={this.props.handler} hintText={this.props.fieldOptions.hint} value={value} />
        <MobiTime options={this.props.mobiOptions} handler={this.props.handler} onSetTime={this.onSetTime} />
      </div>
    )
  }
})