// a static map image with marker from google's API that's used in details for public events and in the web view for public events
import React from 'react';

export default class StaticMap extends React.Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    var source = "https://maps.googleapis.com/maps/api/staticmap?center=";
    var coordinates = this.props.coordinates;
    var address = this.props.streetAddress + this.props.city + this.props.state;
    var fullAddress = this.props.fullAddress;
    var sourceEnd = "&zoom=16&size=400x200&maptype=roadmap";
    var markers = "&markers=color:red%7C"
    var src;

    // handling all the possible permutations of address components woo hoo
    if (this.props.coordinates) {
      src = source + coordinates[1] + "," + coordinates[0] + sourceEnd + markers + coordinates[1] + "," + coordinates[0];
      // console.log(src);
    } else if (this.props.streetAddress && this.props.city && this.props.state) {
      src = source + address + sourceEnd + markers + address;
      // console.log(src);
    } else if (this.props.fullAddress) {
      src = source + fullAddress + sourceEnd + markers + fullAddress;
    }else {
      src = source + "Seattle,WA" + sourceEnd + markers + "Seattle,WA";
      // console.log(src);
    }
    
    return <img
      src={src}
      style={this.props.style}
      id={this.props.id} />
  }
};