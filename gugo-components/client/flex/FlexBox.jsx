import React from 'react';

const flexBoxStyle = {
};

// allows you to set flexbox properies as props inline instead of needing a separate style property in a div
export const FlexBox = React.createClass({
  render() {
    var className = "flex-box-compat";

    if(this.props.className) {
      className += " " + this.props.className;
    }

    var style = _.clone(flexBoxStyle);

    if(this.props.flexDirection) {
      style["WebkitFlexDirection"] = this.props.flexDirection;
      style["WebkitBoxDirection"] = this.props.flexDirection;
      style["WebkitFlexFlow"] = this.props.flexDirection;
      style.flexDirection = this.props.flexDirection;
    }

    if(this.props.alignItems) {
      style['WebkitAlignItems'] = this.props.alignItems;
      style['WebkitBoxAlign'] = this.props.alignItems;
      style.alignItems = this.props.alignItems;
    }

    if(this.props.alignContent) {
      style['WebkitAlignContent'] = this.props.alignContent;
      style['WebkitBoxPack'] = this.props.alignContent;
      style.alignContent = this.props.alignContent;
    }

    if(this.props.justifyContent) {
      style['WebkitJustifyContent'] = this.props.justifyContent;
      style.justifyContent = this.props.justifyContent;
    }

    if(this.props.style) {
      _.extend(style, this.props.style);
    }

    return <div className={className} style={style} onClick={this.props.onClick || function() {} }> {this.props.children} </div>
  }
});

// can use flex: size as a component instead of adding a style tag to a div
export const Flex = React.createClass({
  render() {
    const size = this.props.size || 1;

    const flexStyle = {
      "WebkitBoxFlex": size,      /* OLD - iOS 6-, Safari 3.1-6 */
      "MozkitBoxFlex": size,
      "MozBoxFlex": size,        /* OLD - Firefox 19- */   /* For old syntax, otherwise collapses. */
      "WebkitFlex": size,
      "MsBoxFlex": size,   
      "MsFlex": size,            /* IE 10 */
      "flex": size                  /* NEW, Spec - Opera 12.1, Firefox 20+ */
    };

    return <div style={flexStyle}> {this.props.children} </div>
  }
});

// grows to fill space based on surrounding elements
export const FlexGrow = React.createClass({
  render() {

    const flexGrowStyle = {
      flexGrow : this.props.weight || 1,
      WebkitFlexGrow : this.props.weight || 1
    };

    return <div style={ flexGrowStyle }> {this.props.children} </div>
  }
});

// shrinks to allow space based on surrounding elements
export const FlexShrink = React.createClass({
  render() {

    const flexShrinkStyle = {
      flexShrink : this.props.weight || 0,
      WebkitFlexShrink : this.props.weight || 0
    };

    return <div style={ flexShrinkStyle }> {this.props.children} </div>
  }
});