import { Styles } from 'material-ui';
import { ThemeManager, LightRawTheme } from 'material-ui/lib/styles';

export default AppConfig = {
  // Color palette is also defined in _partial.scss
  palette:  {
    //primary:  Styles.Colors.lightBlue800,  // #0277BD
    //primary2: Styles.Colors.lightBlue700,  // #0288D1
    //primary3: Styles.Colors.lightBlue900,  // #01579B
    //accent:   Styles.Colors.cyan400,       // #26C6DA
    //accent2:  Styles.Colors.red700,        // #D32F2F
    //accent3:  Styles.Colors.cyan900,       // #006064
    cyan:       "#0075A0",                   // Dark Cyan
    darkCyan:   "#1F2839",                   // Very Dark Cyan
    darkerCyan: "#1F2839",                   // Even Darker Cyan
    red:        "#F90C13",                   // Red
    lightCyan:  "#67C9D0",                   // Cyan
    textHint:   "rgba(0, 0, 0, 0.87)",
    white:      Styles.Colors.white,         // #FFFFFF
    grey100:    Styles.Colors.grey100,       //
    grey200:    Styles.Colors.grey200,       // #EEEEEE
    grey300:    Styles.Colors.grey300,       // #E0E0E0
    grey400:    Styles.Colors.grey400,       // #BDBDBD
    grey500:    Styles.Colors.grey500,       // #9E9E9E
    grey600:    Styles.Colors.grey600,       // #757575
    grey700:    Styles.Colors.grey700,       // #616161
    grey800:    Styles.Colors.grey800,       // #424242
    grey900:    Styles.Colors.grey900        // #212121
  }
};
