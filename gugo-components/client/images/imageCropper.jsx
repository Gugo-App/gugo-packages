import React from 'react';
import { Dialog, FlatButton } from 'material-ui';
//import  cropper from 'cropper';


export default class ImageCropper extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open : false,
      sourceImage : null
    };

    this.close = this.close.bind(this);
    this.save = this.save.bind(this);

    this.cropper = require('react-cropper').default;
  }


  open(sourceImage, cropCB) {
    var updt = { open : true };

    //Dont open the modal if something went wrong
    if(sourceImage) {
      updt.sourceImage = sourceImage;
      this.cropCB = cropCB;
    } else {
      return;
    }

    this.setState(updt);
    //this.initCropper();
  }

  _crop() {
    var self = this;

    const canvas = this.refs.cropper.getCroppedCanvas();

    this.dataUrl = canvas.toDataURL();

    canvas.toBlob( blob => {
      self.blob = blob;
    });

    // console.log("Cropped", this.dataUrl);
  }

  save()  {
    console.log("Save");
    this.setState({ open : false });
    this.fireCallback({ url : this.dataUrl, blob : this.blob });
  }

  close() {
    this.setState({open: false});
    this.fireCallback();
  }

  fireCallback(url) {
    this.cropCB(url);
    this.cropCB = null;
  }

  render() {

    const actions = [
      <FlatButton
        label="Cancel"
        primary={false}
        keyboardFocused={false}
        onTouchTap={this.close}
      />,

      <FlatButton
        label="Save"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.save}
      />
    ];

    var cropper = null;
    if(this.state.sourceImage) {
      cropper =  <this.cropper
        ref='cropper'
        src={this.state.sourceImage}
        style={{height: 270, width: 480}}
        // Cropper.js options
        aspectRatio={16 / 9}
        guides={false}
        crop={this._crop.bind(this)}
      />
    }

    return (
      <Dialog
        title="Crop Before Upload"
        actions={actions}
        modal={true}
        open={this.state.open}
        onRequestClose={this.close}
      >
        {cropper}
      </Dialog>
    )
  }
}