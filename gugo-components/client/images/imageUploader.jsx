import React from 'react';
import { TextField, RaisedButton, CircularProgress, Snackbar, Toggle } from 'material-ui';
import { OverlayLoader } from 'meteor/gugo:components/client/loaders/overlayRingLoader';
import ImageCropper from './imageCropper';

let imageContainer = {
  height:          "125px",
  width:           "125px",
  backgroundColor: "gray",
  display:         "inline-block",
  float:           "left"
};

export const ImageUploaderRedux = React.createClass({
  getInitialState() {
    return {
      id : null,
      type : this.props.type,
      hasChanged : false,
      heroUri : this.props.imgUrl
    }
  },
  getImageIdFromUrl(imgUrl) {
    if(imgUrl) {
      var splitArr = imgUrl.split('/');
      //console.log("Split arr", splitArr);
      //Terrible way to do this, find out how to test for mongo IDS
      if(splitArr.length === 7) {
        return splitArr[5];
      } else {
        return (new Meteor.Collection.ObjectID())._str;
      }
    } else {
      return (new Meteor.Collection.ObjectID())._str;
    }

  },
  componentWillMount() {
    var state = this.state;

    state.id      = this.getImageIdFromUrl(this.props.imgUrl);
    state.containerId    = "div-" + state.id;
    state.fileInputId    = "file-input-" + state.id;
    state.uploadButtonId = "upload-file-button-" + state.id;
    state.imageId        = "image-" + state.id;

    //console.log("Image Uploader Redux State", state);

    this.setState(state);

    this.setSlingshotDirective();
  },
  setSlingshotDirective() {
    let options = { type: this.props.type, id: this.state.id, size: this.props.size };
    //console.log("setSlingshotDirective", options);
    this.slingshot = new Slingshot.Upload(this.props.size + "ImageUpload", options);
  },
  getBackgroundImage() {
    return "url('" + this.state.heroUri + "')";
  },
  getSanitizedFileName(text) {
    return text.replace(/ /g, "");
  },
  //File getting functions
  getFile() {
    var input = document.getElementById(this.state.fileInputId),
        file  = input && input.files && input.files[0] ? input.files[0] : null,
        err;

    if (!file) {
      alert('Please choose a file to upload');
      return;
    }

    err = this.slingshot.validate(file);
    if (err) {
      console.error(pfx + 'validation error', { error: err });
      alert(err);
      return null;
    }

    return file;
  },
  getBlobFromURL() {

  },
  onSelectFile(e) {
    e.preventDefault();
    e.stopPropagation();
    $("#" + this.state.fileInputId).click();

    console.log("ON select file input", this.state.fileInputId);
  },
  onFileChange() {
    var ctx  = this,
        file = this.getFile(),
        reader;

    console.log("FIle changed", file);

    if (!file) {
      $("#" + ctx.state.containerId + " .image-selected").hide();
      $("#" + ctx.state.containerId + " #upload-progress").hide();

    } else {

      $("#" + ctx.state.containerId + " .image-selected").show();
      $("#" + ctx.state.containerId + " #image-label").val(ctx.getSanitizedFileName(file.name));

      reader        = new FileReader();
      reader.onload = function (e) {
        var url           = e.target.result;

        ctx.refs['image-cropper'].open(url, function(output) {
          // console.log("Callback fired", output);
          if(output) {
            url = output.url;
          }

          ctx.state.heroUri = url;
          ctx.setState(ctx.state);
          $("#imageContainer").show();
        })
      };
      reader.readAsDataURL(file);

      var state = this.state;
      state.hasChanged = true;
      this.setState(state);
    }
  },
  uploadWithPromise() {
    //console.log("UPLOADING");
    var ctx  = this,
        pfx  = '[ImageUploader] onUploadFileTap ',
        file = this.getFile(),
        blob = this.refs['image-cropper'].blob,
        res;

    if(blob) {
      blob.name = this.getSanitizedFileName(file.name);
      file = blob;
    }

    if (!file) {
      return;
    }

    ctx.setSlingshotDirective();

    OverlayLoader.show("Uploading Image");

    var p = new Promise((resolve, reject) => {
      ctx.slingshot.send(file, function (err, downloadUrl) {
        OverlayLoader.hide();

        if (err) {
          // Log service detailed response
          res = ctx.slingshot.xhr ? ctx.slingshot.xhr.response : null;
          console.error(pfx + 'upload error', { error: err, response: res });
          alert(err);

          reject(res);
        } else {

          // pass the URL to the onUpload event of the parent component
          //ctx.props.onUpload && ctx.props.onUpload(downloadUrl);
          resolve(downloadUrl);
        }
      });
    });

    return p;
  },
  //End file getting functions
  render() {
    var imgContainerStyle = {
          display: "",
          border : "1px solid black",
          backgroundColor : "lightgray",
          height:  270,
          maxWidth:   480
        },
        imgStyle          = {
          height:           "100%",
          width:            "100%",
          backgroundRepeat: "no-repeat"
        };
    var icon = null,
        imgContainer = null;


    if (this.state.heroUri) {
      //imgStyle.backgroundImage = this.getBackgroundImage();
    } else {
      icon = <i className="fa fa-file-image-o" style={{ color : "black", margin : "110px 220px", fontSize : "30px" }}> </i>
    }

    return <div id={this.state.containerId}>
      <form id="event-tile-image-upload-form">
        <div className="ui error message">
          <div className="header"></div>
        </div>

        <div style={imgContainerStyle}  onClick={this.onSelectFile}>
          <img id="imageContainer" src={this.state.heroUri} height="270" width="100%" onClick={this.onSelectFile} >
          </img>
        </div>

        <div className="image-selected" style={{marginLeft:10,display:"none"}}>
          <TextField id="image-label" type="text"/>
        </div>

        <div id="upload-progress" style={{display:"none"}}>
          <CircularProgress mode="indeterminate"/>
        </div>

        {
            //<div className="ui active progress" data-percent={progress}>
            //  <div className="bar"
            //       style={{transitionDuration: "300ms", WebKitTransitionDuration: "300ms", width: {progress}%}}>
            //    <div className="progress">{{ progress }}%</div>
            //  </div>
            //</div>
            }

        <input id={this.state.fileInputId}
               type="file"
               style={{display:"none"}}
               onChange={this.onFileChange}/>
      </form>

      <ImageCropper ref="image-cropper"/>
    </div>
  }
});