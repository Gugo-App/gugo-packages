// the javascript behind the typeahead input field
import React from 'react';
import ReactDOM from 'react-dom';


export default TypeaheadInput = React.createClass({
  onSelected(item) {
    // console.log("Selected item", item);
    Meteor.setTimeout(function () {
      $(".typeahead.box-input").val("");
    }, 0);

    this.props.onSelected(item);
  },
  componentDidMount() {
    console.log("METEOR MOUNTING TYPEAHJEAD", Blaze);

    let data = {
      id:           this.props.id,
      dataMap:      this.props.dataMap,
      placeholder:  this.props.placeholder,
      fontSize:     this.props.fontSize,
      dataTemplate: this.props.dataTemplate,
      selected:     this.onSelected
    };

    this.view = Blaze.renderWithData(Template[this.props.template],
      data, ReactDOM.findDOMNode(this.refs.container));
  },
  render() {
    return <div style={this.props.style} ref="container"></div>
  },
  componentWillUnmount() {
    Blaze.remove(this.view)
  }
});

//region Contacts Typeahead Templates
Template.contactsTypeaheadInject.onRendered(function () {
  console.log("METEOR INJECTING");
  console.log("METEOR", Meteor, "TYPE", Meteor.typeahead);
  Meteor.typeahead.inject();
});

Template.contactsTypeahead.onRendered(function () {
  //console.log("Rendering with data", Template.currentData());
});

Template.contactsTypeaheadInject.helpers({
  dataSource: function () {
    var tData = Template.currentData();
    // console.log("Helper got data source", tData);
    if (tData.dataMap) {
      return tData.dataMap;
    } else {
      console.log("Did not get T Data");
    }
  },
  selected:   function (event, suggestion, datasetName) {
    var data = Template.currentData();

    // event - the jQuery event object
    // suggestion - the suggestion object
    // datasetName - the name of the dataset the suggestion belongs to

    // console.log(suggestion.id);
    console.log(suggestion);

    data.selected(suggestion);
  }
});
//endregion

//region Generic Typeahead Templates
Template.typeaheadInject.onRendered(function () {
  Meteor.typeahead.inject();
});

Template.typeaheadInject.helpers({
  sourceData:   function () {
    var data = Template.currentData();
    return data.dataMap || null;
  },
  id:           function () {
    var data = Template.currentData();
    return data.id || null;
  },
  dataTemplate: function () {
    var data = Template.currentData();
    return data.dataTemplate || null;
  },
  placeholder:  function () {
    var data = Template.currentData();
    return data.placeholder || null;
  },
  fontSize:     function () {
    var data = Template.currentData();
    return data.fontSize || 12;
  },
  selected:     function (event, selected) {
    var data = Template.currentData();
    data.selected(selected);
  }
});
//endregion
