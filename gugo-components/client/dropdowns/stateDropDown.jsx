import React from 'react';
import { DropDownMenu, MenuItem } from 'material-ui';
const items = [];

export default StateDropDown = React.createClass({
  getInitialState() {
    return {
      value: ""
    }
  },
  getDefaultProps() {
    return {
      default: "Select a State"
    }
  },
  onChange(e, idx, value) {
    var state   = this.state;
    state.value = value;
    this.setState(state);
    this.props.onChange && this.props.onChange(value);
  },
  componentWillMount() {
    var state   = this.state;
    state.value = this.props.value || state.value;
    this.setState(state);

    items.push(<MenuItem value="" key="" primaryText={this.props.default}/>);
    _.each(Gugo.data.states, function (state) {
      items.push(<MenuItem value={state} key={state} primaryText={state}/>);
    });
  },
  render() {
    return (
      <DropDownMenu value={this.state.value}
                    onChange={this.onChange}
                    className="custom-dropdown"
                    style={{height:45}}>
        {items}
      </DropDownMenu>
    );
  }
});
