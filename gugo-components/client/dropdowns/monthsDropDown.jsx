import React from 'react';
import { DropDownMenu, MenuItem } from 'material-ui';
const items = [];

export default MonthsDropDown = React.createClass({
  getDefaultProps() {
    return {
      value:       "",
      defaultText: "Select a Month",
      months:      [  { id: 1,
                        name: 'January'},
                        { id: 2,
                        name: 'February'},
                        { id: 3,
                        name: 'March'},
                        { id: 4,
                        name: 'April'},
                        { id: 5,
                        name: 'May'},
                        { id: 6,
                        name: 'June'},
                        { id: 7,
                        name: 'July'},
                        { id: 8,
                        name: 'August'},
                        { id: 9,
                        name: 'September'},
                        { id: 10,
                        name: 'October'},
                        { id: 11,
                        name: 'November'},
                        { id: 12,
                        name: 'December'}],
      style:       {}
    }
  },
  render() {

    //console.log("VenuesDropDown.render", { venueId: this.props.value, venueCount: this.data.venues.length });

    return (
      <MonthsDropDownWithData value={this.props.value}
                                              months={this.props.months}
                                              onChange={this.props.onChange}
                                              defaultText={this.props.defaultText}
                                              style={this.props.style}/>
    );
  }
});

export const MonthsDropDownWithData = React.createClass({
  getInitialState() {
    return {
      value: ""
    }
  },
  getDefaultProps() {
    return {
      defaultText: null,
      months:      [  { id: 1,
                        name: 'January'},
                        { id: 2,
                        name: 'February'},
                        { id: 3,
                        name: 'March'},
                        { id: 4,
                        name: 'April'},
                        { id: 5,
                        name: 'May'},
                        { id: 6,
                        name: 'June'},
                        { id: 7,
                        name: 'July'},
                        { id: 8,
                        name: 'August'},
                        { id: 9,
                        name: 'September'},
                        { id: 10,
                        name: 'October'},
                        { id: 11,
                        name: 'November'},
                        { id: 12,
                        name: 'December'}],
      style:       {}
    }
  },
  onChange(e, idx, value) {
    //console.log("Got venue ID", { value: value });
    var state   = this.state;
    state.value = value;
    this.setState(state);
    this.props.onChange && this.props.onChange(value);
  },
  componentWillMount() {
    var state   = this.state;
    state.value = this.props.value || state.value;
    this.setState(state);

    items.push(<MenuItem value="" key="" primaryText={this.props.defaultText}/>);
    _.each(this.props.months, function (month) {
      items.push(<MenuItem value={month.id} key={month.id} primaryText={month.name}/>);
    });
  },
  render() {
    var style = _.extend({ height: 45 }, this.props.style);

    //console.log("state.value:", { "state.value": this.state.value });

    return (
      <DropDownMenu value={this.state.value}
                    onChange={this.onChange}
                    className="custom-dropdown"
                    style={style}>
        {items}
      </DropDownMenu>
    );
  }
});
