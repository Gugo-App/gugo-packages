import React from 'react';
import { DropDownMenu, MenuItem } from 'material-ui';

const items = [];

export default CategoryDropDown = React.createClass({
  mixins: [ReactMeteorData],
  getMeteorData() {
    var categoriesHandle = SubsMgr.subscribe("allCategories");
    return {
      isLoading:  !categoriesHandle.ready(),
      categories: Gugo.collections.categories.find({}, { sort: { name: 1 } }).fetch()
    }
  },
  getDefaultProps() {
    return {
      value:       "",
      defaultText: "Select a Category",
      categories:  null,
      style:       {}
    }
  },
  render() {
    if (this.data.isLoading) {
      return <div></div>;
    }

    console.log("CategoryDropDown.render", {
      categoryId:    this.props.value,
      categoryCount: this.data.categories.length
    });

    return (
      <Gugo.components.CategoryDropDownWithData value={this.props.value}
                                                categories={this.data.categories}
                                                onChange={this.props.onChange}
                                                defaultText={this.props.defaultText}
                                                style={this.props.style}/>
    );
  }
});

Gugo.components.CategoryDropDownWithData = React.createClass({
  getInitialState() {
    return {
      value: ""
    }
  },
  getDefaultProps() {
    return {
      defaultText: null,
      categories:  null,
      style:       {}
    }
  },
  onStateChange(e, idx, value) {
    console.log("Got category ID", { value: value });
    var state   = this.state;
    state.value = value;
    this.setState(state);
    this.props.onChange && this.props.onChange(value);
  },
  componentWillMount() {
    var state   = this.state;
    state.value = this.props.value || state.value;
    this.setState(state);

    items.push(<MenuItem value="" key="" primaryText={this.props.defaultText}/>);
    _.each(this.props.categories, function (category) {
      items.push(<MenuItem value={category._id} key={category._id} primaryText={category.name}/>);
    });
  },
  render() {
    var style = _.extend({ height: 45 }, this.props.style);

    console.log("state.value:", { "state.value": this.state.value });

    return (
      <DropDownMenu value={this.state.value}
                    onChange={this.onStateChange}
                    className="custom-dropdown"
                    style={style}>
        {items}
      </DropDownMenu>
    );
  }
});
