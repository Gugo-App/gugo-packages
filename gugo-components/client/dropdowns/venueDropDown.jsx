import React from 'react';
import { DropDownMenu, MenuItem } from 'material-ui';
const items = [];

export default VenuesDropDown = React.createClass({
  mixins: [ReactMeteorData],
  getMeteorData() {
    var venuesHandle = SubsMgr.subscribe("allVenues");
    return {
      isLoading: !venuesHandle.ready(),
      venues:    Gugo.collections.venues.find({}, { sort: { name: 1 } }).fetch()
    }
  },
  getDefaultProps() {
    return {
      value:       "",
      defaultText: "Select a Venue",
      venues:      null,
      style:       {}
    }
  },
  render() {
    if (this.data.isLoading) {
      return <div></div>;
    }

    //console.log("VenuesDropDown.render", { venueId: this.props.value, venueCount: this.data.venues.length });

    return (
      <VenuesDropDownWithData value={this.props.value}
                                              venues={this.data.venues}
                                              onChange={this.props.onChange}
                                              defaultText={this.props.defaultText}
                                              style={this.props.style}/>
    );
  }
});

VenuesDropDownWithData = React.createClass({
  getInitialState() {
    return {
      value: ""
    }
  },
  getDefaultProps() {
    return {
      defaultText: null,
      venues:      null,
      style:       {}
    }
  },
  onChange(e, idx, value) {
    //console.log("Got venue ID", { value: value });
    var state   = this.state;
    state.value = value;
    this.setState(state);
    this.props.onChange && this.props.onChange(value);
  },
  componentWillMount() {
    var state   = this.state;
    state.value = this.props.value || state.value;
    this.setState(state);

    items.push(<MenuItem value="" key="" primaryText={this.props.defaultText}/>);
    _.each(this.props.venues, function (venue) {
      items.push(<MenuItem value={venue._id} key={venue._id} primaryText={venue.name}/>);
    });
  },
  render() {
    var style = _.extend({ height: 45 }, this.props.style);

    //console.log("state.value:", { "state.value": this.state.value });

    return (
      <DropDownMenu value={this.state.value}
                    onChange={this.onChange}
                    className="custom-dropdown"
                    style={style}>
        {items}
      </DropDownMenu>
    );
  }
});
