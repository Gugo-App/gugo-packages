Package.describe({
  name: 'gugo:contacts',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Cordova.depends({
  'cordova-plugin-contacts' : '1.1.0'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use([
    'ecmascript',
    'underscore',
    'accounts-base',
    'gugo:core',
    'aldeed:collection2@2.7.0'
  ]);

  api.addFiles([
    'lib/shared/schema.js',
    'lib/shared/collection.js',
    'lib/shared/collection-helpers.js',
    'lib/shared/methods.js'
  ], ['client', 'server'], { transpile : false });

  api.addFiles('lib/server/publication.js', 'server', { transpile : false });

  api.addFiles('lib/client/contacts.js', 'client', { transpile : false });

  api.export('Contacts', 'client');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('gugo:contacts');
  api.addFiles('contacts-tests.js');
});
