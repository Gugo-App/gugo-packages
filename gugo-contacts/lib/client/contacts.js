Contacts = {};

Contacts.storageLocation = 'gugo-contacts-loaded';

function getPhoneNumber (contact) {
  var phones = contact.phoneNumbers;

  if(phones.length === 1) {
    return phones[0];
  }

  for (var i = 0; i < phones.length; i++) {
    if (phones[i].type === "mobile") {
      return phones[i];
    }
  }
}

function isInternational(phone) {
  return (phone.length === 11 && phone.charAt(0) === "1");
}

function transformInternational(phone) {
  return phone.substr(1);
}

function getName (contact) {
  var name = null;
  if (contact) {
    // iOS uses name.formatted. Android usees displayName.
    name = (contact.name && contact.name.formatted) ? contact.name.formatted : (contact.displayName || contact.nickname);
  }
  return name;
}

//Rules for contacts
//1. Has to have a "MOBILE" Phone number or only ONE number
//2. Phone must be 7 characters OR 8 characters with a 1 at the beginning (we filter out the 1) -- TODO
//3. Contact must have a name
Contacts.get = function (filter, callback, force) {
  var self = this;

  var options            = new ContactFindOptions();
  options.filter         = filter || "";
  options.multiple       = true;
  options.hasPhoneNumber = true;

  console.log("Getting contacts....", options.filter);

  var fields = ["displayName", "photos", "phoneNumbers", "nickname"];

  function onSuccess (contacts) {
    var i          = 0;
    var cList      = {};
    var asyncTasks = [];

    console.log("Success!", contacts.length);

    _.each(contacts, function (contact) {
      asyncTasks.push(function (callback) {
        Meteor.setTimeout(function () {
          var name = getName(contact);
          if (name && contact.phoneNumbers) {
            var phoneObj = getPhoneNumber(contact);
            //console.log("Phone object", phoneObj);

            if (!phoneObj) {
              callback();
              return;
            }

            var cleanedPhone = Phoneformat.cleanPhone(phoneObj.value).replace(/\D/g, "");

            //Remove the leading 1 from phone numbers that have it
            if(isInternational(cleanedPhone)) {
              cleanedPhone = transformInternational(cleanedPhone);
            }

            //Don't allow the addition of any phones that aren't 10 digits
            if(cleanedPhone.length !== 10) {
              callback();
              return;
            }

            var mContact = {
              id:        contact.id,
              phone:     cleanedPhone,
              phoneType: phoneObj.type,
              name:      name
            };

            if (typeof cList[cleanedPhone] === "undefined") {
              var exist = Gugo.collections.contacts.findOne({ 'phone' : mContact.phone });
              var doesEqual = exist && Contacts.equals(mContact, exist);

              console.log("Does exist?", exist, "Does equal", doesEqual);

              if(!doesEqual) {
                cList[cleanedPhone] = mContact;
              }

              //console.log("Contact", mContact);
            } else {
              //console.log("Skipping duplicate", mContact);
            }
          }
          i++;
          callback();
        }, 0);
      });
    });

    async.series(asyncTasks, function () {
      console.log("Contacts Retrieved", i);
      var contactTasks = [];

        console.log("Updating list", cList);

        _.each(cList, function (contact) {
          contactTasks.push(function (cb) {
            Meteor.setTimeout(function () {
              Meteor.call('addContact', contact, Meteor.userId());
              cb();
            }, 0);
          });

      });

      async.parallel(contactTasks, function(err, res) {
        console.log("Done importing Contacts");
        Contacts.verifyContactsIntegrity();
        callback && callback();
      })
    });
  }

  function onError (err) {
    console.log("Error!", err);
  }

  Tracker.autorun(function(comp) {
    var handle = SubsMgr.subscribe('myContacts');
    if(handle.ready()) {
      if(force || (!force && self.hasLoadedContacts())) {
        self.setContactsLoaded(true);
        navigator.contacts.find(fields, onSuccess, onError, options);
      }
      comp.stop();
    }

  });

};

//This is a function to verify / update contacts that have not updated correctly
//For instance, if a user in their contacts has signed up and hasn't updated their respective contact
Contacts.verifyContactsIntegrity = function() {
  //Check contacts without a userId
  var contactsWithoutUsers = Gugo.collections.contacts.find({ $or : [{ userId : { $exists : false } }, { userId : null }] }).fetch();
  var phones = contactsWithoutUsers.map( function(contact) {
    return contact.phone;
  });

  phones = _.without(phones, undefined);

  console.log("Phones", phones, phones.length);

  Meteor.call('updateUsersInPhones', phones);

};

Contacts.equals = function(a, b) {
  console.log("Does equal?", a, b);
  return (a.phone === b.phone) && (a.phoneType === b.phoneType) && (a.name === b.name) && (Meteor.userId() === b.ownerId);
};

Contacts.hasLoadedContacts = function() {
  var haveLoaded = localStorage.getItem(this.storageLocation);

  return _.isString(haveLoaded) && haveLoaded === "true";
};

Contacts.setContactsLoaded = function(bool) {
  localStorage.setItem(this.storageLocation, bool);
};

// Contacts.handleStartup = function() {
//   var haveLoaded = localStorage.getItem(this.storageLocation);
//
//   if(_.isString(haveLoaded)) {
//
//   }
// }