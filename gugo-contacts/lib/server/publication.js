Meteor.publish('myContacts', function() {
  this.unblock();

  console.log("LOADING CONTACTS", this.userId);
  if(this.userId) {
    //console.log("Got contacts", Gugo.collections.contacts.find({ ownerId : this.userId }).fetch());
    return Gugo.collections.contacts.find({ ownerId : this.userId });
  } else {
    this.ready();
  }
});

Meteor.publish('myContactUsers', function(friends) {
  this.unblock();

  if(this.userId && friends) {
    //console.log("Finding contact users", Meteor.users.find({ '_id' : { $in : friends } }).fetch())
    return Meteor.users.find({ '_id' : { $in : friends } }, { fields : Gugo.helpers.users.fieldRestrictions()  });
  } else {
    return this.ready();
  }
});