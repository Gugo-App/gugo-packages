Meteor.methods({
  addContact : function(contact, mUserId) {
    this.unblock();

    var existing = Gugo.collections.contacts.findOne({ id: contact.id, phone : contact.phone, ownerId: mUserId}),
      mUser = null,
      existingUser = Meteor.users.findOne({ phone : contact.phone });

    if(mUserId) {
      mUser = Meteor.users.findOne(mUserId);
    }

    function pushContactToFriends() {
      if (mUser) {
        if (!mUser.friends) {
          Meteor.users.update(mUserId, {$set: {'profile.friends': [contact.userId]}});
        } else {
          Meteor.users.update(mUserId, {$push: {'profile.friends': contact.userId}});
        }
      }
    }

    if(existingUser) {
      contact.userId = existingUser._id;
    }

    if(existing) {
      if(!existing.userId && contact.userId) {
        pushContactToFriends();
      }

      _.extend(existing, contact);

      Gugo.collections.contacts.update(contact.id, { $set : existing });
    } else {

      contact.ownerId = mUserId;

      var id = Gugo.collections.contacts.insert(contact);


      if (contact.userId) {
        pushContactToFriends();
      }
    }
  },
  checkForOutstandingContacts : function(phoneNumber, userId) {
    this.unblock();

    var contacts = Gugo.collections.contacts.find({ phone : phoneNumber}).fetch();

    if(contacts) {
      _.each(contacts, function(contact) {
        Gugo.collections.contacts.update(contact._id, {$set : { userId : userId } });
        const user = Meteor.users.findOne(contact.ownerId);

        if(user) {
          const isAlreadyFriend = user && user.profile && user.profile.friends && _.contains(user.profile.friends, userId);


          if (!isAlreadyFriend) {
            Meteor.users.update(contact.ownerId, {$push: {'profile.friends': userId}});
          }
        }
      })
    }
  },
  updateUsersInPhones : function(phones) {
    this.unblock();

    const users = Meteor.users.find({ phone : { $in : phones } }).fetch();
    const myUserId = this.userId;

    _.each(users, function(user) {
      const userId = user._id;
      const contact = Gugo.collections.contacts.findOne({ phone : user.phone, ownerId : myUserId });

      if(contact) {
        Gugo.collections.contacts.update(contact._id, {$set: {userId: userId}});
        Meteor.users.update(contact.ownerId, {$push: {'profile.friends': userId}});
      }
    })
  },
  getDBContactUser : function(contactId) {
    return Meteor.users.findOne(contactId);
  }
});