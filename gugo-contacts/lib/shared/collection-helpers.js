Gugo.collections.contacts.helpers({
  getAvatar : function() {
    var img = "/images/avatar_default.png";
    if(this.userId) {
      var user = Meteor.users.findOne(this.userId);
      if(user && user.profile && user.profile.avatar) {
        return user.profile.avatar;
      } else {
        return img;
      }
    } else {
       return img;
    }
  }
});

Meteor.users.helpers({
  getAvatar: function() {
    var img = "/images/avatar_default.png";
    if(this._id) {
      var user = this;
      if(user && user.profile && user.profile.avatar) {
        return user.profile.avatar;
      } else {
        return img;
      }
    } else {
      return img;
    }
  }
});

Gugo.collections.contacts.allow({
  update : function() {
    return true;
  }
})