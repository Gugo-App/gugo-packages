Gugo.collections.contacts = new Mongo.Collection('contacts');

if(Meteor.isServer) {
  Gugo.collections.contacts._ensureIndex({ ownerId : 1 })
  Gugo.collections.contacts._ensureIndex({ phone : 1 })
}