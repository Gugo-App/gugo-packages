Package.describe({
  name: 'gugo:events-invitations',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use([
    'ecmascript',
    'meteor-platform',
    'underscore',
    'gugo:core',
    'aldeed:collection2@2.7.0',
    'reywood:publish-composite@1.4.2'
  ]);

  api.addFiles(['lib/shared/collection.js', 'lib/shared/schema.js', 'lib/shared/methods.js', 'lib/shared/helpers.js'], ['client', 'server'], { transpile : false });
  api.addFiles(['lib/server/publications.js', 'lib/server/methods.js'], 'server', { transpile : false });
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.addFiles('events-invitations-tests.js');
});
