Meteor.methods({
  batchInvite : function(invitee, invitedContacts, eventId) {
    _.each(invitedContacts, function(contact) {
      Meteor.call('inviteContact', invitee, contact, eventId);
    });
  },
  inviteContact : function(invitee, invited, eventId) {
    if(_.isString(invited)) {
      invited = Gugo.collections.contacts.findOne(invited);
    }

    var blackListed = Gugo.collections.userBlacklist.findOne({ blocked : invitee.phone, blocker: invited.phone });
    if(blackListed) {
      console.log("USER WAS BLACKLISTED");
      return;
    }

    console.log("Invitee", invitee, "invited", invited, "eventId", eventId);

    var existingInvitation = Gugo.collections.eventInvitations.findOne({ eventId : eventId, 'invitee.userId' : invitee.userId, 'invited.phone' : invited.phone });

    if(!existingInvitation) {
      var id = Gugo.collections.eventInvitations.insert({
        invitee: invitee,
        invited: {
          phone: invited.phone,
          name: invited.name,
          contactId: invited._id,
          userId: invited.userId || null
        },
        eventId: eventId
      }, function (err, docs) {
        console.log("Error", err, "docs", docs)
      });

      if (!id) throw new Meteor.Error("mongo-error", "Error Sending Invite to" + invited.name);


      var search = [{phone: invited.phone}];
      if (invited.userId) search.push({_id: invited.userId});

      var contactUser = Meteor.users.findOne({$or: search});

      //If the invited party doesnt have a userId then we need to text them
      if (!contactUser) {
        console.log("Sending invitation, not part of system");
        Meteor.call('sendInvitationSMS', invitee, invited, eventId);
      } else {
        Meteor.call('sendPushInvite', invitee, contactUser, eventId);
        console.log("User Exists, Push notify!");
      }
    }
  },
  inviteNetworks: function(invitee, networks, eventId) {
    _.each(networks, function(network) {
      Meteor.call('inviteNetwork', invitee, network, eventId);
    })
  },
  inviteNetwork: function(invitee, networkId, eventId) {
    var network = null;
    console.log("NETWORK ID", networkId);
    if(_.isString(networkId)) {
      network = Gugo.collections.networks.findOne(networkId);
    }

    if(!network) throw new Meteor.Error('Could not find network');

    console.log("Invitee", invitee, "Network", network, "eventId", eventId);

    Gugo.collections.eventNetworkInvitations.insert({
      ownerId : invitee.userId,
      invitee : invitee,
      networkId : networkId,
      eventId : eventId
    });

    _.each(network.members, function(contactId) {
      var contact = Gugo.collections.contacts.findOne({ _id : contactId, ownerId : invitee.userId} );
      var contactUser = Meteor.users.findOne({ phone : contact.phone });

      var blackListed = Gugo.collections.userBlacklist.findOne({ blocked : invitee.phone, blocker: contact.phone });
      if(!blackListed) {
      //If the invited party doesnt have a userId then we need to text them
        if(!contactUser) {
          console.log("Sending invitation, not part of system", contact);
          Meteor.call('sendInvitationSMS', invitee, contact, eventId);
        } else {
          Meteor.call('sendPushInvite', invitee, contactUser, eventId);
          console.log("User Exists, Push notify!");
        }
      }
    });
  },
  sendPushInvite: function(invitee, invited, eventId) {
    var event = Gugo.collections.userEvents.findOne(eventId) || Gugo.collections.events.findOne(eventId);
    try {
      Push.send({
        from: invited.profile.name,
        title: 'New Invitation!',
        text: 'You have been invited to ' + event.title,
        badge: 1, //optional, use it to set badge count of the receiver when the app is in background.
        query: {
          userId: invited._id
        },
        payload: {
          eventId: eventId
        }
      });
    } catch(e) {
      console.log("Push Error", e);
    }
  },
  //This function remvoves all invitations received from invitee from the DB
  //Invited is an object that can either be a phone number, a userId or both
  removeInvitationsForUser : function(inviteeUserId, invited) {
    var invitedUserId = invited.userId;
    var invitedPhoneNumber = invited.phone;

    var qwry = [];

    if(invitedUserId) {
      qwry.push({ 'invited.userId' : invitedUserId });
    }

    if(invitedPhoneNumber) {
      qwry.push({ 'invited.phone' : invitedPhoneNumber });
    }

    function mongoCallback(err, docs) {
      if(err) {
        console.log("MONGO ERROR", err);
        return;
      }

      console.log("Removed ", docs, "from invitations")
    }

    Gugo.collections.eventInvitations.remove({ 'invitee.userId' : inviteeUserId, $or : qwry },
      Gugo.handlers.mongo.bind(this, { message : "Removing Invites", callback: mongoCallback })
    )
  },
  pushTest : function() {
    console.log("Time to push");
    var self = this;
    Meteor.setTimeout(function() {
    if(!self.userId) return;
      console.log("Pushing test device");
      try {
        Push.send({
          from: "GUGO",
          title: 'New Invitation!',
          text: 'You have been invited',
          badge: 1, //optional, use it to set badge count of the receiver when the app is in background.
          query: {
            userId: this.userId
          }
        });
      } catch(e) {
        console.log("Push Error", e);
      }
    }, 3000);
  }
});