Meteor.publishComposite('myInvitedEvents', function(){
  this.unblock();
  if(!this.userId) return this.ready();

  var user = Meteor.users.findOne(this.userId);

  //console.log("User...", user);

  if(!user) return this.ready();

  return {
    find : function() {
      //console.log("Event Invitations query", user.phone);
      //console.log(Gugo.collections.eventInvitations.find({ 'invited.phone' : user.phone }).fetch());
      return Gugo.collections.eventInvitations.find({ 'invited.phone' : user.phone });
    },
    children : [
      {
        find : function(eventInvite) {
          //console.log("Got event invite", eventInvite);
          return Gugo.collections.userEvents.find({ _id : eventInvite.eventId } , { limit : 1 });
        }
      },
      {
        find : function(eventInvite) {
          //console.log("Got event invite", eventInvite);
          return Gugo.collections.events.find({ _id : eventInvite.eventId } , { limit : 1 });
        }
      },
      {
        find : function(eventInvite) {
          return Meteor.users.find(eventInvite.invitee.userId, { limit : 1, fields : Gugo.helpers.users.fieldRestrictions() })
        }
      }
    ]
  }
});

//Advantages, networks are always up to date, no need to manage network states
//Disadvantages, needs three "joins"
//
//Meteor.publishComposite('myNetworkInvitedEvents', function() {
//  if(!this.userId) return this.ready();
//
//  var user = Meteor.users.findOne(this.userId);
//
//  if(!user) return this.ready();
//
//  return {
//    find: function() {
//      console.log("Found networks ", Gugo.collections.networks.find({ phones : user.phone  }).fetch());
//      return Gugo.collections.networks.find({ phone : user.phone  });
//    }, children : [
//      {
//        find: function (network) {
//          return Gugo.collections.eventNetworkInvitations.find(network._id);
//        },
//        children : [
//          {
//            find: function(networkInvite) {
//              return Gugo.collections.userEvents.find(networkInvite.eventId)
//            }
//          }
//        ]
//      }
//    ]
//  }
//});

Meteor.publishComposite('myNetworkInvitedEvents', function() {
  this.unblock();

  if(!this.userId) return this.ready();

  console.log("MY NETWORK INVITED EVENTS FIRED");

  var user = Meteor.users.findOne(this.userId);

  if(!user) return this.ready();

  return {
    find: function() {
      //console.log("Found contacts", Gugo.collections.contacts.find({ phone : user.phone}).fetch())
      return Gugo.collections.contacts.find({ phone : user.phone})
    }, children : [
      {
        find : function(contact) {
          //console.log("Found, networks", Gugo.collections.networks.find({ members : contact._id  }).fetch());
          return Gugo.collections.networks.find({ members : contact._id  });
        }, children : [
          {
            find : function(network) {
              //console.log("Found invite", Gugo.collections.eventNetworkInvitations.find({ networkId : network._id }).fetch());
              return Gugo.collections.eventNetworkInvitations.find({ networkId : network._id })
            }, children : [
              {
                find : function(networkInvite) {
                  //console.log("Found event",  Gugo.collections.userEvents.find(networkInvite.eventId).fetch());
                  return Gugo.collections.userEvents.find(networkInvite.eventId);
                }
              },
              {
                find : function(networkInvite) {
                  //console.log("Found event",  Gugo.collections.userEvents.find(networkInvite.eventId).fetch());
                  return Gugo.collections.events.find(networkInvite.eventId);
                }
              }
            ]
          }
        ]
      }
    ]
  }
});

Meteor.publishComposite('invitesForEvent', function(eventId) {
  this.unblock();

  if(!this.userId) return;

  //console.log("PUBLISH COMPOSITE INVITES", eventId);

  return {
    find: function() {
      return Gugo.collections.eventInvitations.find({ eventId : eventId });
    }, children : [
      {
        find : function(invite) {
          //console.log("Invite", invite, Meteor.users.findOne(invite.invited.userId));
          //console.log("Found, networks", Gugo.collections.networks.find({ members : contact._id  }).fetch());
          return Meteor.users.find(invite.invited.userId, { fields : Gugo.helpers.users.fieldRestrictions() });
        }
      },
      {
        find : function(invite) {
          console.log("Find for event", invite);
          return Gugo.collections.contacts.find({ 'phone' : invite.invited.phone });
        }
      }
    ]
  }
});

Meteor.publishComposite('networkInvitesForEvents', function(eventId) {
  this.unblock();

  if(!this.userId) return;

  //console.log("PUBLISH COMPOSITE NETWORK INVITES", eventId, Gugo.collections.eventNetworkInvitations.find({ eventId : eventId }).fetch());

  return {
    find: function() {
      return Gugo.collections.eventNetworkInvitations.find({ eventId : eventId });
    }, children : [
      {
        find: function(invite) {
          return Gugo.collections.networks.find(invite.networkId);
        },
        children: [
          {
            find : function(network) {
              //console.log("Finding contacts...", Gugo.collections.contacts.find({ '_id' : { $in : network.members } }).fetch())
              return Gugo.collections.contacts.find({ '_id' : { $in : network.members } });
            }
          }
        ]
      }
    ]
  }
});

//This returns your invitations that you have sent for a specific event
Meteor.publish('myInvitedToEvent', function(eventId) {
  this.unblock();

  if(!this.userId) return;

  return [
    Gugo.collections.eventInvitations.find({ 'invitee.userId' : this.userId, eventId : eventId }),
    Gugo.collections.eventNetworkInvitations.find({ ownerId : this.userId, eventId : eventId })
    ]
});
