Gugo.collections.eventInvitations = new Mongo.Collection('event-invitations');

Gugo.collections.eventNetworkInvitations = new Mongo.Collection('event-network-invitations');

if(Meteor.isServer) {
  //Event Invitation Indexes
  Gugo.collections.eventInvitations._ensureIndex({ 'invited.phone' : 1});
  Gugo.collections.eventInvitations._ensureIndex({ 'invited.userId' : 1});
  Gugo.collections.eventInvitations._ensureIndex({ 'invitee.userId' : 1});
  Gugo.collections.eventInvitations._ensureIndex({ 'eventId' : 1});

  //Network Invitation Indexes
  Gugo.collections.eventNetworkInvitations._ensureIndex({ 'networkId' : 1});
  Gugo.collections.eventNetworkInvitations._ensureIndex({ 'eventId' : 1});
  Gugo.collections.eventNetworkInvitations._ensureIndex({ 'ownerId' : 1});

}