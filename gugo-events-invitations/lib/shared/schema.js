Gugo.schemas.InviteSchema = new SimpleSchema({
  createdAt:  {
    type : Date,
    autoValue : function() {
      if(this.isInsert) {
        return new Date();
      }
    }
  },
  eventId : {
    type : String
  },
  'invited.userId' : {
    type : String,
    optional : true
  },
  'invited.contactId' : {
    type : String,
    optional : true
  },
  'invited.phone' : {
    type : String,
    optional : true
  },
  'invited.name' : {
    type : String,
    optional : true
  },
  'invitee.userId' : {
    type : String
  },
  'invitee.phone' : {
    type : String
  },
  'invitee.name' : {
    type : String
  }
});

InviteNetworkSchema = new SimpleSchema({
  createdAt:  {
    type : Date,
    autoValue : function() {
      if(this.isInsert) {
        return new Date();
      }
    }
  },
  ownerId : {
    type : String
  },
  'invitee.userId' : {
    type : String
  },
  'invitee.phone' : {
    type : String
  },
  'invitee.name' : {
    type : String
  },
  eventId : {
    type : String
  },
  networkId : {
    type : String
  }
});

Gugo.collections.eventInvitations.attachSchema(Gugo.schemas.InviteSchema);

Gugo.collections.eventNetworkInvitations.attachSchema(InviteNetworkSchema);