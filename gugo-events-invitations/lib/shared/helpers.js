const inviteHelpers = {
  timeSince : function() {
    var now = new Date();
    var minutesToShowMins = 60;

    var converted = d37utils.number.convertMS(now.getTime() - this.createdAt.getTime());

    //Show minutes if the time is under 10 minutes, show time stamp if after
    if(converted.d && converted.d > 0) {
      return converted.d + " days ago.";
    } else if (converted.h && converted.h) {
      return converted.h + " hours ago.";
    } else {
      return converted.m <= 0 ? "now" : converted.m + " minutes ago.";
    }
  }
};

Gugo.collections.eventInvitations.helpers(inviteHelpers);
Gugo.collections.eventNetworkInvitations.helpers(inviteHelpers);