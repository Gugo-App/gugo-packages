// Write your package code here!
Gugo.transforms = {
  keyNeedsTraversal: function (stringRep) {
    return stringRep.indexOf('.') > -1;
  },
  //This method takes a string representation of an object ket (obj.subKey.subSubKey) and returns the value, or null if not found
  traverseObjectForVal: function (stringRep, object) {
    var item = _.clone(object),
      nestKeys = [];

    nestKeys = stringRep.split(".");
    for (var i = 0; i < (nestKeys.length + 1); i++) {
      if (typeof item[nestKeys[i]] !== "undefined") {
        item = item[nestKeys[i]];
      }
    }

    if(_.isObject(item)) {
      return null;
    } else if(_.isUndefined(item)) {
      return null;
    } else {
      return item;
    }
  },
  //Gets a key safely and adds it to the transform if it exists
  getKey : function(oKey, obj, tKey, transform) {
    if(Gugo.transforms.keyNeedsTraversal(oKey)) {
      var item = Gugo.transforms.traverseObjectForVal(oKey, obj);
      if(item) {
        transform[tKey] = item;
      }
    } else {
      if(!_.isUndefined(obj[oKey])) {
        transform[tKey] = obj[oKey];
      }
    }
  }
};

//Gugo.transforms = {
//  externalMap : function(transformMap, object) {
//    var transform = {};
//
//    console.log("start", object);
//
//    for(var key in transformMap) {
//      var item = transformMap[key];
//
//      if(_.isString(item)) {
//        console.log("Got string for", item);
//        if(keyNeedsTraversal(item)) {
//          var mItem = traverseObjectForVal(item, object);
//          console.log("Got", mItem);
//          if(typeof mItem !== "undefined") {
//            transform[key] = mItem;
//          }
//        }
//        else if(typeof object[item] !== "undefined") {
//          transform[key] = object[item];
//        } else {
//          transform[key] = transformMap[key];
//        }
//      } else if(_.isBoolean(item)) {
//          transform[key] = transformMap[key];
//      } else if(_.isFunction(item)) {
//        console.log("Got Function for", item);
//      } else {
//        console.log("Couldnt get a type for ", item);
//      }
//    }
//
//    console.log("Transformed object", transform);
//
//    Gugo.collections.events.insert(transform, function(err, docs) {
//      console.log("INserting object....", err, docs);
//    });
//
//  }
//};