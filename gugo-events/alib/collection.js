Gugo.collections.events     = new Mongo.Collection("events");
Gugo.collections.recurringEvents = new Mongo.Collection('recurring-events');
Gugo.collections.userEvents = new Mongo.Collection("user-events");
Gugo.collections.categories = new Mongo.Collection("categories");
Gugo.collections.features   = new Mongo.Collection("features");

//This is for performing operations on events / userEvents as the same "collection"
Gugo.collections.events.performEventOperation = function (op, query, mod) {
  var event      = Gugo.collections.events.findOne(query._id);
  var userEvent  = Gugo.collections.userEvents.findOne(query._id),
      collection = null;

  if (event) {
    collection = Gugo.collections.events;
  } else {
    collection = Gugo.collections.userEvents;
  }

  if (op === "find" || op == "findOne") {
    return collection[op](query);
  } else {
    collection[op](query, mod);
  }

};

Gugo.collections.interestedEvents = new Mongo.Collection("interested-events");
Gugo.collections.calendarEvents   = new Mongo.Collection("calendar-events");


// Client-side only collections
if (Meteor.isClient) {
  Gugo.collections.publicEventListing = new Mongo.Collection("publicEventListing");
  Gugo.collections.venueEventListing = new Mongo.Collection("venueEventListing");
  Gugo.collections.weekendPublicEventsListing = new Mongo.Collection("weekendPublicEventsListing");
}
