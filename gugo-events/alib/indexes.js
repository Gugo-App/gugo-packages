//Events indexes
Gugo.collections.events._ensureIndex({ _id : 1});
Gugo.collections.events._ensureIndex({ startDate : 1});
Gugo.collections.events._ensureIndex({ endDate : 1});
Gugo.collections.events._ensureIndex({ venueId : 1});
Gugo.collections.events._ensureIndex({ 'category.name' : 1});
Gugo.collections.events._ensureIndex({ 'startDate' : 1, 'endDate' : 1});

Gugo.collections.events._ensureIndex({ title : "text" })


//Events indexes
Gugo.collections.userEvents._ensureIndex({ _id : 1});
Gugo.collections.userEvents._ensureIndex({ startDate : 1});
Gugo.collections.userEvents._ensureIndex({ endDate : 1});
Gugo.collections.userEvents._ensureIndex({ ownerId : 1});
Gugo.collections.userEvents._ensureIndex({ ownerId : 1, 'startDate' : 1 });


//Cal
Gugo.collections.calendarEvents._ensureIndex({ events : 1 });
Gugo.collections.calendarEvents._ensureIndex({ ownerId : 1 });

//Interested
Gugo.collections.interestedEvents._ensureIndex({ events : 1 });
Gugo.collections.interestedEvents._ensureIndex({ ownerId : 1 });

//Recurring Events
Gugo.collections.recurringEvents._ensureIndex({ name : 1 })