Gugo.schemas.UserEventSchema = new SimpleSchema({
  title:             {
    type:     String,
    optional: true
  },
  description:      {
    type:     String,
    optional: true
  },
  isAllDay : {
    type : Boolean,
    optional : true
  },
  "images.heroUri": {
    type:     String,
    optional: true
  },
  "images.tileUri": {
    type:     String,
    optional: true
  },
  startDate:        {
    type:     Date,
    optional: true
  },
  endDate:          {
    type:     Date,
    optional: true
  },
  isPrivate:        {
    type:     Boolean,
    optional: false
  },
  isDateTimeLocked: {
    type : Boolean,
    optional: true,
    defaultValue : true
  },
  isLocationLocked: {
    type : Boolean,
    optional: true,
    defaultValue : true
  },
  friendsCanInvite: {
    type : Boolean,
    optional : true,
    defaultValue : true
  },
  ownerId:          {
    type:     String,
    optional: true
  },
  attendeesCount:   {
    type:         Number,
    optional:     true,
    defaultValue: 0
  },
  favoriteCount:    {
    type:         Number,
    optional:     true,
    defaultValue: 0
  },
  commentsCount : {
    type : Number,
    optional : true,
    defaultValue : 0
  },
  location : {
    type : Gugo.schemas.LocationSchema,
    optional : true
  },
  isExternalSource : {
    type : Boolean,
    optional : false
  },
  source: {
    type:     Gugo.schemas.SourceSchema,
    optional: true
  },
  meta : {
    type : Object,
    optional : true,
    blackbox: true
  },
  createdAt:        {
    type:     Date,
    autoValue : function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  }
});

Gugo.collections.userEvents.attachSchema(Gugo.schemas.UserEventSchema);