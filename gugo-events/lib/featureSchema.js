Gugo.data.featureTypes = [
  { id: "idea", label: "Featured Event Idea" },
  { id: "events", label: "Featured Events" },
  { id: "venues", label: "Featured Venues" }
];

Gugo.schemas.FeatureSchema = new SimpleSchema({
  type:             {
    type:          String,
    allowedValues: _.pluck(Gugo.data.featureTypes, "id")
  },
  order:            {
    type:     Number,
    optional: true
  },
  introSnippet:     {
    type:     String,
    optional: true
  },
  title:            {
    type:     String,
    optional: true
  },
  subtitle:         {
    type:     String,
    optional: true
  },
  description:      {
    type:     String,
    optional: true
  },
  location:         {
    type:     Gugo.schemas.LocationSchema,
    optional: true
  },
  eventIds:         {
    type:     [String],
    optional: true
  },
  venueIds:         {
    type:     [String],
    optional: true
  },
  dateEnabled:      {
    type:     Date,
    optional: true
  },
  dateExpired:      {
    type:     Date,
    optional: true
  },
  "images.heroUri": {
    type:     String,
    optional: true
  },
  imageHasTitle:    {
    type:     Boolean,
    optional: true
  },
  createdBy:        {
    type:     String,
    optional: true
  },
  createdAt:        {
    type:      Date,
    optional:  true,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  }
});

Gugo.collections.features.attachSchema(Gugo.schemas.FeatureSchema);
