Gugo.schemas.SourceSchema = new SimpleSchema({
  id:   {
    type:     String,
    optional: true
  },
  name: {
    type:     String,
    optional: true
  }
});

Gugo.schemas.CategorySchema = new SimpleSchema({
  _id:              {
    type:     String,
    optional: true
  },
  name:             {
    type:     String,
    optional: true
  },
  "images.heroUri": {
    type:     String,
    optional: true
  },
  icon:             {
    type:     String,
    optional: true
  }
});

Gugo.schemas.TagSchema = new SimpleSchema({
  name:      {
    type:     String,
    optional: true
  },
  catgoryId: {
    type:     String,
    optional: true
  }
});

Gugo.schemas.EventSchema = new SimpleSchema({
  title:            {
    type:     String,
    optional: true
  },
  description:      {
    type:     String,
    optional: true
  },
  rawDescription : {
    type : String,
    optional : true
  },
  isPrivate:        {
    type:     Boolean,
    optional: false
  },
  startDate:        {
    type:     Date,
    optional: true
  },
  endDate:          {
    type:     Date,
    optional: true
  },
  details: {
    type: [Number],
    optional: true
  },
  snippet:          {
    type:     String,
    optional: true
  },
  imageHasTitle:    {
    type:     Boolean,
    optional: true
  },
  "images.heroUri": {
    type:     String,
    optional: true
  },
  "images.tileUri": {
    type:     String,
    optional: true
  },
  category:         {
    type:     Gugo.schemas.CategorySchema,
    optional: true
  },
  tags:             {
    type:     [Gugo.schemas.TagSchema],
    optional: true
  },
  isDateTimeLocked: {
    type:         Boolean,
    optional:     true,
    defaultValue: true
  },
  isLocationLocked: {
    type:         Boolean,
    optional:     true,
    defaultValue: true
  },
  ownerId:          {
    type:     String,
    optional: true
  },
  keywords:         {
    type:     [String],
    optional: true
  },
  moreInfoUri: {
    type: String,
    optional: true
  },
  buyTicketsUri:    {
    type:     String,
    optional: true
  },
  isSoldOut: {
    type: Boolean,
    optional: true
  },
  attendeesCount:   {
    type:         Number,
    optional:     true,
    defaultValue: 0
  },
  favoriteCount:    {
    type:         Number,
    optional:     true,
    defaultValue: 0
  },
  venueId:          {
    type:     String,
    optional: true
  },
  meta:             {
    type:     Object,
    optional: true,
    blackbox: true
  },
  isExternalSource: {
    type:     Boolean,
    optional: false
  },
  source:           {
    type:     Gugo.schemas.SourceSchema,
    optional: true
  },
  createdAt:        {
    type:      Date,
    optional:  true,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  }
});

// Attach Schemas
Gugo.collections.events.attachSchema(Gugo.schemas.EventSchema);
Gugo.collections.categories.attachSchema(Gugo.schemas.CategorySchema);


//TODO: implement if we can get it working
//if (Meteor.isServer) {
//  Gugo.collections.events._ensureIndex({ "title": "text", "description": "text" });
//}
