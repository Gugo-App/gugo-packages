// EVENTS
Meteor.publish("eventsToday", function (city) {
  var beginToday,
      endToday;

  //TODO: lookup timezone from city
  //var timezone = city || Gugo.timezone.Pacific;
  var timezone = Gugo.timezone.Pacific;

  beginToday = d37utils.date.getStartOfDay(timezone);
  endToday   = d37utils.date.getEndOfDay(timezone);

  return Gugo.collections.events.find(
    {
      //TODO: restrict by city, but this will require a different query since only venue ID lives in event doc
      //"venue.city":      city,
      startDate: { $gte: beginToday, $lt: endToday }
    },
    { sort: { startDate: 1 } }
  );
});

Meteor.publish("queriedPublicEvents", function (query, limit) {
  console.log("Query....", query, limit);
  const options = { limit: limit, sort: { startDate: 1 } };
  return Gugo.collections.events.find(query, options)
});

// I'm trying to make this a new publishComposite option - Hope it works! (-Emma)
Meteor.publishComposite("eventsTodayWithVenue", {
  find:     function (city) {
    var beginToday,
        endToday;

    //TODO: lookup timezone from city
    //var timezone = city || Gugo.timezone.Pacific;
    var timezone = Gugo.timezone.Pacific;

    beginToday = d37utils.date.getStartOfDay(timezone);
    endToday   = d37utils.date.getEndOfDay(timezone);

    return Gugo.collections.events.find(
      {
        //TODO: restrict by city, but this will require a different query since only venue ID lives in event doc
        //"venue.city":      city,
        startDate: { $gte: beginToday, $lt: endToday }
      },
      { sort: { startDate: 1 } }
    );
  },
  children: [
    {
      find: function (event) {
        //console.log("Getting venue for event:", event);
        //console.log(Gugo.collections.venues.findOne(event.venueId));
        return Gugo.collections.venues.find({ _id: event.venueId }, { limit: 1 });
      }
    }
  ]
});

Meteor.publishComposite("publicEventsLimit", function (limit, pageSize, options, collectionName) {
  this.unblock();

  var startDt = d37utils.date.getStartOfDay(),
      filter  = null,
      mCollectionName = "publicEventListing";

  if(options && options.startDate) {
    filter = options;
  } else {
    filter = _.extend(options || {}, { startDate: { $gte: startDt } });
  }

  if(collectionName) {
    mCollectionName = collectionName;
  }


  console.log("publicEventsLimit", pageSize, filter, (limit - 20), mCollectionName);

  return {
    collectionName: mCollectionName,
    find:           function () {
      limit = limit || 50;
      console.log(mCollectionName, limit, startDt);
      //console.log("RETURNING", Gugo.collections.events.find(
      //  filter,
      //  {
      //    sort: { startDate: 1 },
      //    limit: 20,
      //    skip : (limit - 20)
      //  }
      //).fetch().length)
      return Gugo.collections.events.find(
        filter,
        {
          sort: { startDate: 1 },
          limit: pageSize,
          skip : (limit - pageSize)
        }
      );
    },
    children:       [
      {
        find: function (event) {
          return Gugo.collections.venues.find({ _id: event.venueId }, { limit: 1 });
        }
      }
    ]
  }
});


Meteor.publishComposite("weekendPublicEventsLimit", function (limit, query) {
  //console.log("publicWeekendEventsLimit", limit, query);

  return {
    collectionName: "weekendPublicEventsListing",
    find:           function () {
      limit = limit || 50;
      //console.log(Gugo.collections.events.find(
      //  query,
      //  { sort: { startDate: 1 }, limit: limit }
      //).count());
      return Gugo.collections.events.find(
        query,
        { sort: { startDate: 1 }, limit: limit }
      );
    },
    children:       [
      {
        find: function (event) {
          return Gugo.collections.venues.find({ _id: event.venueId }, { limit: 1 });
        }
      }
    ]
  }
});

Meteor.publish("singleEvent", function (eventId) {
  var event     = Gugo.collections.events.find(eventId);
  var userEvent = Gugo.collections.userEvents.find(eventId);

  if (event && event.fetch()[0]) {
    return event;
  } else if (userEvent && userEvent.fetch()[0]) {
    return userEvent;
  } else {
    return this.ready();
  }

});

Meteor.publish("myUserEvents", function () {
  this.unblock();

  if (this.userId) {
    var userEvents = Gugo.collections.userEvents.find({ ownerId: this.userId });
    return userEvents;
  } else {
    return this.ready();
  }
});

Meteor.publish("myInterestedEvents", function () {
  this.unblock();

  if (!this.userId) {
    return this.ready();
  }

  //This may cause complications, we wrap this in an autorun because of the interaction with the infinite list shoving
  //events into another collection, aka we wont be subscribed to events in the events collecition when we get data
  //through that component
  this.autorun(function (comp) {
    const interestedEvents = Gugo.collections.interestedEvents.find({ ownerId: this.userId });
    if (interestedEvents.fetch().length === 0) {
      return this.ready();
    }

    const intEvent = interestedEvents.fetch()[0];

    return [
      interestedEvents,
      Gugo.collections.events.find({ _id: { $in: intEvent.events } }),
      Gugo.collections.userEvents.find({ _id: { $in: intEvent.events } })
    ]
  });

});

Meteor.publish("myCalendarEvents", function () {
  this.unblock();

  if (!this.userId) {
    return;
  }

  if (!this.userId) {
    return this.ready();
  }

  this.autorun(function (comp) {

    const calendarEvents = Gugo.collections.calendarEvents.find({ ownerId: this.userId });
    if (calendarEvents.fetch().length === 0) {
      return this.ready();
    }

    const calEvent = calendarEvents.fetch()[0];

    //console.log("Cal events", calEvent);

    return [
      calendarEvents,
      Gugo.collections.events.find({ _id: { $in: calEvent.events } }),
      Gugo.collections.userEvents.find({ _id: { $in: calEvent.events } })
    ]
  });
});

Meteor.publishComposite("publicEventsWithVenue", function(query) {
  console.log("publicEventsWithVenue Query", query);

  return {
    find:           function () {
      //console.log(Gugo.collections.events.findOne(query));
      return Gugo.collections.events.find(query);
    },
    children:       [
      {
        find: function (event) {
          return Gugo.collections.venues.find({ _id: event.venueId }, { limit: 1 });
        }
      }
    ]
  }
});


Meteor.publish("allCategories", function () {
  //console.log("Public events query", this.userId);
  return Gugo.collections.categories.find({}, { sort: { name: 1 } });
});
//TODO: implement tags
//Meteor.publishComposite("categories", {
//  find:     function () {
//    //console.log("Public events query", this.userId);
//    return Gugo.collections.categories.find({});
//  },
//  children: [
//    {
//      find: function (event) {
//        //console.log("Getting venue for event:", event);
//        //console.log(Gugo.collections.venues.findOne(event.venueId));
//        return Gugo.collections.venues.find({ _id: event.venueId }, { limit: 1 });
//      }
//    }
//  ]
//});

Meteor.publish('currentFeaturesBasic', function() {
  this.unblock();

  var now = new Date();

  return Gugo.collections.features.find({
    $and: [
      {$or: [{dateEnabled: {$lte: now}}, {dateEnabled: null}]},
      {$or: [{dateExpired: {$gte: now}}, {dateExpired: null}]}
    ]
  }, {sort: {order: 1}});

});

// TODO: implement querying by location / city
Meteor.publishComposite("currentFeatures", function() {
  this.unblock();

  return {
    find: function () {
      var now = new Date();
      console.log("Public events query", this.userId, now);
      return Gugo.collections.features.find({
        $and: [
          {$or: [{dateEnabled: {$lte: now}}, {dateEnabled: null}]},
          {$or: [{dateExpired: {$gte: now}}, {dateExpired: null}]}
        ]
      }, {sort: {order: 1}});
    },
    children: [
      {
        find: function (item) {
          if (item.eventIds) {
            return Gugo.collections.events.find({_id: {$in: item.eventIds}});

          } else if (item.venueIds) {
            return Gugo.collections.venues.find({_id: {$in: item.venueIds}});

          } else {
            return null;
          }
        },
        children: [
          {
            find: function (event, item) {
              if (item.eventIds) {
                return Gugo.collections.venues.find({_id: event.venueId});
              }
              return null;
            }
          }
        ]
      }
    ]
  }
});

// TODO: implement querying by location / city
Meteor.publishComposite("currentFeaturesLimit", function(limit, pageSize, options) {
  this.unblock();

  return {
    find: function () {
      var now = new Date();
      console.log("Public events query", this.userId, now);
      return Gugo.collections.features.find({
        $and: [
          {$or: [{dateEnabled: {$lte: now}}, {dateEnabled: null}]},
          {$or: [{dateExpired: {$gte: now}}, {dateExpired: null}]}
        ]
      }, { sort: { order: 1 }, limit: pageSize, skip : (limit - pageSize) });
    },
    children: [
      {
        find: function (item) {
          if (item.eventIds) {
            return Gugo.collections.events.find({_id: {$in: item.eventIds}});

          } else if (item.venueIds) {
            return Gugo.collections.venues.find({_id: {$in: item.venueIds}});

          } else {
            return null;
          }
        },
        children: [
          {
            find: function (event, item) {
              if (item.eventIds) {
                return Gugo.collections.venues.find({_id: event.venueId});
              }
              return null;
            }
          }
        ]
      }
    ]
  }
});

//This is for testering
Meteor.publishComposite("allEvents", {
  find:     function () {

    return Gugo.collections.events.find(
      { },
      { sort: { startDate: 1 } }
    );
  },
  children: [
    {
      find: function (event) {
        //console.log("Getting venue for event:", event);
        //console.log(Gugo.collections.venues.findOne(event.venueId));
        return Gugo.collections.venues.find({ _id: event.venueId }, { limit: 1 });
      }
    }
  ]
});

Meteor.publish("recurringEvent", function(id) {
  console.log("RECURRING EVENT PUBLISH", id);
  if(!id) return this.ready();

  this.autorun(function(comp) {
    const recurringEvent = Gugo.collections.recurringEvents.findOne(id);

    if (!recurringEvent) return this.ready();

    if (recurringEvent.events) {
      return [
        Gugo.collections.recurringEvents.find(id, { limit: 1 }),
        Gugo.collections.events.find({ _id: { $in: recurringEvent.events } })
      ]
    } else {
      return Gugo.collections.recurringEvents.find(id, { limit: 1 });
    }
  });

});

Meteor.publish("recurringEvents", function(filter) {
  console.log("RECURRING EVENT PUBLISH", filter);
  var query;

  if(!filter) query = {};
  else query = filter;

  return Gugo.collections.recurringEvents.find(query);

});

Meteor.publishComposite("singleFeature", function (id) {
  return {
    find:     function () {
      console.log("Public single feature query", id);
      return Gugo.collections.features.find({ _id: id });
    },
    children: [
      {
        find: function (item) {
          if (item.eventIds) {
            return Gugo.collections.events.find({ _id: { $in: item.eventIds } });

          } else if (item.venueIds) {
            return Gugo.collections.venues.find({ _id: { $in: item.venueIds } });

          } else {
            return null;
          }
        },
        children: [
          {
            find: function (event, item) {
              if (item.eventIds) {
                return Gugo.collections.venues.find({ _id: event.venueId });
              }
              return null;
            }
          }
        ]
      }
    ]
  }
});