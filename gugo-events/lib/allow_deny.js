Gugo.collections.events.allow({
  insert: function() {
    return true;
  },
  remove : function() {
    return true;
  }
});

Gugo.collections.userEvents.allow({
  insert: function() {
    return true;
  },
  remove : function() {
    return true;
  }
});

Gugo.collections.recurringEvents.allow({
  insert: function() {
    return true;
  },
  update : function() {
    return true;
  },
  remove : function() {
    return true;
  }
});