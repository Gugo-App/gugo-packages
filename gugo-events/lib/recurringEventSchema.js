RecurringEventsSchema =  new SimpleSchema({
  name : {
    type : String,
    optional : true
  },
  events : {
    type : [String],
    optional : true
  },
  template : {
    type : Gugo.schemas.EventSchema,
    optional : false
  }
});

Gugo.collections.recurringEvents.attachSchema(RecurringEventsSchema);