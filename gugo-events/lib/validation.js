Gugo.validate.Category = function (doc, userId) {
  var pfx = "gugo-events.validate.Category";

  if (userId && !Match.test(userId, String)) {
    throw new Meteor.Error(pfx, "No valid user ID found");
  }

  if (!Match.test(doc, Gugo.schemas.CategorySchema)) {
    console.error(pfx + " Category is invalid", doc);
    throw new Meteor.Error(pfx, "Category is not valid");
  }

  console.log(pfx + " Category is valid", doc);

  return true;
};