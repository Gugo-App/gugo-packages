function prepDatabaseEvent(event, userId) {
  var doc = {
    images: {},
    ownerId: userId,
    title: event.title,
    description: event.description,
    startDate: event.startDate || null,
    endDate: event.endDate || null,
    isDateTimeLocked: event.isDateTimeLocked,
    isLocationLocked: event.isLocationLocked,
    friendsCanInvite: event.friendsCanInvite,
    isAllDay : event.isAllDay,
    isPrivate: true,
    isExternalSource: false,
    location: null
  };

  if(event.img) {
    doc.images.heroUri = event.img;
  }

  if (event.location && event.location.name && !_.isEmpty(event.location.name)) {
    doc.location = {};

    var location = event.location;

    if (location.name) {
      doc.location.name = location.name;
    }

    if (location.address) {
      doc.location.address = location.address;
    }

    if (location.geo) {
      doc.location.geo = {
        coordinates: [location.geo.longitude, location.geo.latitude],
        type : "Point"
      }
    }
  }

  return doc;
}

Meteor.methods({
  createPrivateEvent : function(event, contactInvites, networkInvites) {
    console.log("Creating event", event);
    check(this.userId, String);

    var doc = prepDatabaseEvent(event, this.userId);

    console.log("Inserting Private Event", doc);

    var id = Gugo.collections.userEvents.insert(doc, Gugo.handlers.mongo.bind(this, { msg : "User Events" }));

    if (id) {
      Meteor.call('addEventToCalendar', id);
      if (!event.isDateTimeLocked && (event.startDate || event.endDate)) {
        Meteor.call('addSuggestedDate', (event.startDate || null), (event.endDate || null), event.isAllDay, id);
      }

      if (!event.isLocationLocked && doc.location) {
        console.log("EVENT LOCATION", doc.location);
        Meteor.call('addSuggestedLocation', event.location, id);
      }

      var user = Meteor.users.findOne(this.userId);

      if(Meteor.isServer) {
        Meteor.defer( function() {
          console.log("Deferred till created event");
          if (contactInvites && contactInvites.length > 0) {
            Meteor.call('batchInvite', {
                userId: user._id,
                phone: user.phone,
                name: user.profile.name
              },
              contactInvites,
              id
            );
          }

          if (networkInvites && networkInvites.length > 0) {
            Meteor.call('inviteNetworks', {
                userId: user._id,
                phone: user.phone,
                name: user.profile.name
              },
              networkInvites,
              id)
          }
        });
      }
    } else {
      throw new Meteor.Error(500, "Could not create private event, Please Try Again");
    }

    return id;
  },
  updatePrivateEvent : function(eventId, event, contactInvites, networkInvites) {
    console.log("Event id", eventId, event);

    var doc = prepDatabaseEvent(event, this.userId);

    function mongoCallback(err, docs) {
      if(err) {
        throw new Meteor.Error(err);
      }

      if(docs === 0) {
        throw new Meteor.Error('Error Updating event');
      }

      return docs;
    }

    //console.log("Doc", doc);

    if (!event.isDateTimeLocked && (event.startDate || event.endDate)) {
      Meteor.call('addSuggestedDate', (event.startDate || null), (event.endDate || null), event.isAllDay, eventId);
    }

    if (!event.isLocationLocked && doc.location) {
      //console.log("EVENT LOCATION", doc.location);
      Meteor.call('addSuggestedLocation', event.location, eventId);
    }


    Gugo.collections.userEvents.update(eventId, {
      $set : doc
    }, Gugo.handlers.mongo.bind(this, { message : "User Event Update", callback : mongoCallback }))

    var user = Meteor.users.findOne(this.userId);

    //console.log("USER", user);
    //console.log("Contact invites", contactInvites);


    if(Meteor.isServer) {
      Meteor.defer( function() {
        //Gugo.collections.eventInvitations.remove({
        //  'invited.contactId': {
        //    $nin: contactInvites
        //  }
        //});

        if (contactInvites && contactInvites.length > 0) {
          Meteor.call('batchInvite', {
              userId: user._id,
              phone: user.phone,
              name: user.profile.name
            },
            contactInvites,
            eventId
          );
        }

        //Gugo.collections.eventNetworkInvitations.remove({
        //  'networkId': {
        //    $nin: networkInvites
        //  }
        //});

        if (networkInvites && networkInvites.length > 0) {
          Meteor.call('inviteNetworks', {
              userId: user._id,
              phone: user.phone,
              name: user.profile.name
            },
            networkInvites,
            eventId)
        }
      });
    }

  },
  inviteToEvent : function(contactInvites, networkInvites, eventId) {
    var user = Meteor.users.findOne(this.userId);

    //Gugo.collections.eventInvitations.remove({
    //  'invited.contactId': {
    //    $nin: contactInvites
    //  }
    //});

    if (contactInvites && contactInvites.length > 0) {
      Meteor.call('batchInvite', {
          userId: user._id,
          phone: user.phone,
          name: user.profile.name
        },
        contactInvites,
        eventId
      );
    }

    //Gugo.collections.eventNetworkInvitations.remove({
    //  'networkId': {
    //    $nin: networkInvites
    //  }
    //});

    if (networkInvites && networkInvites.length > 0) {
      Meteor.call('inviteNetworks', {
          userId: user._id,
          phone: user.phone,
          name: user.profile.name
        },
        networkInvites,
        eventId)
    }

    return true;
  },
  deletePrivateEvent : function(eventId) {
    Gugo.collections.activity.remove({ eventId : eventId });
    Gugo.collections.comments.remove({ eventId : eventId });
    Gugo.collections.eventInvitations.remove({ eventId : eventId });
    Gugo.collections.eventNetworkInvitations.remove({ eventId : eventId });

    Gugo.collections.interestedEvents.update({ events : eventId }, {
      $pull : {
        events : eventId
      }
    }, function(err, docs) {
      console.log("Err", err, "docs", docs)
    });

    Gugo.collections.calendarEvents.update({ events : eventId }, {
      $pull : {
        events : eventId
      }
    }, function(err, docs) {
      console.log("Err", err, "docs", docs)
    });



    return Gugo.collections.userEvents.remove(eventId);
  }
});

function performEventOperation(op, query, mod) {
  var event = Gugo.collections.events.findOne(query._id);
  var userEvent = Gugo.collections.userEvents.findOne(query._id);

  if(event) {
    Gugo.collections.events[op](query, mod);
  } else {
    Gugo.collections.userEvents[op](query, mod);
  }
}


Meteor.methods({
  addEventToInterested : function(eventId) {
    if(!this.userId) return;

    function cb(err) {
      if(!err) {
        Meteor.call('newActivity', { eventId: eventId, type : ActivityTypes.NEW_INTEREST });
      }
    }

    Gugo.collections.interestedEvents.upsert({ ownerId : this.userId}, {
      $push : {
        events : eventId
      }
    }, Gugo.handlers.mongo.bind(this, { message : " Add Interest", callback : cb }));

    performEventOperation('update', { _id : eventId }, { $inc : { favoriteCount : 1} });
  },
  removeEventFromInterested : function(eventId) {
    if(!this.userId) return;


    Gugo.collections.interestedEvents.update({ ownerId : this.userId}, {
      $pull : {
        events : eventId
      }
    });

    performEventOperation('update', { _id : eventId }, { $inc : { favoriteCount : -1 } });
  },
  removeAllBlacklistedInterestedEvents : function(blacklistUserId) {
    var interestedEvents = Gugo.collections.interestedEvents.findOne({ ownerId : this.userId });

    console.log("INterested", interestedEvents);

    if(!interestedEvents) return;

    var eventsToRemove = Gugo.collections.userEvents.find({ _id : { $in : interestedEvents.events }, ownerId : blacklistUserId }).fetch();

    console.log("Eventsrto remove", eventsToRemove);

    if(!eventsToRemove) return;

    var eventIdsToRemove = eventsToRemove.map( function(event) {
      return event._id;
    });

    if(eventIdsToRemove && eventIdsToRemove.length > 0) {
      Gugo.collections.interestedEvents.update(interestedEvents._id, { $pullAll : { 'events' : eventIdsToRemove  } });
    }
  },
  addEventToCalendar : function(eventId) {
    if(!this.userId) return;

    function cb(err) {
      if(!err) {
        Meteor.call('newActivity', { eventId: eventId, type : ActivityTypes.NEW_CALENDAR });
      }
    }

    Gugo.collections.calendarEvents.upsert({ ownerId : this.userId}, {
      $push : {
        events : eventId
      }
    }, Gugo.handlers.mongo.bind(this, { message : " Add Calendar", callback : cb }));

  },
  removeEventFromCalendar : function(eventId) {
    if(!this.userId) return;

    Gugo.collections.calendarEvents.update({ ownerId : this.userId}, {
      $pull : {
        events : eventId
      }
    });
  },
  removeAllBlacklistedCalendarEvents : function(blacklistUserId) {
    var calendarEvents = Gugo.collections.calendarEvents.findOne({ ownerId : this.userId });

    if(!calendarEvents) return;

    console.log("Calendar events");

    var eventsToRemove = Gugo.collections.userEvents.find({ _id : { $in : calendarEvents.events }, ownerId : blacklistUserId }).fetch();

    if(!eventsToRemove) return;

    console.log("Events ro remove", eventsToRemove);
    var eventIdsToRemove = eventsToRemove.map( function(event) {
      return event._id;
    });

    if(eventIdsToRemove && eventIdsToRemove.length > 0) {
      Gugo.collections.calendarEvents.update(calendarEvents._id, { $pullAll : { 'events' : eventIdsToRemove  } });
    }
  }
});

