// Collection Helpers
Gugo.collections.helpers.events = {
  startDateMonthDay: function () {
    return moment(this.startDate).format("MMMM D");
  },
  fullStartDateTime: function () {
    if (!this.startDate) {
      return "TBA";
    }

    return moment(this.startDate).format("dddd MMMM D, hA")
  },
  fullEndDateTime:   function () {
    if (!this.startDate) {
      return "TBA";
    }

    return moment(this.endDate).format("dddd MMMM D, hA")
  },
  fullDateTime:      function () {
    const start  = this.startDate;
    const end    = this.endDate;
    const allDay = this.isAllDay;

    var str = "TBA";

    if (!start) {
      return str;
    }

    //console.log("Start", start, "end", end);

    if (allDay) {
      if (start.getDay() === end.getDay() && start.getMonth() === end.getMonth()) {
        str = moment(start).format("MMM Do") + ", all day";
      } else if (start.getMonth() === end.getMonth()) {
        str = moment(start).format("MMM Do") + moment(end).format(" - Do") + ", all day";
      } else {
        str = moment(start).format("MMM Do") + moment(end).format(" - MMM Do") + ", all day";
      }
    } else {
      if (start && !end) {
        str = moment(start).format("MMM Do, hh:mm A");
      } else if (start.getDay() === end.getDay()) {
        str = moment(start).format("MMM Do, hh:mm A") + " - " + moment(end).format("hh:mm A");
      } else {
        str = moment(start).format("MMM Do, hh:mm A") + " - " + moment(end).format("MMM Do, hh:mm A");
      }
    }

    return str;
  },
  fullTime:          function () {
    if (this.startDate && !this.endDate) {
      return moment(this.startDate).format("hA")
    } else {
      return moment(this.startDate).format("hA") + " - " + moment(this.endDate).format("hA");
    }
  },
  eventLocation:     function () {
    // venue is not stored in events
    return null;
  },
  heroUri:           function () {
    if (this.images && this.images.heroUri) {
      return this.images.heroUri;
    } else {
      return '/images/bulletin-default.png';
    }
  }
};

Gugo.collections.events.helpers(Gugo.collections.helpers.events);

if (Meteor.isClient) {
  Gugo.collections.publicEventListing.helpers(Gugo.collections.helpers.events);
  Gugo.collections.weekendPublicEventsListing.helpers(Gugo.collections.helpers.events);
}

// Global helpers
Gugo.collections.events.utils = {
  get defaultEvent () {
    return {
      _id:         "default",
      name:        "Default Event Name",
      description: "The default event description.",
      startDate:   new Date(),
      images:      {
        heroUri: "//dummyimage.com/480x270",
        tileUri: "//dummyimage.com/100x100"
      },
      venue:       {
        name: "Default Venue Name"
      }
    }
  },
  get emptyEvent () {
    return {
      startDate: new Date(),
      createdAt: new Date()
    };
  },
  findEventFromAllCollections : function(query) {
    return Gugo.collections.events.findOne(query) || Gugo.collections.publicEventListing.findOne(query) || Gugo.collections.weekendPublicEventsListing.findOne(query);
  }
};


// Collection Helpers
Gugo.collections.userEvents.helpers({
  startDateMonthDay: function () {
    return moment(this.startDate).format("MMMM D");
  },
  fullStartDateTime: function () {
    if (!this.startDate) {
      return "TBA";
    }

    return moment(this.startDate).format("dddd MMMM D, hA")
  },
  fullEndDateTime:   function () {
    if (!this.startDate) {
      return "TBA";
    }

    return moment(this.endDate).format("dddd MMMM D, hA")
  },
  fullDateTime:      function () {
    const start  = this.startDate;
    const end    = this.endDate;
    const allDay = this.isAllDay;

    var str = "TBA";

    if (!start) {
      return str;
    }

    console.log("Start", start, "end", end);

    if (allDay) {
      if (start.getDate() === end.getDate() && start.getMonth() === end.getMonth()) {
        str = moment(start).format("MMM Do") + ",  all day";
      } else if (start.getMonth() === end.getMonth()) {
        str = moment(start).format("MMM Do") + moment(end).format(" - Do") + ",  all day";
      } else {
        str = moment(start).format("MMM Do") + moment(end).format(" - MMM Do") + ",  all day";
      }
    } else {
      if (start && !end) {
        str = moment(start).format("MMM Do, hh:mm A");
      } else if (start.getDate() === end.getDate()) {
        str = moment(start).format("MMM Do, hh:mm A") + " - " + moment(end).format("hh:mm A");
      } else {
        str = moment(start).format("MMM Do, hh:mm A") + " - " + moment(end).format("MMM Do, hh:mm A");
      }
    }

    return str;
  },
  fullTime:          function () {
    if (this.startDate && !this.endDate) {
      return moment(this.startDate).format("hA")
    } else {
      return moment(this.startDate).format("hA") + " - " + moment(this.endDate).format("hA");
    }
  },
  eventLocation:     function () {
    if (this.location) {
      if (this.location.name) {
        return this.location.name;
      } else if (this.location.address) {
        return this.location.address;
      } else {
        return "";
      }
    } else {
      return "";
    }
  },
  heroUri:           function () {
    if (this.images && this.images.heroUri) {
      return this.images.heroUri;
    } else {
      return '/images/bulletin-default.png';
    }
  }
});
