Package.describe({
  name: 'gugo:events',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use([
    'ecmascript',
    'meteor-platform',
    'underscore',
    'gugo:core',
    'gugo:venues',
    'aldeed:collection2@2.7.0',
    'ground:db@0.3.14',
    'peerlibrary:reactive-publish'
  ]);

  api.addFiles('alib/collection.js', ['client', 'server'], { transpile : false });
  api.addFiles('alib/indexes.js', 'server', { transpile : false });
  api.addFiles('lib/publication.js', ['server'], { transpile : false });

  api.addFiles([
    'lib/allow_deny.js',
    'lib/eventSchema.js',
    'lib/featureSchema.js',
    'lib/userEventSchema.js',
    'lib/methods.js',
    'lib/validation.js',
    'lib/recurringEventSchema.js'
  ], ['client', 'server'], { transpile : false });

  api.addFiles('event-helpers.js', ['client', 'server'], { transpile : false });

  api.addFiles('events.js');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('goin:events');
  api.addFiles('events-tests.js');
});
